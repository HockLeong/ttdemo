package com.tunetalk.ttdealersdk.ocr;

public enum DocumentType
{
    MYKAD, MRTD, IKAD, WORK_PERMIT, VISA, BARCODE;
}
