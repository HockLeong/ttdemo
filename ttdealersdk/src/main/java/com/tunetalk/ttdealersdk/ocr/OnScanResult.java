package com.tunetalk.ttdealersdk.ocr;

import com.tunetalk.ttdealersdk.ocr.entity.ScanResultEntity;

public interface OnScanResult
{
    void onScanResult (ScanResultEntity entity);

    void onImageResult (byte[] imageBytes);
}
