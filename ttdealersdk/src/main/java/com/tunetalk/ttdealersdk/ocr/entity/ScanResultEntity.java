package com.tunetalk.ttdealersdk.ocr.entity;

public class ScanResultEntity
{
    ScanResultItemEntity myKad;
    ScanResultItemEntity iKad;
    ScanResultItemEntity mrtd;
    ScanResultItemEntity barcode;

    public ScanResultItemEntity getMyKad ()
    {
        return myKad;
    }

    public ScanResultEntity setMyKad (ScanResultItemEntity myKad)
    {
        this.myKad = myKad;
        return this;
    }

    public ScanResultItemEntity getiKad ()
    {
        return iKad;
    }

    public ScanResultEntity setiKad (ScanResultItemEntity iKad)
    {
        this.iKad = iKad;
        return this;
    }

    public ScanResultItemEntity getMrtd ()
    {
        return mrtd;
    }

    public ScanResultEntity setMrtd (ScanResultItemEntity mrtd)
    {
        this.mrtd = mrtd;
        return this;
    }

    public ScanResultItemEntity getBarcode ()
    {
        return barcode;
    }

    public ScanResultEntity setBarcode (ScanResultItemEntity barcode)
    {
        this.barcode = barcode;
        return this;
    }
}
