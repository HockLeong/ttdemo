package com.tunetalk.ttdealersdk.ocr;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.microblink.entities.recognizers.Recognizer;
import com.microblink.entities.recognizers.blinkid.imageoptions.FaceImageOptions;
import com.microblink.entities.recognizers.blinkid.imageoptions.FullDocumentImageOptions;
import com.microblink.entities.recognizers.blinkid.imageoptions.MrzImageOptions;
import com.microblink.entities.recognizers.blinkid.imageoptions.SignatureImageOptions;
import com.microblink.uisettings.BarcodeUISettings;
import com.microblink.uisettings.BaseScanUISettings;
import com.microblink.uisettings.UISettings;
import com.microblink.uisettings.options.BeepSoundUIOptions;
import com.microblink.uisettings.options.HelpIntentUIOptions;
import com.microblink.uisettings.options.ShowOcrResultMode;
import com.microblink.uisettings.options.ShowOcrResultUIOptions;
import com.tunetalk.ttdealersdk.R;

public class OCRSettings
{
    /**
     * Configure all OCR scan settings
     *
     * @param settings
     * @param helpIntent
     */
    public static void configureScanSettings (@NonNull UISettings settings, @Nullable Intent helpIntent)
    {
        if (settings instanceof BeepSoundUIOptions)
        {
            ((BeepSoundUIOptions) settings).setBeepSoundResourceID(R.raw.beep);
        }

        if (helpIntent != null && settings instanceof HelpIntentUIOptions)
        {
            ((HelpIntentUIOptions) settings).setHelpIntent(helpIntent);
        }

        if (settings instanceof ShowOcrResultUIOptions)
        {
            // If you want, you can disable drawing of OCR results on scan activity. Drawing OCR results can be visually
            // appealing and might entertain the user while waiting for scan to complete, but might introduce a small
            // performance penalty.
            // ((ShowOcrResultUIOptions) settings).setShowOcrResult(false);

            // Enable showing of OCR results as animated dots. This does not have effect if non-OCR recognizer like
            // barcode recognizer is active.
            ((ShowOcrResultUIOptions) settings).setShowOcrResultMode(ShowOcrResultMode.ANIMATED_DOTS);
        }

        if (settings instanceof BarcodeUISettings)
        {
            ((BarcodeUISettings) settings).setShowDialogAfterScan(false);
        }

        if (settings instanceof BaseScanUISettings)
        {
            BaseScanUISettings baseScanUISettings = ((BaseScanUISettings) settings);
            baseScanUISettings.setPinchToZoomAllowed(true);
        }
    }

    /**
     * Configure all the images retrieve settings
     *
     * @param recognizer
     * @return
     */
    public static Recognizer configureImageSettings (Recognizer recognizer)
    {
        if (recognizer instanceof FullDocumentImageOptions)
        {
            FullDocumentImageOptions options = (FullDocumentImageOptions) recognizer;
            options.setReturnFullDocumentImage(true);
        }

        if (recognizer instanceof FaceImageOptions)
        {
            FaceImageOptions options = (FaceImageOptions) recognizer;
            options.setReturnFaceImage(true);
        }

        if (recognizer instanceof SignatureImageOptions)
        {
            SignatureImageOptions options = (SignatureImageOptions) recognizer;
            options.setReturnSignatureImage(true);
        }

        if (recognizer instanceof MrzImageOptions)
        {
            MrzImageOptions options = (MrzImageOptions) recognizer;
            options.setReturnMrzImage(true);
        }

        return recognizer;
    }

}

