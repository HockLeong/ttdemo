package com.tunetalk.ttdealersdk.ocr.entity;


import com.tunetalk.ttdealersdk.ocr.DocumentType;

public class ScanResultItemEntity
{
    DocumentType documentType;

    //Common
    String fullName;
    String gender;
    String dob; //Format (yyyy-MM-dd)

    //MyKad
    String nric;
    String poscode;
    String address;

    //Passport
    String passportNumber;
    String nationality;

    //Barcode
    String scannedBarCode;


    public DocumentType getDocumentType ()
    {
        return documentType;
    }

    public ScanResultItemEntity setDocumentType (DocumentType documentType)
    {
        this.documentType = documentType;
        return this;
    }

    public String getFullName ()
    {
        return fullName;
    }

    public ScanResultItemEntity setFullName (String fullName)
    {
        this.fullName = fullName;
        return this;
    }

    public String getGender ()
    {
        return gender;
    }

    public ScanResultItemEntity setGender (String gender)
    {
        this.gender = gender;
        return this;
    }

    public String getDob ()
    {
        return dob;
    }

    public ScanResultItemEntity setDob (String dob)
    {
        this.dob = dob;
        return this;
    }

    public String getNric ()
    {
        return nric;
    }

    public ScanResultItemEntity setNric (String nric)
    {
        this.nric = nric;
        return this;
    }

    public String getPoscode ()
    {
        return poscode;
    }

    public ScanResultItemEntity setPoscode (String poscode)
    {
        this.poscode = poscode;
        return this;
    }

    public String getAddress ()
    {
        return address;
    }

    public ScanResultItemEntity setAddress (String address)
    {
        this.address = address;
        return this;
    }

    public String getPassportNumber ()
    {
        return passportNumber;
    }

    public ScanResultItemEntity setPassportNumber (String passportNumber)
    {
        this.passportNumber = passportNumber;
        return this;
    }

    public String getNationality ()
    {
        return nationality;
    }

    public ScanResultItemEntity setNationality (String nationality)
    {
        this.nationality = nationality;
        return this;
    }

    public String getScannedBarCode ()
    {
        return scannedBarCode;
    }

    public ScanResultItemEntity setScannedBarCode (String scannedBarCode)
    {
        this.scannedBarCode = scannedBarCode;
        return this;
    }
}
