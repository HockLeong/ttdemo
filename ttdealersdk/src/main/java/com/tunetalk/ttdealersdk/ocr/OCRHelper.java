package com.tunetalk.ttdealersdk.ocr;

import android.app.Activity;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.microblink.entities.recognizers.Recognizer;
import com.microblink.entities.recognizers.RecognizerBundle;
import com.microblink.entities.recognizers.blinkbarcode.barcode.BarcodeRecognizer;
import com.microblink.entities.recognizers.blinkid.malaysia.IkadRecognizer;
import com.microblink.entities.recognizers.blinkid.malaysia.MyKadBackRecognizer;
import com.microblink.entities.recognizers.blinkid.malaysia.MyKadFrontRecognizer;
import com.microblink.entities.recognizers.blinkid.mrtd.MrtdRecognizer;
import com.microblink.entities.recognizers.blinkid.mrtd.MrzResult;
import com.microblink.uisettings.ActivityRunner;
import com.microblink.uisettings.UISettings;
import com.tunetalk.ttdealersdk.ocr.entity.ScanResultEntity;
import com.tunetalk.ttdealersdk.ocr.entity.ScanResultItemEntity;
import com.tunetalk.ttdealersdk.util.Common;
import com.tunetalk.ttdealersdk.util.FileUtils;
import com.tunetalk.ttdealersdk.util.Make;

import java.util.Locale;

public class OCRHelper
{
    private static final String DOCUMENT_FILE_PATH = FileUtils.ORC_DIR + "/identity.jpg";
    private static OCRHelper mInstance;

    private Activity mActivity;

    public static synchronized OCRHelper get ()
    {
        if (mInstance == null)
            mInstance = new OCRHelper();

        return mInstance;
    }

    /**
     * Configure MyKad recognizer
     *
     * @return RecognizerBundle
     */
    public RecognizerBundle buildMyKadElement ()
    {
        MyKadFrontRecognizer mykadFront = new MyKadFrontRecognizer();
        OCRSettings.configureImageSettings(mykadFront);

        MyKadBackRecognizer mykadBack = new MyKadBackRecognizer();
        OCRSettings.configureImageSettings(mykadBack);

        return prepareRecognizerBundle(mykadFront, mykadBack);
    }

    /**
     * Configure iKad recognizer
     *
     * @return RecognizerBundle
     */
    public RecognizerBundle buildIKadElement ()
    {
        IkadRecognizer ikad = new IkadRecognizer();
        OCRSettings.configureImageSettings(ikad);

        return prepareRecognizerBundle(ikad);
    }

    /**
     * Configure MRTD recognizer (e.g.: For passport)
     *
     * @return RecognizerBundle
     */
    public RecognizerBundle buildMrtdElement ()
    {
        MrtdRecognizer mrtdRecognizer = new MrtdRecognizer();
        mrtdRecognizer.setAllowUnverifiedResults(true);
        OCRSettings.configureImageSettings(mrtdRecognizer);

        return prepareRecognizerBundle(mrtdRecognizer);
    }

    /**
     * Configure barcode recognizer
     *
     * @return RecognizerBundle
     */
    public RecognizerBundle buildBarCodeElement ()
    {
        BarcodeRecognizer barcode = new BarcodeRecognizer();
        barcode.setScanCode39(true);
        barcode.setScanCode128(true);
        barcode.setScanInverse(true);
        barcode.setScanAztecCode(true);
        barcode.setScanDataMatrix(true);
        barcode.setScanEan13(true);
        barcode.setScanEan8(true);
        barcode.setScanItf(true);
        barcode.setScanPdf417(true);
        barcode.setScanQrCode(true);
        barcode.setScanUpca(true);
        barcode.setScanUpce(true);

        return prepareRecognizerBundle(barcode);
    }

    /**
     * Add all the required recognizers and form a bundle
     *
     * @param recognizers
     * @return RecognizerBundle
     */
    private RecognizerBundle prepareRecognizerBundle (@NonNull Recognizer<?, ?>... recognizers)
    {
        return new RecognizerBundle(recognizers);
    }

    /**
     * Start the scan activity
     *
     * @param activity
     * @param settings
     * @param resultCode
     */
    public void scan (Activity activity, @NonNull UISettings settings, int resultCode)
    {
        mActivity = activity;
        OCRSettings.configureScanSettings(settings, null);
        ActivityRunner.startActivityForResult(activity, resultCode, settings);
    }

    /**
     * Retrieve the scanning data and images
     *
     * @param mRecognizerBundle
     * @param result
     */
    public void getResult (RecognizerBundle mRecognizerBundle, final OnScanResult result)
    {
        final ScanResultEntity mResultEntity = new ScanResultEntity();
        final ScanResultItemEntity itemEntity = new ScanResultItemEntity();

        if (mRecognizerBundle == null)
        {
            return;
        }

        for (Recognizer r : mRecognizerBundle.getRecognizers())
        {
            if (r instanceof MyKadFrontRecognizer)
            {
                MyKadFrontRecognizer.Result myKad = (MyKadFrontRecognizer.Result) r.getResult();
                if (myKad.getResultState() == Recognizer.Result.State.Valid)
                {
                    try
                    {
                        itemEntity.setDocumentType(DocumentType.MYKAD);

                        if (myKad.getOwnerFullName() != null && ! myKad.getOwnerFullName().isEmpty())
                            itemEntity.setFullName(Common.ProcessName(myKad.getOwnerFullName().replace("\r\n", " ").replace("\n", " ")));

                        if (myKad.getNricNumber() != null && ! myKad.getNricNumber().isEmpty())
                            itemEntity.setNric(myKad.getNricNumber().replaceAll("-", ""));
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }

                    try
                    {
                        String[] tokens = myKad.getOwnerAddress().split("\\s");

                        for (String s : tokens)
                        {
                            try
                            {
                                s.replaceAll("O", "0");
                                s.replaceAll("o", "0");
                                s.replaceAll("l", "1");
                                int x = Integer.parseInt(s);

                                itemEntity.setPoscode(s);

                                String address = myKad.getOwnerAddress().substring(0, myKad.getOwnerAddress().indexOf(s)).replaceAll("\n", " ");
                                itemEntity.setAddress(address);
                            }
                            catch (Exception exp)
                            {
                                exp.printStackTrace();
                            }
                        }
                    }
                    catch (NullPointerException ex)
                    {
                        ex.printStackTrace();
                    }
                }

                try
                {
                    FileUtils.Compress(DOCUMENT_FILE_PATH, myKad.getFullDocumentImage().convertToBitmap());
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                mResultEntity.setMyKad(itemEntity);
            }
            else if (r instanceof MrtdRecognizer)
            {
                MrtdRecognizer.Result mrtd = (MrtdRecognizer.Result) r.getResult();
                if (mrtd.getResultState() == Recognizer.Result.State.Valid)
                {
                    MrzResult passport = mrtd.getMrzResult();
                    if (passport.isMrzParsed())
                    {
                        try
                        {
                            itemEntity
                                .setDocumentType(DocumentType.MRTD)
                                .setFullName(passport.getSecondaryId() + " " + passport.getPrimaryId())
                                .setGender(passport.getGender())
                                .setDob(passport.getDateOfBirth().getOriginalDateString())
                                .setPassportNumber(passport.getDocumentNumber().replaceAll("<", ""));

                            for (String countryCode : Locale.getISOCountries())
                            {
                                Locale obj = new Locale("", countryCode);

                                if (obj.getISO3Country().equals(passport.getNationality()))
                                {
                                    String mNationality = obj.getDisplayName();

                                    if (passport.getNationality().equals("CHN"))
                                        itemEntity.setNationality(passport.getNationality());
                                    else
                                        itemEntity.setNationality(Common.replaceSpecialChar(mNationality));
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.printStackTrace();
                        }
                    }

                    try
                    {
                        FileUtils.Compress(DOCUMENT_FILE_PATH, mrtd.getFullDocumentImage().convertToBitmap());
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                    mResultEntity.setMrtd(itemEntity);
                }
            }
            else if (r instanceof IkadRecognizer)
            {
                IkadRecognizer.Result iKad = (IkadRecognizer.Result) r.getResult();

                if (iKad.getResultState() == Recognizer.Result.State.Valid)
                {
                    try
                    {
                        itemEntity
                            .setDocumentType(DocumentType.IKAD)
                            .setFullName(iKad.getName())
                            .setPassportNumber(iKad.getPassportNumber().replaceAll("<", ""));

                        String[] tokens = iKad.getAddress().split("\\s");
                        for (String s : tokens)
                        {
                            try
                            {
                                s.replaceAll("O", "0");
                                s.replaceAll("o", "0");
                                s.replaceAll("l", "1");
                                int x = Integer.parseInt(s);

                                itemEntity.setPoscode(s);

                                String address = iKad.getAddress().substring(0, iKad.getAddress().indexOf(s)).replaceAll("\n", " ");
                                itemEntity.setAddress(address);
                            }
                            catch (Exception exp)
                            {
                                exp.printStackTrace();
                            }
                        }

                        for (String countryCode : Locale.getISOCountries())
                        {
                            Locale obj = new Locale("", countryCode);

                            if (obj.getISO3Country().equals(iKad.getNationality()))
                                itemEntity.setNationality(obj.getDisplayName());
                        }

                        if (iKad.getSex() != null)
                        {
                            itemEntity.setGender(iKad.getSex());
                        }

                        com.microblink.results.date.Date rawDate = iKad.getDateOfBirth();
                        String date = rawDate.getYear() + "-" + rawDate.getMonth() + "-" + rawDate.getDay();
                        itemEntity.setDob(date);
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }

                    try
                    {
                        FileUtils.Compress(DOCUMENT_FILE_PATH, iKad.getFullDocumentImage().convertToBitmap());
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                    mResultEntity.setiKad(itemEntity);
                }
            }
            else if (r instanceof BarcodeRecognizer)
            {
                BarcodeRecognizer.Result bar = (BarcodeRecognizer.Result) r.getResult();
                if (bar.getResultState() == Recognizer.Result.State.Valid)
                {
                    try
                    {
                        itemEntity
                            .setDocumentType(DocumentType.BARCODE)
                            .setScannedBarCode(bar.getStringData());
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }

                    mResultEntity.setBarcode(itemEntity);
                }
            }
        }

        if (result != null)
        {
            result.onScanResult(mResultEntity);

            Make.ProgressDialog.Show(mActivity);
            new Handler().postDelayed(new Runnable()
            {
                @Override public void run ()
                {
                    Make.ProgressDialog.Dismiss();

                    //Return image result other than barcode document type
                    if (itemEntity.getDocumentType() != null
                        && ! itemEntity.getDocumentType().equals(DocumentType.BARCODE))
                    {
                        result.onImageResult(FileUtils.getBytesFromFile(DOCUMENT_FILE_PATH));
                    }
                }
            }, 1000);

        }
    }
}
