package com.tunetalk.ttdealersdk.util;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Common
{
    //    static final String NAME_PATTERN = "^([A-Z]+[,.]?[@/ ]?|[A-Z]+['-]?)*$";
    public static final String NAME_PATTERN = "^([A-Za-z,.@/ '-]*)*$";
    public static final String NAME_PATTERN_2 = "^([A-Za-z ]*)*$";

    public static final String ALPHABET_DIGIT_PATTERN = "^([A-Za-z0-9 ]*)*$";

    public static boolean IsNIRC (String args, int mCurrentYear)
    {
        String nirc = args;
        String dob = nirc.substring(0, 6);

        int day = Integer.parseInt(dob.substring(4, 6));
        int month = Integer.parseInt(dob.substring(2, 4));
        int year = 0;

        try
        {
            Date deliverDate = getAdjustedDate(dob.substring(0, 2));
            String dateString2 = new SimpleDateFormat("yyyy", Locale.getDefault()).format(deliverDate);
            year = Integer.parseInt(dateString2);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }

        return (day > 0 && day <= 31 && month > 0 && month <= 12 && year > 1900 && year <= mCurrentYear);
    }

    public static boolean IsMobileNumber (String args)
    {
        return (args.length() == 10 || args.length() == 11) && args.charAt(0) == '0' && args.charAt(1) == '1';
    }

    public static String ProcessName (String args)
    {
        args = args.toUpperCase();
        args = args.replaceAll("0", "O")
            .replaceAll("1", "I")
            .replaceAll("2", "Z")
            .replaceAll("3", "B")
            .replaceAll("4", "A")
            .replaceAll("5", "S")
            .replaceAll("6", "E")
            .replaceAll("7", "T")
            .replaceAll("8", "B")
            .replaceAll("9", "Q");
//                .replaceAll("/", "I");

        return args;
    }

    public static boolean IsName (boolean isPassport, String args)
    {
        Pattern p;

        if (! isPassport)
            p = Pattern.compile(NAME_PATTERN);
        else
            p = Pattern.compile(NAME_PATTERN_2);

        Matcher m = p.matcher(args.toUpperCase());
        return m.find();
    }

    public static void setDialogSize (Dialog dialog, WindowManager wm)
    {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displaymetrics);
        int width = (int) (displaymetrics.widthPixels * 0.9);
        int height = (int) (displaymetrics.heightPixels * 0.9);
        dialog.getWindow().setLayout(width, height);
    }

    public static String getFormattedTime (Long millisecond)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        return sdf.format(new Date(millisecond));
    }

    public static String getFormattedDate (Long millisecond)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("ccc, MMM dd yyyy", Locale.getDefault());
        return sdf.format(new Date(millisecond));
    }

    public static Date getAdjustedDate (String year)
    {
        Date date = null;
        try
        {
            date = new SimpleDateFormat("yy", Locale.getDefault()).parse(year);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            //compare the current time and input time
            if (cal.after(Calendar.getInstance()))
            {
                cal.add(Calendar.YEAR, - 100);
                date = cal.getTime();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return date;
    }

    public static String maskMobileNumber (String phoneNumber)
    {
        if (IsMobileNumber(phoneNumber))
        {
            if (phoneNumber.length() == 10)
                phoneNumber = phoneNumber.substring(0, 3) + "****" + phoneNumber.substring(7, 10);
            else if (phoneNumber.length() == 11)
                phoneNumber = phoneNumber.substring(0, 3) + "****" + phoneNumber.substring(7, 11);
        }

        return phoneNumber;
    }

    public static String replaceSpecialChar (String str)
    {
        return str.replaceAll("[^a-zA-Z0-9 ,.]", "").trim();
    }

    public static boolean isValidRegex (String str, String pattern)
    {
        Pattern regex = Pattern.compile(pattern);
        Matcher matcher = regex.matcher(str);
        return matcher.find();
    }

    public static boolean isValidString (String args)
    {
        return args != null && ! args.isEmpty();
    }

    public static <T> boolean isValidList (List<T> argsList)
    {
        return argsList != null && argsList.size() > 0;
    }

    public static <T> boolean isValidArray (T[] array)
    {
        return array != null && array.length > 0;
    }

    public static void restartApp (Context context)
    {
        PackageManager packageManager = context.getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(context.getPackageName());
        ComponentName componentName = intent.getComponent();
        Intent mainIntent = Intent.makeRestartActivityTask(componentName);
        context.startActivity(mainIntent);
        Runtime.getRuntime().exit(0);
    }

    public static void setTextUnderline(TextView textView)
    {
        textView.setPaintFlags(textView.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
    }

    public static String getStringResourceByName (Context context, String key)
    {
        int resId = context.getResources().getIdentifier(key, "string", context.getPackageName());
        if (resId == 0)
        {
            return key;
        }
        else
        {
            return context.getString(resId);
        }
    }

    public static boolean hasConnectivity (Context context)
    {
        try
        {
            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo actInfo = manager.getActiveNetworkInfo();
            return actInfo != null && actInfo.isConnectedOrConnecting();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
    }

    public static String getUUID() throws Exception{
        return UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
    }
}
