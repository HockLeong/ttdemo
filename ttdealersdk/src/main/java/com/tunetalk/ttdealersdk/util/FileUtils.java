package com.tunetalk.ttdealersdk.util;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;

import com.orhanobut.logger.Logger;
import com.tunetalk.ttdealersdk.activity.MySurfaceView;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileUtils
{
    public static String ORC_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + "/.OCR";
    public static String TEST_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Debug/";
    public static String TEST_FILE = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Debug/" + System.currentTimeMillis() + ".txt";

    public static final int JPEG_QUALITY = 75;

    public static void MakeOCRFolder ()
    {
        String output = FileUtils.ORC_DIR;
        File f = new File(output);
        if (! f.exists())
        {
            f.mkdirs();
        }
    }

    public static void MakeTestFolder ()
    {
        String output = FileUtils.TEST_DIR;
        File f = new File(output);
        if (! f.exists())
        {
            f.mkdirs();
        }
    }

    public static void ClearFolder(final File f)
    {
        new AsyncTask<Void, Void, Void>()
        {
            @Override protected Void doInBackground (Void... params)
            {
                if (f.isDirectory())
                {
                    String[] children = f.list();

                    for (int i = 0; i < children.length; i++)
                    {
                        File file = new File(f, children[i]);

                        if (file.isDirectory())
                        {
                            String[] grandChildren = file.list();

                            for (int j = 0; j < grandChildren.length; j++)
                                new File(file, grandChildren[j]).delete();

                            file.delete();
                        }
                        else
                            file.delete();
                    }

                    f.delete();
                }
                return null;
            }
        }.execute();
    }

    public static void ClearOCRFolder ()
    {
        new AsyncTask<Void, Void, Void>()
        {
            @Override protected Void doInBackground (Void... params)
            {
                File f = new File(ORC_DIR);

                if (f.isDirectory())
                {
                    String[] children = f.list();

                    for (int i = 0; i < children.length; i++)
                    {
                        File file = new File(f, children[i]);

                        if (file.isDirectory())
                        {
                            String[] grandChildren = file.list();

                            for (int j = 0; j < grandChildren.length; j++)
                                new File(file, grandChildren[j]).delete();

                            file.delete();
                        }
                        else
                            file.delete();
                    }

                    f.delete();
                }
                return null;
            }
        }.execute();
    }

    public static byte[] getBytesFromFile (String path)
    {
        File file = new File(path);
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try
        {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();

            return bytes;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            return null;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] AddWatermark (Activity act, byte[] imageBuffer)
    {
        try
        {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            Bitmap watermark = BitmapFactory.decodeStream(act.getResources().getAssets().open("watermark.png"));
            Bitmap photo = Bitmap.createScaledBitmap(BitmapFactory.decodeByteArray(imageBuffer, 0, imageBuffer.length),
                MySurfaceView.PICTURE_WIDTH, MySurfaceView.PICTURE_HEIGHT, true);

            watermark = Bitmap.createScaledBitmap(watermark, Math.round(photo.getWidth()), Math.round(photo.getHeight()), true);

            Bitmap mWaterMarkImage = Bitmap.createBitmap(photo.getWidth(), photo.getHeight(),
                photo.getConfig() != null ? photo.getConfig() : Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(mWaterMarkImage);
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            canvas.drawBitmap(photo, 0, 0, paint);
            canvas.drawBitmap(watermark,
                (float) (photo.getWidth() / 2) - (watermark.getWidth() / 2),
                (float) (photo.getHeight() / 2) - (watermark.getHeight() / 2), paint);

            mWaterMarkImage.compress(Bitmap.CompressFormat.JPEG, MySurfaceView.JPEG_QUALITY, outputStream);
            byte[] buffer = outputStream.toByteArray();

            outputStream.flush();
            watermark.recycle();
            photo.recycle();
            mWaterMarkImage.recycle();

            return buffer;
        }
        catch (IOException io)
        {
            io.printStackTrace();
            return null;
        }
    }

    public static void copy (File src, File dst)
    {
        try
        {
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dst);

            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0)
            {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public static String getFilePath (Context context, Uri uri)
    {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = context.getContentResolver().query(
            uri, filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String filePath = cursor.getString(columnIndex);
        cursor.close();
        return filePath;
    }

    public static String toBase64(byte[] buff)
    {
        return Base64.encodeToString(buff, Base64.NO_WRAP);
    }

    public static String getImageBase64 (String path)
    {
        Logger.d(path);
        File file = new File(path);
        Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, MySurfaceView.JPEG_QUALITY, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();

        return Base64.encodeToString(b, Base64.NO_WRAP);
    }

    public static void Compress (String filename, Bitmap bitmap)
    {
        FileOutputStream fos = null;
        try
        {
            fos = new FileOutputStream(filename);
            boolean success = bitmap.compress(Bitmap.CompressFormat.JPEG, FileUtils.JPEG_QUALITY, fos);
            if (! success)
            {
                com.microblink.util.Log.e("Debug", "Failed to compress bitmap!");
                if (fos != null)
                {
                    try
                    {
                        fos.close();
                    }
                    catch (IOException ignored)
                    {

                    }
                    finally
                    {
                        fos = null;
                    }
                    new File(filename).delete();
                }
            }
        }
        catch (FileNotFoundException e)
        {
            com.microblink.util.Log.e("Debug", e, "Failed to save image");
        }
        finally
        {
            if (fos != null)
            {
                try
                {
                    fos.close();
                }
                catch (IOException ignored)
                {
                    ignored.printStackTrace();
                }
            }
        }
    }
}
