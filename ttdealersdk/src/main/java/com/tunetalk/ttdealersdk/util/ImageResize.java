package com.tunetalk.ttdealersdk.util;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;

import com.orhanobut.logger.Logger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * Created by jasonhor on 9/29/15.
 */
public class ImageResize {
    private static final int MAX_HEIGHT = 800;
    private static final int MAX_WIDTH = 800;

    public static boolean resizeForUpload(Uri imageUri) {
        File file = new File(imageUri.getPath());
        Bitmap b = BitmapFactory.decodeFile(file.getAbsolutePath());
        Bitmap out = null;

        FileOutputStream fOut;
        try {
//            if (b.getHeight() > b.getWidth()) {
//                out = Bitmap.createScaledBitmap(b, 768, 1024, false);
//            } else if (b.getHeight() == b.getWidth()){
//                out = Bitmap.createScaledBitmap(b, 600, 600, false);
//            }else {
//                out = Bitmap.createScaledBitmap(b, 1024, 768, false);
//            }

            int imgWidth = b.getWidth();
            int imgHeight = b.getHeight();
            int newHeight = (imgHeight * 1200) / imgWidth;

            out = Bitmap.createScaledBitmap(b, 1200, newHeight, false);
            fOut = new FileOutputStream(file);
            out.compress(Bitmap.CompressFormat.JPEG, 90, fOut);
            fOut.flush();
            fOut.close();
            b.recycle();
            out.recycle();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static byte[] resizeAndGetBytes(Uri imageUri)
    {
        File file = new File(imageUri.getPath());
        Bitmap b = BitmapFactory.decodeFile(file.getAbsolutePath());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int imgWidth = b.getWidth();
        int imgHeight = b.getHeight();
        int newHeight = (imgHeight * MAX_HEIGHT) / imgWidth;

        Bitmap.createScaledBitmap(b, MAX_WIDTH, newHeight, false).compress(Bitmap.CompressFormat.JPEG, 60, baos);
        return baos.toByteArray();
    }

    public static void rotateImage(final Activity mActivity, final String path) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                File file = new File(path);
                BitmapFactory.Options opts = new BitmapFactory.Options();
                opts.inJustDecodeBounds = false;
                opts.inPreferredConfig = Bitmap.Config.RGB_565;
                opts.inDither = true;

                Bitmap b = BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
                try {
                    ExifInterface ei = new ExifInterface(path);
                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    Matrix matrix = new Matrix();

                    Logger.d("orientation: "+orientation);
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            matrix.postRotate(90);
                            b = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            matrix.postRotate(180);
                            b = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            matrix.postRotate(270);
                            b = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
                            break;
                        default:
                            b = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
                            break;
                    }

                    FileOutputStream fOut = new FileOutputStream(path);
                    b.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                    fOut.flush();
                    fOut.close();
                    b.recycle();
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private static Bitmap rotateImageIfRequired(Context context,Bitmap img, Uri selectedImage) {

        // Detect rotation
        int rotation=getRotation(context, selectedImage);
        if(rotation!=0){
            Matrix matrix = new Matrix();
            matrix.postRotate(rotation);
            Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
            img.recycle();
            return rotatedImg;
        }else{
            return img;
        }
    }

    private static int getRotation(Context context,Uri selectedImage) {
        int rotation =0;
        ContentResolver content = context.getContentResolver();


        Cursor mediaCursor = content.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{"orientation", "date_added"}, null, null, "date_added desc");

        if (mediaCursor != null && mediaCursor.getCount() !=0 ) {
            while(mediaCursor.moveToNext()){
                rotation = mediaCursor.getInt(0);
                break;
            }
        }
        mediaCursor.close();
        return rotation;
    }

    public static void decodeSampledBitmap(Context context, Uri selectedImage) {

        try {
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
            BitmapFactory.decodeStream(imageStream, null, options);
            imageStream.close();

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            imageStream = context.getContentResolver().openInputStream(selectedImage);
            Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

            img = rotateImageIfRequired(context, img, selectedImage);

            FileOutputStream fOut = new FileOutputStream(selectedImage.getPath());
            img.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.flush();
            fOut.close();
            img.recycle();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
