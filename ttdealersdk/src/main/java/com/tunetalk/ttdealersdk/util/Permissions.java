package com.tunetalk.ttdealersdk.util;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;

import java.util.List;

public class Permissions
{
    public static boolean addPermission (Activity activity, List<String> permissionsList, String permission)
    {
        if(Build.VERSION.SDK_INT >= 23)
        {
            if (activity.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED)
            {
                permissionsList.add(permission);
                if (! activity.shouldShowRequestPermissionRationale(permission))
                    return false;
            }
            return true;
        }
        return true;
    }

    public static boolean canAskPermission(Activity activity, String permission)
    {
        if(Build.VERSION.SDK_INT >= 23)
        {
            return activity.shouldShowRequestPermissionRationale(permission);
        }
        return true;
    }

    public static boolean hasPermission(Activity activity, String permission)
    {
        if(Build.VERSION.SDK_INT >= 23)
        {
            return activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

    public static boolean hasPermission (Activity activity, String... permissions)
    {
        if (Build.VERSION.SDK_INT >= 23 && permissions != null)
        {
            for (String permission : permissions)
            {
                if (activity.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED)
                    return false;
            }
//            return activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
        }

        return true;
    }

    public static boolean hasPermissions(Activity activity, List<String> permissions)
    {
        if(Build.VERSION.SDK_INT >= 23)
        {
            for(String s : permissions)
            {
                if(activity.checkSelfPermission(s) == PackageManager.PERMISSION_DENIED)
                    return false;
            }

            return true;
        }
        else
            return true;
    }

    public static final int CAMERA_PERMISSION_CODE = 99;
    public static final int ACCESS_STORAGE_CODE = 100;
    public static final int RECEIVE_SMS_CODE = 101;
    public static final int ACCEESS_LOCATION_CODE = 102;
    public static final int PHONE_PERMISSION_CODE = 103;
    public static final int READ_CONTACT_PERMISSION_CODE = 104;
    public static final int READ_PHONE_STATE_CODE =105;

    public static final String LOCATION_COARSE = Manifest.permission.ACCESS_COARSE_LOCATION;
    public static final String LOCATION_FINE = Manifest.permission.ACCESS_FINE_LOCATION;
    public static final String STORAGE_READ = Manifest.permission.READ_EXTERNAL_STORAGE;
    public static final String STORAGE_WRITE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public static final String CAMERA = Manifest.permission.CAMERA;
    public static final String READ_CONTACT = Manifest.permission.READ_CONTACTS;
    public static final String READ_PHONE_STATE = Manifest.permission.READ_PHONE_STATE;
    public static final String RECEIVE_SMS = Manifest.permission.RECEIVE_SMS;
    public static final String READ_SMS = Manifest.permission.READ_SMS;
    public static final String CALL_PHONE = Manifest.permission.CALL_PHONE;
}
