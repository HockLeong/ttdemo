package com.tunetalk.ttdealersdk.util;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class Constant
{
    private static SimpleDateFormat API_DATE_TIME_PARSER = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
    private static SimpleDateFormat API_DATE_PARSER = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private static SimpleDateFormat API_BIRTHDATE_PARSER = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());

    public interface Settings
    {
        Locale LOCALE = Locale.getDefault();
        String PLATFORM = "Android";
    }

    public interface Key
    {
        interface Intent
        {
            String REGISTRATION_ID = "registrationId";
            String API_KEY = "API_KEY";
            String ICCID = "simNumber";
            String MSISDN = "msisdn";
            String ERROR_DESCRIPTION = "errorDescription";
            String IS_SELECT_NUMBER = "IS_SELECT_NUMBER";

            String SIM_REGISTRATION_REQUEST_BODY = "SIMRegistrationEntity";
            String PORT_IN_REQUEST_BODY = "PortInEntity";

            String SERVICE_CATEGORIES = "SERVICE_CATEGORIES";
        }

        interface Common
        {
            String APIKEY = "apiKey";
        }
    }

    public interface Preferences
    {
        String OFFLINE_DATA = "OFFLINE_DATA";
    }

    public interface ResultCode
    {
        int SUCCESSFUL = 0;
        int USER_CANCELLED = 1;
        int FAILED =2;
        int OFFLINE_SERVICE = 3;
    }

    public interface RequestCode
    {
        int OKU_REQUEST_CODE = 1040;

        int SCAN_DOCUMENT_REQUEST_CODE = -258810022;
    }
}
