package com.tunetalk.ttdealersdk.util;

import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.tunetalk.ttdealersdk.base.BaseEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class RESTAsync extends AsyncTask<Void, Void, Void>
{
    private static int CONNECT_TIME_OUT = 60 * 1000;
    private static int READ_TIME_OUT = 60 * 1000;
    private HttpRequest mHttpRequest;
    private RequestMode mRequestMode;
    private JSONObject mJSONObject;
    private String mURL, mJSONString;
    @Nullable
    private String mSession;

    public void setParameter (String url, RequestMode mode)
    {
        mURL = url;
        mRequestMode = mode;
    }

    public void setParameter (String session, String url, RequestMode mode)
    {
        mSession = session;
        mURL = url;
        mRequestMode = mode;
    }

    public void setParameter (String session, String url, JSONObject json, RequestMode mode)
    {
        mURL = url;
        mRequestMode = mode;
        mJSONObject = json;
        mSession = session;
    }

    @Override
    protected Void doInBackground (Void... params)
    {
        try
        {
            if (mRequestMode == RequestMode.GET)
            {
                mHttpRequest = HttpRequest
                    .get(mURL)
                    .header("Session", mSession)
                    .contentType(HttpRequest.CONTENT_TYPE_JSON)
                    .connectTimeout(CONNECT_TIME_OUT)
                    .readTimeout(READ_TIME_OUT);
            }
            else if (mRequestMode == RequestMode.POST)
            {
                mHttpRequest = HttpRequest
                    .post(mURL)
                    .header("Session", mSession)
                    .contentType(HttpRequest.CONTENT_TYPE_JSON)
                    .send(mJSONObject.toString())
                    .connectTimeout(CONNECT_TIME_OUT)
                    .readTimeout(READ_TIME_OUT);
            }
            else if (mRequestMode == RequestMode.DELETE)
            {
                mHttpRequest = HttpRequest
                    .delete(mURL)
                    .header("Session", mSession)
                    .contentType(HttpRequest.CONTENT_TYPE_JSON)
                    .connectTimeout(CONNECT_TIME_OUT)
                    .readTimeout(READ_TIME_OUT);

                if (mJSONObject != null)
                    mHttpRequest.send(mJSONObject.toString());
            }

            Log.e("Debug", mURL);
            mJSONString = mHttpRequest.body();

            if (mSession == null && mJSONString.contains("Request Successful"))
                mSession = mHttpRequest.header("Session");

            BaseEntity baseEntity = new GsonBuilder().create().fromJson(mJSONString, BaseEntity.class);
            if (baseEntity != null && baseEntity.getMessage().equalsIgnoreCase("Authentication Failed!"))
            {
//                BaseActivity.getInstance().backToLoginPage();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public String getResponse ()
    {
        return mJSONString;
    }

    public String getJSON ()
    {
        if (mJSONString == null)
        {
            return null;
        }
        else
        {
            if (isJSONValid(mJSONString))
                return mJSONString;
            else
                return null;
        }

    }

    public boolean isJSONValid (String test)
    {
        try
        {
            new JSONObject(test);
        }
        catch (JSONException ex)
        {
            try
            {
                new JSONArray(test);
            }
            catch (JSONException ex1)
            {
                return false;
            }
        }
        return true;
    }

    public String getSession ()
    {
        return mSession;
    }

    public enum RequestMode
    {
        GET,
        POST,
        DELETE
    }

}
