package com.tunetalk.ttdealersdk.util;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.tunetalk.ttdealersdk.R;

public class CustomDialog
{
    private Handler mHandler;
    private Runnable mRunnable;
    private DialogFragment mDialogFragment;
    private FragmentManager mFragmentManager;
    private Dialog.Builder mDialogBuilder;
    private String mDialogMessage;
    private ObjectAnimator mObjectAnimator;
    private boolean mEnableAnimation = false;
    private boolean mDialogCancelable = true, mDialogCancleOnTouchOutside = true;
    private int mCount = 0;

    public CustomDialog()
    {
        mDialogBuilder = new Dialog.Builder(com.rey.material.R.style.Material_App_Dialog_Simple_Light)
        {
            @Override
            protected void onBuildDone(Dialog dialog)
            {
                dialog.layoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.setCancelable(mDialogCancelable);
                dialog.setCanceledOnTouchOutside(mDialogCancleOnTouchOutside);
                final TextView tvMessage = (TextView) dialog.findViewById(R.id.dialog_loading_tvMessage);
                tvMessage.setText(mDialogMessage);

                if(mEnableAnimation)
                {
                    mHandler = new Handler();
                    mRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            ++mCount;
                            tvMessage.setText(tvMessage.getText().toString() + ".");

                            if(mCount <= 3)
                                mHandler.postDelayed(this, 1000);
                            else
                            {
                                mCount = 0;
                                tvMessage.setText(mDialogMessage);
                                mHandler.postDelayed(this, 1000);
                            }
                        }
                    };

                    mHandler.post(mRunnable);
                }

                if(mObjectAnimator != null)
                {
                    mObjectAnimator.setTarget(tvMessage);
                    mObjectAnimator.setEvaluator(new ArgbEvaluator());
                    mObjectAnimator.start();
                }
            }
        };
    }

    public CustomDialog with(FragmentManager manager)
    {
        mFragmentManager = manager;
        return this;
    }

    public CustomDialog positiveAction(String actionText)
    {
        mDialogBuilder.positiveAction(actionText);
        return this;
    }

    public CustomDialog negativeAction(String actionText)
    {
        mDialogBuilder.negativeAction(actionText);
        return this;
    }

    public CustomDialog setMessage(String message)
    {
        mDialogMessage = message;
        return this;
    }

    public CustomDialog setTitle(String title)
    {
        mDialogBuilder.title(title);
        return this;
    }

    public CustomDialog setContentView(int layoutId)
    {
        mDialogBuilder.contentView(layoutId);
        return this;
    }

    public CustomDialog withLoadingAnimation(boolean enable)
    {
        mEnableAnimation = enable;
        return this;
    }

    public CustomDialog withMessageColorAnimation(ObjectAnimator animator)
    {
        mObjectAnimator = animator;
        return this;
    }

    public CustomDialog setCancledOnTouchOutside(boolean b)
    {
        mDialogCancleOnTouchOutside = b;
        return this;
    }

    public CustomDialog setCancelable(boolean b)
    {
        mDialogCancelable = b;
        return this;
    }

    public CustomDialog build()
    {
        return this;
    }

    public void show()
    {
        mDialogFragment = DialogFragment.newInstance(mDialogBuilder);
        mDialogFragment.show(mFragmentManager, null);
    }

    public void dismiss()
    {
        if(mEnableAnimation)
            mHandler.removeCallbacks(mRunnable);
        try{
        mDialogFragment.dismiss();
        }catch (Exception e){

        }
    }

    public DialogFragment getDialogFragment()
    {
        return mDialogFragment;
    }
}
