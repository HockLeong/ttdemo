package com.tunetalk.ttdealersdk.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPreferencesUtils
{
    private static SharedPreferences getSharedPreferences (Context context)
    {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void insert (Context context, String key, String value)
    {
        getSharedPreferences(context).edit()
            .putString(key, value)
            .apply();
    }

    public static void insert (Context context, String key, long value)
    {
        getSharedPreferences(context).edit()
            .putLong(key, value)
            .apply();
    }

    public static void insert (Context context, String key, boolean value)
    {
        getSharedPreferences(context).edit()
            .putBoolean(key, value)
            .apply();
    }

    public static void delete (Context context, String key)
    {
        getSharedPreferences(context).edit()
            .remove(key)
            .apply();
    }

    public static String Get (Context context, String preferenceName, String key)
    {
        SharedPreferences retrieveuserdata = context.getSharedPreferences(preferenceName,
            Context.MODE_PRIVATE);

        return retrieveuserdata.getString(key, "");
    }

    public static String getString (Context context, String key)
    {
        return getSharedPreferences(context).getString(key, "");
    }

    public static Long getLong (Context context, String key)
    {
        return getSharedPreferences(context).getLong(key, - 1);
    }

    public static Boolean getBoolean (Context context, String key)
    {
        return getSharedPreferences(context).getBoolean(key, false);
    }
}
