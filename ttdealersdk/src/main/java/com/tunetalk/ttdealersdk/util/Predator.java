package com.tunetalk.ttdealersdk.util;

import java.io.DataInputStream;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class Predator
{
    private RSAPublicKey mRSAPublicKey;
    private RSAPrivateKey mRSAPrivateKey;

    public byte[] decryptRSA(byte[] encrypted, String cipherAlgorithm) throws Exception
    {
        Cipher cipher = Cipher.getInstance(cipherAlgorithm);
        cipher.init(Cipher.DECRYPT_MODE, mRSAPrivateKey);

        byte[] decryptedInByte = cipher.doFinal(encrypted);
        return decryptedInByte;
    }

    public byte[] encryptRSA(String string, String cipherAlgorithm) throws Exception
    {
        Cipher cipher = Cipher.getInstance(cipherAlgorithm);
        cipher.init(Cipher.ENCRYPT_MODE, mRSAPublicKey);

        byte[] encryptedInByte = cipher.doFinal(string.getBytes());
        return encryptedInByte;
    }

    public Predator setRSAPublicKey(InputStream input, String keyAlgorithm) throws Exception
    {
        DataInputStream dis = new DataInputStream(input);
        byte[] keyBytes = new byte[input.available()];
        dis.readFully(keyBytes);
        dis.close();

        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance(keyAlgorithm);
        mRSAPublicKey = (RSAPublicKey) kf.generatePublic(spec);

        return this;
    }

    public Predator setRSAPrivateKey(InputStream input, String keyAlgorithm) throws Exception
    {
        DataInputStream dis = new DataInputStream(input);
        byte[] keyBytes = new byte[input.available()];
        dis.readFully(keyBytes);
        dis.close();

        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance(keyAlgorithm);
        mRSAPrivateKey = (RSAPrivateKey) kf.generatePrivate(spec);
        return this;
    }


    byte[] mSecretKey, mIVs;
    SecretKeySpec mSecretKeySpec;
    String mAlgorithm, mSecureRandomAlgorithm, mSecureRandomProvider;

    public Predator setup(String algorithm, String secureRandomAlgorithm, String secureRandomprovider)
    {
        this.mAlgorithm = algorithm;
        this.mSecureRandomAlgorithm = secureRandomAlgorithm;
        this.mSecureRandomProvider = secureRandomprovider;
        return this;
    }

    public Predator madeSecretKey(byte[] password, int keySize) throws Exception
    {
        SecureRandom secureRandom = SecureRandom.getInstance(mSecureRandomAlgorithm, mSecureRandomProvider);
        secureRandom.setSeed(password);

        KeyGenerator kgen = KeyGenerator.getInstance(mAlgorithm);
        kgen.init(keySize, secureRandom);

        SecretKey skey = kgen.generateKey();
        mSecretKey = skey.getEncoded();

        return this;
    }

    public Predator madeSecretKeySepec()
    {
        mSecretKeySpec = new SecretKeySpec(mSecretKey, mAlgorithm);
        return this;
    }

    public byte[] encryptAES(byte[] data) throws Exception
    {
        Cipher cipher = Cipher.getInstance("AES/ECB/ZeroBytePadding");
        cipher.init(Cipher.ENCRYPT_MODE, mSecretKeySpec);
        byte[] encrypted = cipher.doFinal(data);
        return encrypted;
    }

    public byte[] decryptAES(byte[] encrypted) throws Exception
    {
        Cipher cipher = Cipher.getInstance("AES/ECB/ZeroBytePadding");
        cipher.init(Cipher.DECRYPT_MODE, mSecretKeySpec);
        byte[] decrypted = cipher.doFinal(encrypted);
        return decrypted;
    }


    public interface Algorithm
    {
        String RSA_NONE_PKCS1PADDING = "RSA/NONE/PKCS1Padding";
        String RSA_NONE_NOPADDING = "RSA/NONE/NoPadding";
        String RSA = "RSA";
        String AES = "AES";
    }

    public interface Key
    {
        String RSA = "RSA";
        String AES = "AES";
    }

    public interface SecRandom
    {
        String ALGORITHM_SHA1PRNG = "SHA1PRNG";
        String PROVIDER_CRYPTO = "Crypto";
    }
}
