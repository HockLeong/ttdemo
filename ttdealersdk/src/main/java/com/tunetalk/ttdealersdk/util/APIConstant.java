package com.tunetalk.ttdealersdk.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class APIConstant
{
    public static boolean isProductionServer;
    public static  String SERVER_HOST = "SERVER_HOST";
    public static final int SCHEDULE_INTERVAL = 1800000; //30 Minutes
    public static final String CODE_SUCCESS = "200", CODE_FAILURE = "01";

    public static final String APPLICATION_ID = "Eo1Tdji6OYs5BhMzA5W2seX8UwGemOqLENJMhyE4";
    public static final String CLIENT_KEY = "2BgAojJySQCtwUrZTY0DZeQ0upaMmRx2kDoBzVXq";

    //V2
    public static final String FORTUNE_WHEEL_FRONT_STAGING_URL_V2 = "https://dealer-dev.tunetalk.net/wheel2/?apiKey=%s";
    public static final String FORTUNE_WHEEL_FRONT_PROD_URL_V2 = "https://dealer-app.tunetalk.net/wheel2/?apiKey=%s";

    public static final String FORTUNE_WHEEL_STAGING_URL_V2 = "https://dealer-dev.tunetalk.net/wheel2/pages/wheel_of_fortune/index.html?apiKey=%1$s&type=%2$s";
    public static final String FORTUNE_WHEEL_PROD_URL_V2 = "https://dealer-app.tunetalk.net/wheel2/pages/wheel_of_fortune/index.html?apiKey=%1$s&type=%2$s";

    final String LiveChatURL = "http://211.25.82.22:8081/chatv2/phplive.php";
    final String DeveloperURL = "http://www.nexstream.com.my/";

    final String DemoURL = "https://dealer-dev.tunetalk.net/api/";
    final String ProdURL = "https://dealer-app.tunetalk.net/api/";

    private String SelectedURL;

    public APIConstant ()
    {
//        SelectedURL = (BuildConfig.APP_MODE == 1) ? ProdURL : DemoURL;
//        SelectedFAQURL = (BuildConfig.APP_MODE == 1) ? FAQProdURL : FAQDemoURL;

    }

    public APIConstant(Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int serverHostMode = sharedPreferences.getInt(SERVER_HOST, 1);

//        Log.e("Debug", "Mode: " + serverHostMode);
        SelectedURL = (serverHostMode == 1) ? ProdURL : DemoURL;
    }

    public String getSelectedURL ()
    {
        return SelectedURL;
    }

    public String getDeveloperURL ()
    {
        return DeveloperURL;
    }

    public String getLoginURL ()
    {
        return SelectedURL + "login";
    }

    public String getLogoutURL ()
    {
        return SelectedURL + "logout";
    }

    public String getPromotionURL ()
    {
        return SelectedURL + "promotion";
    }

    public String getLiveChatURL ()
    {
        return LiveChatURL;
    }

    public String getPushURL ()
    {
        return SelectedURL + "push";
    }

    public String getSetPushURL ()
    {
        return SelectedURL + "pushSet";
    }

    public String getSIMRegistrationURL ()
    {
        return SelectedURL + "simReg";
    }

    public String getPortInURL ()
    {
        return SelectedURL + "simPort";
    }

    public String getSIMReplacementURL ()
    {
        return SelectedURL + "simRepl";
    }

    public String getPortInStatusURL ()
    {
        return SelectedURL + "portInStatus";
    }

    public String getTopupValuesURL ()
    {
        return SelectedURL + "topupValues";
    }

    public String getBalanceURL ()
    {
        return SelectedURL + "balance";
    }

    public String getFortueWheelURL ()
    {
        return SelectedURL + "fortune";
    }

    public String getTAndCURL ()
    {
        return SelectedURL + "acceptTnc";
    }

    public String getHistoryURL (int type)
    {
        return type == 0 ? SelectedURL + "history/register" : type == 1 ? SelectedURL + "history/replacement" : type == 2 ? SelectedURL + "history/portIn" : SelectedURL + "history/topup";
    }

    public String getSubscriptionURL ()
    {
        return SelectedURL + "subscriptionPlan";
    }

    public String getSubscriptionResponseURL ()
    {
        return SelectedURL + "subscription";
    }

    public String getTopUpURL ()
    {
        return SelectedURL + "topup";
    }

    public String getValidateURL ()
    {
        return SelectedURL + "validate";
    }

    public String getDealerRegisterURL ()
    {
        return SelectedURL + "dealer/registration";
    }

    public boolean isProduction (Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int serverHostMode = sharedPreferences.getInt(SERVER_HOST, 1);

        return serverHostMode != 0;
//        return BuildConfig.APP_MODE == 0 ? false : true;
    }

    public String getUpdateDeviceIDURL ()
    {
        return SelectedURL + "updateDeviceId";
    }

    public String getRolePermissionURL ()
    {
        return SelectedURL + "rolePermission";
    }

    public String getPurchaseHistoryURL ()
    {
        return SelectedURL + "history/purchase_history";
    }

    public String getInvoiceURL ()
    {
        return SelectedURL + "history/purchase_history/%1$d/download?session=%2$s";
    }

    public String getProductTypeURL ()
    {
        return SelectedURL + "product/%s";
    }

    public String getPackageURL ()
    {
        return SelectedURL + "package";
    }

    public String getCreatePackageURL ()
    {
        return SelectedURL + "package/create";
    }

    public String getDeletePackageURL ()
    {
        return SelectedURL + "package/%d";
    }

    public String getPurchasePackageURL ()
    {
        return SelectedURL + "purchase";
    }

    public String getAddressURL ()
    {
        return SelectedURL + "dealer/address";
    }

    public String getProfileURL ()
    {
        return SelectedURL + "dealer";
    }

    public String getUpdateProfileURL ()
    {
        return SelectedURL + "dealer/update";
    }

    public String getSimNumberURL ()
    {
        return SelectedURL + "simNumber/%s";
    }

    public String getPreferedSimNumberURL ()
    {
        return SelectedURL + "preferNumber/%s";
    }

    public String getFortuneHelpURL ()
    {
        return SelectedURL + "fortune/help";
    }

    public String getPortInHistoryDetailsURL ()
    {
        return SelectedURL + "portInStatus/%d";
    }

    public String getPortInResubmitURL ()
    {
        return SelectedURL + "simResubmitPortIn/%d";
    }

    public String getFortuneWheelPreviewURL(Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int serverHostMode = sharedPreferences.getInt(SERVER_HOST, 1);

        return (serverHostMode == 1) ? FORTUNE_WHEEL_FRONT_PROD_URL_V2 : FORTUNE_WHEEL_FRONT_STAGING_URL_V2;
//        return (BuildConfig.APP_MODE == 1) ? FORTUNE_WHEEL_FRONT_PROD_URL_V2 : FORTUNE_WHEEL_FRONT_STAGING_URL_V2;
    }

    public String getFortuneWebV2URL (Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int serverHostMode = sharedPreferences.getInt(SERVER_HOST, 1);

        return (serverHostMode== 1) ? FORTUNE_WHEEL_PROD_URL_V2 : FORTUNE_WHEEL_STAGING_URL_V2;
//        return (BuildConfig.APP_MODE == 1) ? FORTUNE_WHEEL_PROD_URL_V2 : FORTUNE_WHEEL_STAGING_URL_V2;
    }

    public String getCancelPortInURL()
    {
        return SelectedURL + "simCancelPortIn";
    }

    public String getUpdatePackageNameURL(){
        return SelectedURL + "package/%d/edit";
    }

    public String getAnalyticURL(){
        return SelectedURL + "report";
    }
}
