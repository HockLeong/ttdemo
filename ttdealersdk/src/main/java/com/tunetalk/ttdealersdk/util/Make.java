package com.tunetalk.ttdealersdk.util;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.Window;

import com.tunetalk.ttdealersdk.R;


public class Make
{
    public static class ProgressDialog
    {
        private static android.app.ProgressDialog mProgressDialog;

        public static void Show (Context context)
        {
            try
            {
                if (mProgressDialog != null && mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            try
            {
                mProgressDialog = new android.app.ProgressDialog(context);
                mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mProgressDialog.setMessage(context.getString(R.string.common_text_loading));
                mProgressDialog.setCancelable(false);
                mProgressDialog.setIndeterminate(false);
                mProgressDialog.show();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        public static void Dismiss ()
        {
            try
            {
                if (mProgressDialog != null && mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    public static AlertDialog.Builder dialogBuilder (Activity activity, Object msg)
    {
        String m;

        if (msg instanceof Integer)
            m = activity.getResources().getString((int) msg);
        else
            m = (String) msg;

        return new AlertDialog.Builder(activity)
            .setPositiveButton(activity.getString(R.string.btn_okay), null)
            .setMessage(Common.getStringResourceByName(activity, m));
    }
}
