package com.tunetalk.ttdealersdk.util;

import android.app.Activity;
import android.content.Intent;

public class ActivityUtils
{
    public static void result (Activity activity, int resultCode, String errorMessage)
    {
        Intent i = activity.getIntent();

        if (Common.isValidString(errorMessage))
            i.putExtra("errorDescription", Common.getStringResourceByName(activity, errorMessage));

        activity.setResult(resultCode, i);
        activity.finish();
    }

    public static void result (Activity activity, int resultCode, String msisdn, String errorMessage)
    {
        Intent i = activity.getIntent();

        if (Common.isValidString(msisdn))
            i.putExtra("msisdn", msisdn);

        if (Common.isValidString(errorMessage))
            i.putExtra("errorDescription", Common.getStringResourceByName(activity, errorMessage));

        activity.setResult(resultCode, i);
        activity.finish();
    }

}
