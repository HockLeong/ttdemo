package com.tunetalk.ttdealersdk.entity;


public class BaseResp
{
    private String apiKey, code, message;

    public String getApiKey ()
    {
        return apiKey;
    }

    public void setApiKey (String apiKey)
    {
        this.apiKey = apiKey;
    }

    public String getCode ()
    {
        return code;
    }

    public boolean getResultCode()
    {
        return code.equals("200");
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }
}