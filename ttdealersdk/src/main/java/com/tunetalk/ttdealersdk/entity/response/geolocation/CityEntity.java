package com.tunetalk.ttdealersdk.entity.response.geolocation;

public class CityEntity
{
    String CityName;
    int StateID;

    public String getCityName()
    {
        return CityName;
    }

    public int getStateID()
    {
        return StateID;
    }
}
