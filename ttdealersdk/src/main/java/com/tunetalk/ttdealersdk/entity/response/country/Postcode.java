package com.tunetalk.ttdealersdk.entity.response.country;

/**
 * Created by user on 17/05/16.
 */
public class Postcode {
    private String postcode, cityName;
    private int stateId;

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    @Override
    public String toString() {
        return "Postcode{" +
                "postcode='" + postcode + '\'' +
                ", cityName='" + cityName + '\'' +
                ", stateId='" + stateId + '\'' +
                '}';
    }
}
