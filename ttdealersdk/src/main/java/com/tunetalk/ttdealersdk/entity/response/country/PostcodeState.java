package com.tunetalk.ttdealersdk.entity.response.country;

/**
 * Created by user on 17/05/16.
 */
public class PostcodeState {


    private Postcode[] postcodes;


    public Postcode[] getPostcodes() {
        return postcodes;
    }

    public void setPostcodes(Postcode[] postcodes) {
        this.postcodes = postcodes;
    }
}
