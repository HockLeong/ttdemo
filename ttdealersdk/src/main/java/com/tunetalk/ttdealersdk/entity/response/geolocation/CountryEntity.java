package com.tunetalk.ttdealersdk.entity.response.geolocation;

public class CountryEntity
{
    String code, name;

    public String getName()
    {
        return name;
    }

    public String getCode()
    {
        return code;
    }
}
