package com.tunetalk.ttdealersdk.entity.request;

public class InitRequest
{
    private String apiKey;
    private String appId;
    private String platform;

    public String getApiKey ()
    {
        return apiKey;
    }

    public InitRequest setApiKey (String apiKey)
    {
        this.apiKey = apiKey;
        return this;
    }

    public String getAppId ()
    {
        return appId;
    }

    public InitRequest setAppId (String appId)
    {
        this.appId = appId;
        return this;
    }

    public String getPlatform ()
    {
        return platform;
    }

    public InitRequest setPlatform (String platform)
    {
        this.platform = platform;
        return this;
    }
}
