package com.tunetalk.ttdealersdk.entity.response.geolocation;

public class StateEntity
{
    String StateName;
    String StateID;

    public String getStateName()
    {
        return StateName;
    }

    public String getStateID()
    {
        return StateID;
    }
}
