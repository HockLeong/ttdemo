package com.tunetalk.ttdealersdk.entity.response.country;

public class NewStateList
{
    private String code, name;

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }
}