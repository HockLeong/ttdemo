package com.tunetalk.ttdealersdk.entity.response.ocr;

public class OCRImageEntity
{
    private byte[] mDocumentBuffer;
    private byte[] mFaceBuffer;

    public byte[] getDocumentBuffer ()
    {
        return mDocumentBuffer;
    }

    public OCRImageEntity setDocumentBuffer (byte[] mDocumentBuffer)
    {
        this.mDocumentBuffer = mDocumentBuffer;
        return this;
    }

    public byte[] getFaceBuffer ()
    {
        return mFaceBuffer;
    }

    public OCRImageEntity setFaceBuffer (byte[] mFaceBuffer)
    {
        this.mFaceBuffer = mFaceBuffer;
        return this;
    }
}
