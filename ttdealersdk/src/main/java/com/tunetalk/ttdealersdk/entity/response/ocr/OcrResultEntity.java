package com.tunetalk.ttdealersdk.entity.response.ocr;

import android.app.Activity;

import com.google.gson.Gson;
import com.tunetalk.ttdealersdk.entity.response.postcode.PostcodeItemEntity;
import com.tunetalk.ttdealersdk.entity.response.postcode.PostcodeRootEntity;

import java.io.IOException;
import java.io.InputStream;

public class OcrResultEntity
{
    Activity activity;

    public String name;
    public String passport;
    public String nric;
    public String gender;
    public String dob;
    public String address;
    public String poscode;
    public String state;
    public String city;
    public String country;
    public String nationality;

    public OcrResultEntity (Activity activity)
    {
        this.activity = activity;
    }

    public String getName ()
    {
        return name;
    }

    public OcrResultEntity setName (String name)
    {
        this.name = name;
        return this;
    }

    public String getPassport ()
    {
        return passport;
    }

    public OcrResultEntity setPassport (String passport)
    {
        this.passport = passport;
        return this;
    }

    public String getNric ()
    {
        return nric;
    }

    public OcrResultEntity setNric (String nric)
    {
        this.nric = nric;
        return this;
    }

    public String getGender ()
    {
        return gender;
    }

    public OcrResultEntity setGender (String gender)
    {
        this.gender = gender;
        return this;
    }

    public String getDob ()
    {
        return dob;
    }

    public OcrResultEntity setDob (String dob)
    {
        this.dob = dob;
        return this;
    }

    public String getAddress ()
    {
        return address;
    }

    public OcrResultEntity setAddress (String address)
    {
        this.address = address;
        return this;
    }

    public String getPoscode ()
    {
        return poscode;
    }

    public OcrResultEntity setPoscode (final String poscode)
    {
        this.poscode = poscode;

        String json = readJsonFromAssets(activity, "Postcode.json");
        final PostcodeRootEntity postcode = new Gson().fromJson(json, PostcodeRootEntity.class);
        postcode.setup();

        if (poscode != null && poscode.length() == 5)
        {
            new Runnable()
            {
                @Override
                public void run ()
                {
                    final PostcodeItemEntity[] mPostcodes = postcode.getPostcodes();

                    for (int i = 0; i < mPostcodes.length - 1; ++ i)
                    {
                        if (poscode.equals(mPostcodes[i].getPostcode()))
                        {
                            for (int j = 0; j < postcode.getStateID().size(); ++ j)
                            {
                                if (postcode.getStateID().get(j).equals(mPostcodes[i].getStateId()))
                                {
                                    final int finalI = i, finalJ = j;
                                    activity.runOnUiThread(new Runnable()
                                    {
                                        @Override public void run ()
                                        {
                                            setState(postcode.getStateName().get(finalJ));
                                            setCity(mPostcodes[finalI].getCityName());
                                            setCountry("Malaysia");
                                        }
                                    });
                                    break;
                                }
                            }

                            break;
                        }
                    }
                }
            }.run();
        }

        return this;
    }

    public String getState ()
    {
        return state;
    }

    public OcrResultEntity setState (String state)
    {
        this.state = state;
        return this;
    }

    public String getCity ()
    {
        return city;
    }

    public OcrResultEntity setCity (String city)
    {
        this.city = city;
        return this;
    }

    public String getCountry ()
    {
        return country;
    }

    public OcrResultEntity setCountry (String country)
    {
        this.country = country;
        return this;
    }

    public String getNationality ()
    {
        return nationality;
    }

    public OcrResultEntity setNationality (String nationality)
    {
        this.nationality = nationality;
        return this;
    }

    private static String readJsonFromAssets (Activity activity, String filename)
    {
        try
        {
            InputStream is = activity.getAssets().open(filename);
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            is.close();
            return new String(buffer, "UTF-8");
        }
        catch (IOException io)
        {
            io.printStackTrace();
            return null;
        }
    }
}
