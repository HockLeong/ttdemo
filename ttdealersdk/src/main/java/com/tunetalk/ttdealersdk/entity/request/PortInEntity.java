package com.tunetalk.ttdealersdk.entity.request;

import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PortInEntity implements Serializable
{
    private String Last8Digit, FullName, DOB;
    private String Address, Country, City, State, Postcode, PortInNumber, ServiceProvider;
    private Boolean IsMale, IsMalaysian = null, isManual = false, isOKU = false;
    private String ImageOne, ImageTwo;
    private String OkuImageOne, OkuImageTwo;
    private String StaffLoginId;
    private List<PortInValidityEntity> portIns;

    @Nullable
    private String NIRC, Passport, Nationality, EMAIL;

    public Boolean getManual ()
    {
        return isManual;
    }

    public PortInEntity setManual (Boolean manual)
    {
        isManual = manual;
        return this;
    }

    public String getLast8Digit ()
    {
        return Last8Digit;
    }

    public PortInEntity setLast8Digit (String last8Digit)
    {
        Last8Digit = last8Digit;
        return this;
    }

    public String getFullName ()
    {
        return FullName;
    }

    public PortInEntity setFullname (String nm)
    {
        FullName = nm;
        return this;
    }

    public String getDOB ()
    {
        return DOB;
    }

    public PortInEntity setDOB (String DOB)
    {
        this.DOB = DOB;
        return this;
    }

    public String getNIRC ()
    {
        return NIRC;
    }

    public PortInEntity setNIRC (String NIRC)
    {
        this.NIRC = NIRC;
        return this;
    }

    public String getPassport ()
    {
        return Passport;
    }

    public PortInEntity setPassport (String passport)
    {
        Passport = passport;
        return this;
    }

    public String getNationality ()
    {
        return Nationality;
    }

    public PortInEntity setNationality (String nationality)
    {
        Nationality = nationality;
        return this;
    }

    public String getAddress ()
    {
        return Address;
    }

    public PortInEntity setAddress (String address)
    {
        Address = address;
        return this;
    }

    public PortInEntity setCountry (String country)
    {
        Country = country;
        return this;
    }

    public String getCountry ()
    {
        return Country;
    }

    public String getCity ()
    {
        return City;
    }

    public PortInEntity setCity (String city)
    {
        City = city;
        return this;
    }

    public String getState ()
    {
        return State;
    }

    public PortInEntity setState (String state)
    {
        State = state;
        return this;
    }

    public String getPostcode ()
    {
        return Postcode;
    }

    public PortInEntity setPostcode (String postcode)
    {
        Postcode = postcode;
        return this;
    }

    public String getPortInNumber ()
    {
        return PortInNumber;
    }

    public PortInEntity setPortInNumber (String portInNumber)
    {
        PortInNumber = portInNumber;
        return this;
    }

    public String getServiceProvider ()
    {
        return ServiceProvider;
    }

    public PortInEntity setServiceProvider (String serviceProvider)
    {
        ServiceProvider = serviceProvider;
        return this;
    }

    public PortInEntity setEMAIL (String email)
    {
        EMAIL = email;
        return this;
    }

    public String getEMAIL ()
    {
        return EMAIL;
    }

    public String getGender ()
    {
        return IsMale ? "Male" : "Female";
    }

    public PortInEntity setIsMale (Boolean isMale)
    {
        IsMale = isMale;
        return this;
    }

    public Boolean getIsMalaysian ()
    {
        return IsMalaysian;
    }

    public String getIDType ()
    {
        return getIsMalaysian() ? "mykad" : "passport";
    }

    public String getIDNumber ()
    {
        return getIsMalaysian() ? getNIRC() : getPassport();
    }

    public PortInEntity setIsMalaysian (Boolean isMalaysian)
    {
        IsMalaysian = isMalaysian;
        return this;
    }

    public Boolean getOKU ()
    {
        return isOKU;
    }

    public PortInEntity setOKU (Boolean OKU)
    {
        isOKU = OKU;
        return this;
    }

    public String getImageOne ()
    {
        return ImageOne;
    }

    public PortInEntity setImageOne (String imageOne)
    {
        ImageOne = imageOne;
        return this;
    }

    public String getImageTwo ()
    {
        return ImageTwo;
    }

    public PortInEntity setImageTwo (String imageTwo)
    {
        ImageTwo = imageTwo;
        return this;
    }

    public String getOkuImageOne ()
    {
        return OkuImageOne;
    }

    public PortInEntity setOkuImageOne (String okuImageOne)
    {
        OkuImageOne = okuImageOne;
        return this;
    }

    public String getOkuImageTwo ()
    {
        return OkuImageTwo;
    }

    public PortInEntity setOkuImageTwo (String okuImageTwo)
    {
        OkuImageTwo = okuImageTwo;
        return this;
    }

    public String getStaffLoginId ()
    {
        return StaffLoginId;
    }

    public PortInEntity setStaffLoginId (String staffLoginId)
    {
        StaffLoginId = staffLoginId;
        return this;
    }

    public String getDate ()
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date currentDate = new Date();
        return df.format(currentDate);
    }

    public List<PortInValidityEntity> getPortIns ()
    {
        if (portIns == null)
            portIns = new ArrayList<>();

        return portIns;
    }

    public PortInEntity setPortIns (List<PortInValidityEntity> portIns)
    {
        this.portIns = portIns;
        return this;
    }

    public String getJSON (String firstDigit, String username)
    {
        JSONObject json = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date currentDate = new Date();
        try
        {
            if (getEMAIL().length() != 0)
                json.put("emailAddr", getEMAIL());
            else
                json.put("emailAddr", "apps@tunetalk.com");

            json.put("partnerDealerCode", username);

            jsonArray.put(getImageOne());

            if (getImageTwo() != null)
                jsonArray.put(getImageTwo());

            if (getOkuImageOne() != null)
                jsonArray.put(getOkuImageOne());

            if (getOkuImageTwo() != null)
                jsonArray.put(getOkuImageTwo());

            json.put("photos", jsonArray);
            json.put("date", df.format(currentDate));

            if (portIns != null && portIns.size() > 0)
            {
                JSONArray jsonArray2 = new JSONArray();
                for (PortInValidityEntity entity : portIns)
                {
                    //Prevent if resubmit append first8Digits again
                    if (entity.getSimNumber().length() == 8)
                    {
                        setLast8Digit(entity.getSimNumber());
                        String simNumber = firstDigit + entity.getSimNumber();
                        entity.setSimNumber(simNumber);
                    }

                    JSONObject object = new JSONObject();
                    object.put("simNumber", entity.simNumber);
                    object.put("msisdnToPortIn", entity.getMsisdnToPortIn());

                    jsonArray2.put(object);
                }
                /*
                 *
                 *   =========================================================
                 *              Alternative way (Converting to JSONArray)
                 *   =========================================================
                 *
                 *    String portInList = new Gson().toJson(portIns, new TypeToken<ArrayList<PortInValidityEntity>>() {}.getType());
                 *    JSONArray jsonArray2 = new JSONArray(portInList);
                 */

                json.put("portIns", jsonArray2);
            }
            else
            {
                json.put("simNumber", firstDigit + getLast8Digit());
                json.put("msisdnToPortIn", getPortInNumber());
            }

            json.put("isManual", isManual);
            json.put("middleName", "");

            if (getFullName().length() > 30)
            {
                json.put("firstName", getFullName().substring(0, 29));
                json.put("lastName", getFullName().substring(30, getFullName().length() - 1));
            }
            else
            {
                try
                {
                    json.put("firstName", getFullName().substring(0, getFullName().indexOf(' ')));
                    json.put("lastName", getFullName().substring(getFullName().indexOf(' ') + 1));
                }
                catch (StringIndexOutOfBoundsException ppp)
                {
                    json.put("firstName", getFullName());
                    json.put("lastName", "NA");
                }
            }
            json.put("gender", getGender());
            json.put("dob", getDOB());
            json.put("nationality", getNationality());
            json.put("address", getAddress());
            json.put("country", getCountry());
            json.put("state", getState());
            json.put("city", getCity());
            json.put("postCode", getPostcode());
            json.put("currentProvider", getServiceProvider());
            json.put("idType", getIDType());
            json.put("idNumber", getIDNumber());

            return json.toString();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    public void debug ()
    {
        JSONObject json = new JSONObject();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date currentDate = new Date();
        try
        {
            json.put("date", df.format(currentDate));
            json.put("simNumber", "xxxxxxxx" + getLast8Digit());
            json.put("middleName", "");
            if (getFullName().length() > 30)
            {
                json.put("firstName", getFullName().substring(0, 29));
                json.put("lastName", getFullName().substring(30, 59));
            }
            else
            {
                json.put("firstName", getFullName().substring(0, getFullName().length() - 1));
                json.put("lastName", "");
            }
            json.put("gender", getGender());
            json.put("dob", getDOB());
            json.put("nationality", getNationality());
            json.put("address", getAddress());
            json.put("country", getCountry());
            json.put("state", getState());
            json.put("city", getCity());
            json.put("postCode", getPostcode());
            json.put("msisdnToPortIn", getPortInNumber());
            json.put("currentProvider", getServiceProvider());
            json.put("idType", getIDType());
            json.put("idNumber", getIDNumber());

            if (getEMAIL().length() != 0)
                json.put("emailAddr", getEMAIL());
            else
                json.put("emailAddr", "NULL");

            json.put("staffLoginId", "DEBUG");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

}
