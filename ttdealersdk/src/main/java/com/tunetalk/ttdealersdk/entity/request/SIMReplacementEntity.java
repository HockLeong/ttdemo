package com.tunetalk.ttdealersdk.entity.request;

import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SIMReplacementEntity implements Serializable
{
    private String Last8Digit, FullName;
    private String MobileNumber;
    private Boolean IsMalaysian = null, isManual = false;
    private String ImageOne, ImageTwo, ImageThree;
    private String StaffLoginId;

    @Nullable
    private String NIRC, Passport, EMAIL;

    public Boolean getManual ()
    {
        return isManual;
    }

    public SIMReplacementEntity setManual (Boolean manual)
    {
        isManual = manual;
        return this;
    }

    public String getLast8Digit ()
    {
        return Last8Digit;
    }

    public SIMReplacementEntity setLast8Digit (String last8Digit)
    {
        Last8Digit = last8Digit;
        return this;
    }

    public String getFullName ()
    {
        return FullName;
    }

    public SIMReplacementEntity setFullName (String nm)
    {
        FullName = nm;
        return this;
    }

    public String getNIRC ()
    {
        return NIRC;
    }

    public SIMReplacementEntity setNIRC (String NIRC)
    {
        this.NIRC = NIRC;
        return this;
    }

    public String getPassport ()
    {
        return Passport;
    }

    public SIMReplacementEntity setPassport (String passport)
    {
        Passport = passport;
        return this;
    }

    public String getMobileNumber ()
    {
        return MobileNumber;
    }

    public SIMReplacementEntity setMobileNumber (String mobileNumber)
    {
        MobileNumber = mobileNumber;
        return this;
    }

    public SIMReplacementEntity setEMAIL (String email)
    {
        EMAIL = email;
        return this;
    }

    public String getEMAIL ()
    {
        return EMAIL;
    }

    public Boolean getIsMalaysian ()
    {
        return IsMalaysian;
    }

    public String getIDType ()
    {
        return getIsMalaysian() ? "mykad" : "passport";
    }

    public String getIDNumber ()
    {
        return getIsMalaysian() ? getNIRC() : getPassport();
    }

    public SIMReplacementEntity setIsMalaysian (Boolean isMalaysian)
    {
        IsMalaysian = isMalaysian;
        return this;
    }

    public String getImageOne ()
    {
        return ImageOne;
    }

    public SIMReplacementEntity setImageOne (String imageOne)
    {
        ImageOne = imageOne;
        return this;
    }

    public String getImageTwo ()
    {
        return ImageTwo;
    }

    public SIMReplacementEntity setImageTwo (String imageTwo)
    {
        ImageTwo = imageTwo;
        return this;
    }

    public String getImageThree ()
    {
        return ImageThree;
    }

    public SIMReplacementEntity setImageThree (String imageThree)
    {
        ImageThree = imageThree;
        return this;
    }

    public String getStaffLoginId ()
    {
        return StaffLoginId;
    }

    public SIMReplacementEntity setStaffLoginId (String staffLoginId)
    {
        StaffLoginId = staffLoginId;
        return this;
    }

    public String getDate ()
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date currentDate = new Date();
        return df.format(currentDate);
    }

    public String getJSON (String firstDigit, String username)
    {
        JSONObject json = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date currentDate = new Date();
        try
        {
            if (getEMAIL().length() != 0)
                json.put("emailAddr", getEMAIL());
            else
                json.put("emailAddr", "apps@tunetalk.com");

            json.put("oldSimNumber", "-");

            json.put("msisdn", getMobileNumber());
            json.put("partnerDealerCode", username);

            jsonArray.put(getImageOne());

            if (getImageTwo() != null)
                jsonArray.put(getImageTwo());

            json.put("photos", jsonArray);
            json.put("date", df.format(currentDate));
            json.put("simNumber", firstDigit + getLast8Digit());
            json.put("middleName", "");
            json.put("isManual", isManual);

            if (getFullName().length() > 30)
            {
                json.put("firstName", getFullName().substring(0, 29));
                json.put("lastName", getFullName().substring(30, getFullName().length() - 1));
            }
            else
            {
                try
                {
                    json.put("firstName", getFullName().substring(0, getFullName().indexOf(' ')));
                    json.put("lastName", getFullName().substring(getFullName().indexOf(' ') + 1));
                }
                catch (StringIndexOutOfBoundsException ppp)
                {
                    json.put("firstName", getFullName());
                    json.put("lastName", "NA");
                }
            }
            json.put("idType", getIDType());
            json.put("idNumber", getIDNumber());

            //TODO:
//            json.put("latitude", DashboardActivity.getCurrentLocation()[0]);
//            json.put("longitude", DashboardActivity.getCurrentLocation()[1]);
            return json.toString();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    public void debug ()
    {
        JSONObject json = new JSONObject();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date currentDate = new Date();
        try
        {
            json.put("date", df.format(currentDate));
            json.put("simNumber", "xxxxxxxx" + getLast8Digit());
            json.put("middleName", "");
            if (getFullName().length() > 30)
            {
                json.put("firstName", getFullName().substring(0, 29));
                json.put("lastName", getFullName().substring(30, getFullName().length() - 1));
            }
            else
            {
                json.put("firstName", getFullName().substring(0, getFullName().length() - 1));
                json.put("lastName", "");
            }
            json.put("idType", getIDType());
            json.put("idNumber", getIDNumber());

            if (getEMAIL().length() != 0)
                json.put("emailAddr", getEMAIL());
            else
                json.put("emailAddr", "NULL");

            json.put("msisdn", getMobileNumber());
            json.put("staffLoginId", "DEBUG");

            Log.e("Debug", json.toString());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
