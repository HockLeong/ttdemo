package com.tunetalk.ttdealersdk.entity.request;

public class SimRegistrationOneRequest
{
    private String idPhoto;
    private String selfie;
    private String name;
    private String idType;
    private String idNumber;
    private String gender;
    private String nationality;
    private String dob;
    private String address;
    private String city;
    private String state;
    private String country;
    private String postCode;

    private String date;
    private String firstName;
    private String lastName;
    private String deviceId;

    public String getIdPhoto ()
    {
        return idPhoto;
    }

    public SimRegistrationOneRequest setIdPhoto (String idPhoto)
    {
        this.idPhoto = idPhoto;
        return this;
    }

    public String getSelfie ()
    {
        return selfie;
    }

    public SimRegistrationOneRequest setSelfie (String selfie)
    {
        this.selfie = selfie;
        return this;
    }

    public String getName ()
    {
        return name;
    }

    public SimRegistrationOneRequest setName (String name)
    {
        this.name = name;
        return this;
    }

    public String getIdType ()
    {
        return idType;
    }

    public SimRegistrationOneRequest setIdType (String idType)
    {
        this.idType = idType;
        return this;
    }

    public String getIdNumber ()
    {
        return idNumber;
    }

    public SimRegistrationOneRequest setIdNumber (String idNumber)
    {
        this.idNumber = idNumber;
        return this;
    }

    public String getGender ()
    {
        return gender;
    }

    public SimRegistrationOneRequest setGender (String gender)
    {
        this.gender = gender;
        return this;
    }

    public String getNationality ()
    {
        return nationality;
    }

    public SimRegistrationOneRequest setNationality (String nationality)
    {
        this.nationality = nationality;
        return this;
    }

    public String getDob ()
    {
        return dob;
    }

    public SimRegistrationOneRequest setDob (String dob)
    {
        this.dob = dob;
        return this;
    }

    public String getAddress ()
    {
        return address;
    }

    public SimRegistrationOneRequest setAddress (String address)
    {
        this.address = address;
        return this;
    }

    public String getCity ()
    {
        return city;
    }

    public SimRegistrationOneRequest setCity (String city)
    {
        this.city = city;
        return this;
    }

    public String getState ()
    {
        return state;
    }

    public SimRegistrationOneRequest setState (String state)
    {
        this.state = state;
        return this;
    }

    public String getCountry ()
    {
        return country;
    }

    public SimRegistrationOneRequest setCountry (String country)
    {
        this.country = country;
        return this;
    }

    public String getPostCode ()
    {
        return postCode;
    }

    public SimRegistrationOneRequest setPostCode (String postCode)
    {
        this.postCode = postCode;
        return this;
    }
}
