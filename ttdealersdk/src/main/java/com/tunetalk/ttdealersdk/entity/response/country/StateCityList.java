package com.tunetalk.ttdealersdk.entity.response.country;

public class StateCityList
{
    private String CountryCode, FCode, Id, Name;

    public String getCountryCode ()
    {
        return CountryCode;
    }

    public void setCountryCode (String countryCode)
    {
        CountryCode = countryCode;
    }

    public String getFCode ()
    {
        return FCode;
    }

    public void setFCode (String fCode)
    {
        FCode = fCode;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String id)
    {
        Id = id;
    }

    public String getName ()
    {
        return Name;
    }

    public void setName (String name)
    {
        Name = name;
    }
}