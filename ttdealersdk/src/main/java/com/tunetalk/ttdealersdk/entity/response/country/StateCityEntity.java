package com.tunetalk.ttdealersdk.entity.response.country;

public class StateCityEntity
{
    private StateCityList[] SimpleGeoName;

    public StateCityList[] getSimpleGeoName ()
    {
        return SimpleGeoName;
    }

    public void setSimpleGeoName (StateCityList[] simpleGeoName)
    {
        SimpleGeoName = simpleGeoName;
    }
}