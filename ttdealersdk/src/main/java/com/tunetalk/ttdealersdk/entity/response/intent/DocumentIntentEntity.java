package com.tunetalk.ttdealersdk.entity.response.intent;

import com.tunetalk.ttdealersdk.enums.ServiceCategories;
import com.tunetalk.ttdealersdk.ocr.DocumentType;

/**
 * Singleton entity to store intent variable
 *
 */
public class DocumentIntentEntity
{
    String apiKey;
    String partnerDealerId;
    ServiceCategories serviceCategories;
    DocumentType documentType;

    public String getApiKey ()
    {
        return apiKey;
    }

    public DocumentIntentEntity setApiKey (String apiKey)
    {
        this.apiKey = apiKey;
        return this;
    }

    public String getPartnerDealerCode ()
    {
        return partnerDealerId;
    }

    public DocumentIntentEntity setPartnerDealerCode (String partnerDealerId)
    {
        this.partnerDealerId = partnerDealerId;
        return this;
    }

    public ServiceCategories getServiceCategories ()
    {
        return serviceCategories;
    }

    public DocumentIntentEntity setServiceCategories (ServiceCategories serviceCategories)
    {
        this.serviceCategories = serviceCategories;
        return this;
    }

    public DocumentType getDocumentType ()
    {
        return documentType;
    }

    public DocumentIntentEntity setDocumentType (DocumentType documentType)
    {
        this.documentType = documentType;
        return this;
    }
}
