package com.tunetalk.ttdealersdk.entity.response.country;

public class CountryEntity
{
    private CountryList[] SimpleGeoName;

    public CountryList[] getSimpleGeoName ()
    {
        return SimpleGeoName;
    }

    public void setSimpleGeoName (CountryList[] simpleGeoName)
    {
        SimpleGeoName = simpleGeoName;
    }

    String code, name;

    public String getName()
    {
        return name;
    }

    public String getCode()
    {
        return code;
    }
}