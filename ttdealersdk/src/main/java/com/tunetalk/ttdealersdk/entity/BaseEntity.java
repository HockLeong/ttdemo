package com.tunetalk.ttdealersdk.entity;

public class BaseEntity
{
    int resultCode;
    String errorDescription;

    public int getResultCode ()
    {
        return resultCode;
    }

    public BaseEntity setResultCode (int resultCode)
    {
        this.resultCode = resultCode;
        return this;
    }

    public String getErrorDescription ()
    {
        return errorDescription;
    }

    public BaseEntity setErrorDescription (String errorDescription)
    {
        this.errorDescription = errorDescription;
        return this;
    }
}
