package com.tunetalk.ttdealersdk.entity.response;

import com.tunetalk.ttdealersdk.entity.BaseResp;

public class RegisterSimNumberEntity extends BaseResp
{
    String simNumber;

    public String getSimNumber ()
    {
        if(simNumber == null)
            simNumber = "";

        return simNumber;
    }

    public RegisterSimNumberEntity setSimNumber (String simNumber)
    {
        this.simNumber = simNumber;
        return this;
    }
}
