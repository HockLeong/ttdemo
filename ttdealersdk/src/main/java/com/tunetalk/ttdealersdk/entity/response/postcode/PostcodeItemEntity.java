package com.tunetalk.ttdealersdk.entity.response.postcode;

public class PostcodeItemEntity
{
    String postcode, cityName, stateId;

    public String getPostcode()
    {
        return postcode;
    }

    public String getCityName()
    {
        return cityName;
    }

    public String getStateId()
    {
        return stateId;
    }
}
