package com.tunetalk.ttdealersdk.entity.request;

public class SimRegistrationTwoReq
{
    String msisdn;
    String registerId;
    String simNumber;

    public String getMsisdn ()
    {
        return msisdn;
    }

    public SimRegistrationTwoReq setMsisdn (String msisdn)
    {
        this.msisdn = msisdn;
        return this;
    }

    public String getRegisterId ()
    {
        return registerId;
    }

    public SimRegistrationTwoReq setRegisterId (String registerId)
    {
        this.registerId = registerId;
        return this;
    }

    public String getSimNumber ()
    {
        return simNumber;
    }

    public SimRegistrationTwoReq setSimNumber (String simNumber)
    {
        this.simNumber = simNumber;
        return this;
    }
}
