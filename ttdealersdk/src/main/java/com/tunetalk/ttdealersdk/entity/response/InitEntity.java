package com.tunetalk.ttdealersdk.entity.response;

import com.tunetalk.ttdealersdk.entity.BaseResp;

public class InitEntity extends BaseResp
{
    private String innovatifLicenseKey;
    private String mircroBlinkLicenseKey;

    public String getInnovatifLicenseKey ()
    {
        return innovatifLicenseKey;
    }

    public InitEntity setInnovatifLicenseKey (String innovatifLicenseKey)
    {
        this.innovatifLicenseKey = innovatifLicenseKey;
        return this;
    }

    public String getMircroBlinkLicenseKey ()
    {
        return mircroBlinkLicenseKey;
    }

    public InitEntity setMircroBlinkLicenseKey (String mircroBlinkLicenseKey)
    {
        this.mircroBlinkLicenseKey = mircroBlinkLicenseKey;
        return this;
    }
}
