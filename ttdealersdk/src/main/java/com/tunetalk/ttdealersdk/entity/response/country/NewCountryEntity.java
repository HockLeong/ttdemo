package com.tunetalk.ttdealersdk.entity.response.country;

public class NewCountryEntity
{
    private NewCountryList[] countries;
    private NewStateList[] states;

    public NewCountryList[] getCountries ()
    {
        return countries;
    }

    public void setCountries (NewCountryList[] countries)
    {
        this.countries = countries;
    }

    public NewStateList[] getStatesList ()
    {
        return states;
    }
}