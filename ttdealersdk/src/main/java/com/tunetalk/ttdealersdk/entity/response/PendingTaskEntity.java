package com.tunetalk.ttdealersdk.entity.response;

import com.tunetalk.ttdealersdk.entity.BaseEntity;

public class PendingTaskEntity extends BaseEntity
{
    String id;
    String simNo;
    String insertDate;
    String serviceType;

    public String getId ()
    {
        return id;
    }

    public PendingTaskEntity setId (String id)
    {
        this.id = id;
        return this;
    }

    public String getSimNo ()
    {
        return simNo;
    }

    public PendingTaskEntity setSimNo (String simNo)
    {
        this.simNo = simNo;
        return this;
    }

    public String getInsertDate ()
    {
        return insertDate;
    }

    public PendingTaskEntity setInsertDate (String insertDate)
    {
        this.insertDate = insertDate;
        return this;
    }

    public String getServiceType ()
    {
        return serviceType;
    }

    public PendingTaskEntity setServiceType (String serviceType)
    {
        this.serviceType = serviceType;
        return this;
    }
}
