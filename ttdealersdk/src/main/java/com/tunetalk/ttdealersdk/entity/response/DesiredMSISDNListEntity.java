package com.tunetalk.ttdealersdk.entity.response;

import com.tunetalk.ttdealersdk.entity.BaseResp;

import java.util.List;

public class DesiredMSISDNListEntity extends BaseResp
{
    List<String> desiredMSISDN;

    public List<String> getDesiredMSISDN ()
    {
        return desiredMSISDN;
    }

    public DesiredMSISDNListEntity setDesiredMSISDN (List<String> desiredMSISDN)
    {
        this.desiredMSISDN = desiredMSISDN;
        return this;
    }
}
