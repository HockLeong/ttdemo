package com.tunetalk.ttdealersdk.entity.response.country;

public class CountryList
{
    private String CountryCode, FCode, Id, Name;

    private StateCityEntity Children;

    public String getCountryCode ()
    {
        return CountryCode;
    }

    public void setCountryCode (String countryCode)
    {
        CountryCode = countryCode;
    }

    public String getFCode ()
    {
        return FCode;
    }

    public void setFCode (String fCode)
    {
        FCode = fCode;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String id)
    {
        Id = id;
    }

    public String getName ()
    {
        return Name;
    }

    public void setName (String name)
    {
        Name = name;
    }

    public StateCityEntity getChildren ()
    {
        return Children;
    }

    public void setChildren (StateCityEntity children)
    {
        Children = children;
    }
}