package com.tunetalk.ttdealersdk.entity.response;

import com.tunetalk.ttdealersdk.entity.BaseEntity;

public class OfflineTaskSubmissionEntity extends BaseEntity
{
    String createdDate;
    String simNo;

    public String getCreatedDate ()
    {
        return createdDate;
    }

    public OfflineTaskSubmissionEntity setCreatedDate (String createdDate)
    {
        this.createdDate = createdDate;
        return this;
    }

    public String getSimNo ()
    {
        return simNo;
    }

    public OfflineTaskSubmissionEntity setSimNo (String simNo)
    {
        this.simNo = simNo;
        return this;
    }
}
