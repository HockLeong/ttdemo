package com.tunetalk.ttdealersdk.entity.request;

import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SIMRegistrationEntity implements Serializable
{
    private String Last8Digit, FullName, DOB;
    private String Address, Country, City, State, Postcode;
    private Boolean IsMale, IsMalaysian = null, isManual = false, isOKU = false;
    private String ImageOne, ImageTwo, blockedNumber;
    private String OkuImageOne, OkuImageTwo;
    private String StaffLoginId;

    @Nullable
    private String NIRC, Passport, Nationality, AlternatePhoneNumber, EMAIL, webOrderId;

    public Boolean getManual ()
    {
        return isManual;
    }

    public SIMRegistrationEntity setManual (Boolean manual)
    {
        isManual = manual;
        return this;
    }

    @Nullable
    public String getWebOrderId ()
    {
        return webOrderId;
    }

    public SIMRegistrationEntity setWebOrderId (@Nullable String webOrderId)
    {
        this.webOrderId = webOrderId;
        return this;
    }

    public String getLast8Digit ()
    {
        return Last8Digit;
    }

    public SIMRegistrationEntity setLast8Digit (String last8Digit)
    {
        Last8Digit = last8Digit;
        return this;
    }

    public String getFullName ()
    {
        return FullName;
    }

    public SIMRegistrationEntity setFullName (String nm)
    {
        FullName = nm;
        return this;
    }

    public String getDOB ()
    {
        return DOB;
    }

    public SIMRegistrationEntity setDOB (String DOB)
    {
        this.DOB = DOB;
        return this;
    }

    public String getNIRC ()
    {
        return NIRC;
    }

    public SIMRegistrationEntity setNIRC (String NIRC)
    {
        this.NIRC = NIRC;
        return this;
    }

    public String getPassport ()
    {
        return Passport;
    }

    public SIMRegistrationEntity setPassport (String passport)
    {
        Passport = passport;
        return this;
    }

    public String getNationality ()
    {
        return Nationality;
    }

    public SIMRegistrationEntity setNationality (String nationality)
    {
        Nationality = nationality;
        return this;
    }

    public String getAddress ()
    {
        return Address;
    }

    public SIMRegistrationEntity setAddress (String address)
    {
        Address = address;
        return this;
    }

    public SIMRegistrationEntity setCountry (String country)
    {
        Country = country;
        return this;
    }

    public String getCountry ()
    {
        return Country;
    }

    public String getCity ()
    {
        return City;
    }

    public SIMRegistrationEntity setCity (String city)
    {
        City = city;
        return this;
    }

    public String getState ()
    {
        return State;
    }

    public SIMRegistrationEntity setState (String state)
    {
        State = state;
        return this;
    }

    public String getPostcode ()
    {
        return Postcode;
    }

    public SIMRegistrationEntity setPostcode (String postcode)
    {
        Postcode = postcode;
        return this;
    }

    public String getAlternatePhoneNumber ()
    {
        return AlternatePhoneNumber;
    }

    public SIMRegistrationEntity setAlternatePhoneNumber (String alternatePhoneNumber)
    {
        AlternatePhoneNumber = alternatePhoneNumber;
        return this;
    }

    public SIMRegistrationEntity setEMAIL (String email)
    {
        EMAIL = email;
        return this;
    }

    public String getEMAIL ()
    {
        return EMAIL;
    }

    public String getGender ()
    {
        return IsMale ? "Male" : "Female";
    }

    public SIMRegistrationEntity setIsMale (Boolean isMale)
    {
        IsMale = isMale;
        return this;
    }

    public Boolean getIsMalaysian ()
    {
        return IsMalaysian;
    }

    public String getIDType ()
    {
        return getIsMalaysian() ? "mykad" : "passport";
    }

    public String getIDNumber ()
    {
        return getIsMalaysian() ? getNIRC() : getPassport();
    }

    public SIMRegistrationEntity setIsMalaysian (Boolean isMalaysian)
    {
        IsMalaysian = isMalaysian;
        return this;
    }

    public String getImageOne ()
    {
        return ImageOne;
    }

    public SIMRegistrationEntity setImageOne (String imageOne)
    {
        ImageOne = imageOne;
        return this;
    }

    public String getImageTwo ()
    {
        return ImageTwo;
    }

    public SIMRegistrationEntity setImageTwo (String imageTwo)
    {
        ImageTwo = imageTwo;
        return this;
    }

    public String getBlockedNumber ()
    {
        return blockedNumber;
    }

    public SIMRegistrationEntity setBlockedNumber (String blockedNumber)
    {
        this.blockedNumber = blockedNumber;
        return this;
    }

    public Boolean getOKU ()
    {
        return isOKU;
    }

    public SIMRegistrationEntity setOKU (Boolean OKU)
    {
        isOKU = OKU;
        return this;
    }

    public String getOkuImageOne ()
    {
        return OkuImageOne;
    }

    public SIMRegistrationEntity setOkuImageOne (String okuImageOne)
    {
        OkuImageOne = okuImageOne;
        return this;
    }

    public String getOkuImageTwo ()
    {
        return OkuImageTwo;
    }

    public SIMRegistrationEntity setOkuImageTwo (String okuImageTwo)
    {
        OkuImageTwo = okuImageTwo;
        return this;
    }

    public String getStaffLoginId ()
    {
        return StaffLoginId;
    }

    public SIMRegistrationEntity setStaffLoginId (String staffLoginId)
    {
        StaffLoginId = staffLoginId;
        return this;
    }

    public String getDate ()
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date currentDate = new Date();
        return df.format(currentDate);
    }

    public String getJSON (String firstDigit, String username)
    {
        JSONObject json = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date currentDate = new Date();
        try
        {
            if (getAlternatePhoneNumber() == null || getAlternatePhoneNumber().length() == 0)
                json.put("altMSISDN", "-");
            else
                json.put("altMSISDN", getAlternatePhoneNumber());

            if (getEMAIL().length() != 0)
                json.put("emailAddr", getEMAIL());
            else
                json.put("emailAddr", "apps@tunetalk.com");

            json.put("partnerDealerCode", username);
            json.put("isManual", isManual);

            jsonArray.put(getImageOne());

            if (getImageTwo() != null)
                jsonArray.put(getImageTwo());

            if (getOkuImageOne() != null)
                jsonArray.put(getOkuImageOne());

            if (getOkuImageTwo() != null)
                jsonArray.put(getOkuImageTwo());

            json.put("photos", jsonArray);
            json.put("date", df.format(currentDate));
            json.put("simNumber", firstDigit + getLast8Digit());
            json.put("middleName", "");
            if (getFullName().length() > 30)
            {
                json.put("firstName", getFullName().substring(0, 29));
                json.put("lastName", getFullName().substring(30, getFullName().length() - 1));
            }
            else
            {
                try
                {
                    json.put("firstName", getFullName().substring(0, getFullName().indexOf(' ')));
                    json.put("lastName", getFullName().substring(getFullName().indexOf(' ') + 1));
                }
                catch (StringIndexOutOfBoundsException ppp)
                {
                    json.put("firstName", getFullName());
                    json.put("lastName", "NA");
                }
            }
            json.put("gender", getGender());
            json.put("dob", getDOB());
            json.put("nationality", getNationality());
            json.put("address", getAddress());
            json.put("country", getCountry());
            json.put("state", getState());
            json.put("city", getCity());
            json.put("postCode", getPostcode());
            json.put("idType", getIDType());
            json.put("idNumber", getIDNumber());
            json.put("blockedNumber", getBlockedNumber());

            json.put("isOKU", isOKU);

            return json.toString();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    public void debug ()
    {
        Log.e("Debug", getJSON("00000000", "username"));
    }
}
