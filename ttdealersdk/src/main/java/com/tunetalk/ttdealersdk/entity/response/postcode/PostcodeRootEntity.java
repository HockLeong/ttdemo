package com.tunetalk.ttdealersdk.entity.response.postcode;

import java.util.ArrayList;
import java.util.List;

public class PostcodeRootEntity
{
    List<String> mStateID = new ArrayList<>();
    List<String> mStateName = new ArrayList<>();
    List<String> mStateRegion = new ArrayList<>();

    PostcodeItemEntity[] postcodes;

    public void setup()
    {
        mStateID.add("1");
        mStateID.add("2");
        mStateID.add("3");
        mStateID.add("4");
        mStateID.add("5");
        mStateID.add("6");
        mStateID.add("7");
        mStateID.add("8");
        mStateID.add("9");
        mStateID.add("10");
        mStateID.add("11");
        mStateID.add("12");
        mStateID.add("13");
        mStateID.add("14");
        mStateID.add("15");
        mStateID.add("16");

        mStateName.add("JOHOR");                    mStateRegion.add("SOUTHERN");
        mStateName.add("KEDAH");                    mStateRegion.add("NORTHERN");
        mStateName.add("KELANTAN");                 mStateRegion.add("EAST");
        mStateName.add("MELAKA");                   mStateRegion.add("SOUTHERN");
        mStateName.add("NEGERI SEMBILAN");          mStateRegion.add("SOUTHERN");
        mStateName.add("PAHANG");                   mStateRegion.add("EAST");
        mStateName.add("PERAK");                    mStateRegion.add("NORTHERN");
        mStateName.add("PERLIS");                   mStateRegion.add("NORTHERN");
        mStateName.add("PULAU PINANG");             mStateRegion.add("NORTHERN");
        mStateName.add("SABAH");                    mStateRegion.add("SABAH");
        mStateName.add("SARAWAK");                  mStateRegion.add("SARAWAK");
        mStateName.add("SELANGOR");                 mStateRegion.add("CENTRAL");
        mStateName.add("TERENGGANU");               mStateRegion.add("EAST");
        mStateName.add("WP KUALA LUMPUR");          mStateRegion.add("CENTRAL");
        mStateName.add("WP LABUAN");                mStateRegion.add("SABAH");
        mStateName.add("WP PUTRAJAYA");             mStateRegion.add("CENTRAL");
    }

    public PostcodeItemEntity[] getPostcodes()
    {
        return postcodes;
    }

    public List<String> getStateRegion()
    {
        return mStateRegion;
    }

    public List<String> getStateID()
    {
        return mStateID;
    }

    public List<String> getStateName()
    {
        return mStateName;
    }
}
