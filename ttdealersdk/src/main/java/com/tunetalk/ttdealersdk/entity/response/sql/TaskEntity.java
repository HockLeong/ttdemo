package com.tunetalk.ttdealersdk.entity.response.sql;

public class TaskEntity
{
    String id;
    String simNo, username, json, insertdate;
    int type;

    public String getId ()
    {
        return id;
    }

    public TaskEntity setId (String id)
    {
        this.id = id;
        return this;
    }

    public String getSimNo ()
    {
        return simNo;
    }

    public TaskEntity setSimNo (String msisdn)
    {
        this.simNo = msisdn;
        return this;
    }

    public String getUsername()
    {
        return username;
    }

    public TaskEntity setUsername(String username)
    {
        this.username = username;
        return this;
    }

    public int getType()
    {
        return type;
    }

    public TaskEntity setType(int type)
    {
        this.type = type;
        return this;
    }

    public String getJSON()
    {
        return json;
    }

    public TaskEntity setJSON(String json)
    {
        this.json = json;
        return this;
    }

    public String getInsertDate()
    {
        return insertdate;
    }

    public TaskEntity setInsertDate(String insertdate)
    {
        this.insertdate = insertdate;
        return this;
    }
}
