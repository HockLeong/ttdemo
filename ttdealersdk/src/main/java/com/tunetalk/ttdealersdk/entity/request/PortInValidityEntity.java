package com.tunetalk.ttdealersdk.entity.request;

import com.tunetalk.ttdealersdk.util.Common;

import java.io.Serializable;

public class PortInValidityEntity implements Serializable
{
    String simNumber;
    String msisdnToPortIn;

    public String getSimNumber ()
    {
        return simNumber;
    }

    public PortInValidityEntity setSimNumber (String simNumber)
    {
        this.simNumber = simNumber;
        return this;
    }

    public String getMsisdnToPortIn ()
    {
        return msisdnToPortIn;
    }

    public PortInValidityEntity setMsisdnToPortIn (String msisdnToPortIn)
    {
        this.msisdnToPortIn = msisdnToPortIn;
        return this;
    }

    public boolean isAbleToPortIn ()
    {
        return (msisdnToPortIn != null && Common.IsMobileNumber(msisdnToPortIn)) && (simNumber.length() == 8);
    }

}
