package com.tunetalk.ttdealersdk.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.tunetalk.ttdealersdk.entity.response.sql.TaskEntity;
import com.tunetalk.ttdealersdk.util.Common;

import java.util.ArrayList;
import java.util.List;

public class SQLiteHandler extends SQLiteOpenHelper
{
    final String CREATE_AMSTERDAM = "CREATE TABLE amsterdam " +
        "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, simnum TEXT, username TEXT, type TINYINT, json TEXT, insertdate DATE)";

    //final String CREATE_LOG = "CREATE TABLE log (message TEXT, location TEXT)";

    Context mContext;
    SQLiteDatabase mDatabase;

    //DB version increase if any changes in table
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "hypertasking";
    private static final String TABLE_AMSTERDAM = "amsterdam";

    public static final String COLUMN_ID = "id";

    final String DATABASE_ALTER_AMSTERDAM_V2 = "ALTER TABLE "
        + TABLE_AMSTERDAM + " ADD COLUMN " + COLUMN_ID + " INTEGER;";

    public SQLiteHandler (Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
        mDatabase = getWritableDatabase();
    }

    @Override
    public void onCreate (SQLiteDatabase db)
    {
        db.execSQL(CREATE_AMSTERDAM);
        //db.execSQL(CREATE_LOG);
    }

    @Override
    public void onUpgrade (SQLiteDatabase db, int oldVersion, int newVersion)
    {
        mDatabase = db;

        if (db.getVersion() != newVersion)
        {
            switch (db.getVersion())
            {
                case 0:
                    onCreate(db);
                case 1:
                {
//                    db.execSQL(QUERY FOR v1 to v2);
//                    db.setVersion(CURRENT_NEW_DATABASE_VERSION = 2);
                }
            }
        }
        onOpen(db);
    }

    public boolean addTask (TaskEntity entity)
    {
        ContentValues values = new ContentValues();
//        values.put("id", entity.getId());
        values.put("simnum", entity.getSimNo());
        values.put("username", entity.getUsername());
        values.put("type", entity.getType());
        values.put("json", entity.getJSON());
        values.put("insertdate", entity.getInsertDate());

        try
        {
            long rowAffected = mDatabase.insert(TABLE_AMSTERDAM, null, values);
            return rowAffected > 0;
        }
        catch (SQLException sq)
        {
            return false;
        }
    }

    public void removeTask (String id)
    {
        mDatabase.delete(TABLE_AMSTERDAM, "id = '" + id + "'", null);
    }

    public void removeTaskByUUID (String uuid)
    {
        mDatabase.delete(TABLE_AMSTERDAM, "id = '" + uuid + "'", null);
    }

    public List<String> readPendingTask ()
    {
        String query = "SELECT simnum FROM " + TABLE_AMSTERDAM + " ORDER BY insertdate";
        List<String> list = new ArrayList<>();
        Cursor cursor = mDatabase.rawQuery(query, null);

        try
        {
            while (cursor.moveToNext())
                list.add(cursor.getString(cursor.getColumnIndex("simnum")));

            cursor.close();
            return list;
        }
        catch (SQLException sq)
        {
            sq.printStackTrace();

            return list;
        }
    }

    public TaskEntity readPendingTaskById (String taskId)
    {
        TaskEntity entity = new TaskEntity();

        String query = "SELECT * FROM " + TABLE_AMSTERDAM + " WHERE id = " + taskId + "";

        try
        {
            Cursor cursor = mDatabase.rawQuery(query, null);

            while (cursor.moveToNext())
            {
                entity.setSimNo(cursor.getString(cursor.getColumnIndex("simnum")));
                entity.setUsername(cursor.getString(cursor.getColumnIndex("username")));
                entity.setType(cursor.getInt(cursor.getColumnIndex("type")));
                entity.setJSON(cursor.getString(cursor.getColumnIndex("json")));
                entity.setInsertDate(cursor.getString(cursor.getColumnIndex("insertdate")));

                if (Common.isValidString(cursor.getString(cursor.getColumnIndex("id"))))
                    entity.setId(cursor.getString(cursor.getColumnIndex("id")));
            }

            cursor.close();
            return entity;
        }
        catch (SQLException sq)
        {
            sq.printStackTrace();
            return entity;
        }
    }

    public TaskEntity readSingleTask (String simnum)
    {
//        String query = "SELECT * FROM " + TABLE_AMSTERDAM + " WHERE simnum = '" + simnum + "'";
        String query = "SELECT * FROM " + TABLE_AMSTERDAM;
        TaskEntity entity = new TaskEntity();

        try
        {
            Cursor cursor = mDatabase.rawQuery(query, null);
            while (cursor.moveToNext())
            {
                entity.setSimNo(cursor.getString(cursor.getColumnIndex("simnum")));
                entity.setUsername(cursor.getString(cursor.getColumnIndex("username")));
                entity.setType(cursor.getInt(cursor.getColumnIndex("type")));
                entity.setJSON(cursor.getString(cursor.getColumnIndex("json")));
                entity.setInsertDate(cursor.getString(cursor.getColumnIndex("insertdate")));

                if (Common.isValidString(cursor.getString(cursor.getColumnIndex("id"))))
                    entity.setId(cursor.getString(cursor.getColumnIndex("id")));
            }
            cursor.close();
            return entity;
        }
        catch (SQLException dq)
        {
            dq.printStackTrace();

            return null;
        }
    }

    public List<TaskEntity> readAllPendingTasks ()
    {
        List<TaskEntity> pendingTaskList = new ArrayList<>();

        String query = "SELECT * FROM " + TABLE_AMSTERDAM;

        try
        {
            Cursor cursor = mDatabase.rawQuery(query, null);
            while (cursor.moveToNext())
            {
                TaskEntity entity = new TaskEntity();
                entity.setSimNo(cursor.getString(cursor.getColumnIndex("simnum")));
                entity.setUsername(cursor.getString(cursor.getColumnIndex("username")));
                entity.setType(cursor.getInt(cursor.getColumnIndex("type")));
                entity.setJSON(cursor.getString(cursor.getColumnIndex("json")));
                entity.setInsertDate(cursor.getString(cursor.getColumnIndex("insertdate")));

                if (Common.isValidString(cursor.getString(cursor.getColumnIndex("id"))))
                    entity.setId(cursor.getString(cursor.getColumnIndex("id")));

                pendingTaskList.add(entity);
            }

            cursor.close();
        }
        catch (SQLException dq)
        {
            dq.printStackTrace();
        }

        return pendingTaskList;
    }

    public TaskEntity configureUUID ()
    {
//        String query = "SELECT * FROM " + TABLE_AMSTERDAM + " WHERE simnum = '" + simnum + "'";
        String query = "SELECT * FROM " + TABLE_AMSTERDAM;
        TaskEntity entity = new TaskEntity();

        try
        {
            Cursor cursor = mDatabase.rawQuery(query, null);
            while (cursor.moveToNext())
            {
                entity.setSimNo(cursor.getString(cursor.getColumnIndex("simnum")));
                entity.setUsername(cursor.getString(cursor.getColumnIndex("username")));
                entity.setType(cursor.getInt(cursor.getColumnIndex("type")));
                entity.setJSON(cursor.getString(cursor.getColumnIndex("json")));
                entity.setInsertDate(cursor.getString(cursor.getColumnIndex("insertdate")));
                entity.setId(Common.getUUID());
            }

            cursor.close();
        }
        catch (SQLException dq)
        {
            dq.printStackTrace();

            return null;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return entity;
    }

    public List<TaskEntity> readTaskList ()
    {
        List<TaskEntity> mList = new ArrayList<>();
        String query = "SELECT simnum, username, type, insertdate FROM amsterdam ORDER BY insertdate";
        Cursor cursor = mDatabase.rawQuery(query, null);

        try
        {
            while (cursor.moveToNext())
            {
                mList.add(new TaskEntity()
                    .setSimNo(cursor.getString(0))
                    .setUsername(cursor.getString(1))
                    .setType(cursor.getInt(2))
                    .setInsertDate(cursor.getString(3)));
            }
            cursor.close();
            return mList;
        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    public void addLog (String message, String location)
    {
        ContentValues values = new ContentValues();
        values.put("message", message);
        values.put("location", location);

        try
        {
            mDatabase.insert("log", null, values);
        }
        catch (SQLException sq)
        {
            sq.printStackTrace();
        }
    }

    public List<String> readLog ()
    {
        List<String> log = new ArrayList<>();
        String query = "SELECT * FROM log";
        Cursor cursor = mDatabase.rawQuery(query, null);

        try
        {
            while (cursor.moveToNext())
            {
                log.add(cursor.getString(0));
            }
            cursor.close();
            return log;
        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    public void removeLog ()
    {
        mDatabase.delete("log", null, null);
    }
}
