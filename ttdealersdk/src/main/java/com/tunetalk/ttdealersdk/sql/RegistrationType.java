package com.tunetalk.ttdealersdk.sql;

public enum RegistrationType
{
    SIM_REGISTRATION(0),SIM_REPLACEMENT(1), PORT_IN (2);

    private final int type;

    RegistrationType(int type)
    {
        this.type = type;
    }

    public int getType()
    {
        return type;
    }

    @Override public String toString ()
    {
        return super.toString();
    }
}
