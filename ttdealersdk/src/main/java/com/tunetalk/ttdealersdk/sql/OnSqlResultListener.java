package com.tunetalk.ttdealersdk.sql;

import com.tunetalk.ttdealersdk.entity.response.sql.TaskEntity;

public interface OnSqlResultListener
{
    void onSuccess (TaskEntity entity);

    void onFailure();
}
