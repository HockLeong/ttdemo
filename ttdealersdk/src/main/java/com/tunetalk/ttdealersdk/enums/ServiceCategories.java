package com.tunetalk.ttdealersdk.enums;

public enum ServiceCategories
{
    SIM_REG_AUTO,
    SIM_REG_MANUAL,
    SIM_REPLACEMENT_AUTO,
    SIM_REPLACEMENT_MANUAL,
    PORT_IN_AUTO,
    PORT_IN_MANUAL
}
