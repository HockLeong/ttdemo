package com.tunetalk.ttdealersdk.api;

import com.cheese.geeksone.core.Container;
import com.cheese.geeksone.core.ContentType;
import com.cheese.geeksone.core.Geeksone;
import com.cheese.geeksone.core.OnGlobalListener;
import com.orhanobut.logger.Logger;
import com.tunetalk.ttdealersdk.BuildConfig;

public class ApiClient
{
    private static ApiClient mInstance;
    private static int CONNECTION_TIME_OUT = 60000;
    private static int READ_TIME_OUT = 60000;

    public static synchronized ApiClient get ()
    {
        if (mInstance == null)
            reset();

        return mInstance;
    }

    public static void reset ()
    {
        mInstance = new ApiClient();
    }

    public Geeksone getService ()
    {
        return new Geeksone(ContentType.JSON)
            .setTimeout(CONNECTION_TIME_OUT, READ_TIME_OUT)
            .setOnGlobalListener(new OnGlobalListener()
            {
                @Override public void OnGlobalListener (Container container, Geeksone async)
                {
                    if (BuildConfig.DEBUG)
                    {
                        Logger.json(async.getResponse());
                    }
                }
            });
    }

}
