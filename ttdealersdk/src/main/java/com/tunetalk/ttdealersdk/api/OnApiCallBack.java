package com.tunetalk.ttdealersdk.api;

public interface OnApiCallBack<T>
{
    void onSuccess (T response);

    void onFailure ();
}
