package com.tunetalk.ttdealersdk.api;

import java.util.Map;

public class Webservice
{
    private static String HOST;
    private static final String TT_SELFCARE_SDK_DEVELOPMENT_URL = "http://selfcare-partner-dev.ap-southeast-1.elasticbeanstalk.com/api/";
    private static final String TT_SELFCARE_SDK_PRODUCTION_URL = "http://selfcare-partner-dev.ap-southeast-1.elasticbeanstalk.com/api/";

    private static final String DEALER_DEVELOPMENT_URL = "http://dealer-partner-dev.ap-southeast-1.elasticbeanstalk.com/api/";
    private static final String DEALER_PRODUCTION_URL = "http://dealer-partner-dev.ap-southeast-1.elasticbeanstalk.com/api/";
//    private static final String DEALER_PRODUCTION_URL = "http://192.168.1.140:8080/api/";x

    public static boolean isProductionServer = true;
    public static Map<String, String> HEADER;

    public static String getTTSelfCareHost ()
    {
//        switch (BuildConfig.APP_MODE)
//        {
//            case 0:
//                HOST = TT_SELFCARE_SDK_DEVELOPMENT_URL;
//                break;
//            case 1:
//                HOST = TT_SELFCARE_SDK_PRODUCTION_URL;
//                break;
//            default:
//                HOST = TT_SELFCARE_SDK_PRODUCTION_URL;
//                break;
//        }

        HOST = isProductionServer ? TT_SELFCARE_SDK_PRODUCTION_URL : TT_SELFCARE_SDK_DEVELOPMENT_URL;
        return HOST;
    }

    public static String getTTSelfCareHost (String path, Object... args)
    {
        return String.format(getTTSelfCareHost() + path, args);
    }

    public static String getDealerHost ()
    {
        HOST = isProductionServer ? DEALER_PRODUCTION_URL : DEALER_DEVELOPMENT_URL;
        return HOST;
    }

    public static String getDealerHost (String path, Object... args)
    {
        return String.format(getDealerHost() + path, args);
    }

    public static boolean isValidSession ()
    {
        return HEADER != null && HEADER.get("apiKey") != null && ! HEADER.get("apiKey").isEmpty();
    }

    public interface URL
    {
        String INIT = "app/init";
        String RETRIEVE_SIM_NUMBER = "app/simNumber/%s";
        String SIM_REGISTRATION = "app/simReg";
        String PATH_MSISDN_LAST_4_DIGIT = "app/preferNumber/%s";
        String SIM_REPLACEMENT = "app/simRepl";
        String PORT_IN = "app/simPort";
    }
}
