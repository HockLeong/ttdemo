package com.tunetalk.ttdealersdk.api;

import android.app.Activity;
import android.content.DialogInterface;

import com.cheese.geeksone.core.Container;
import com.cheese.geeksone.core.Geeksone;
import com.cheese.geeksone.core.OnResultListener;
import com.tunetalk.ttdealersdk.R;
import com.tunetalk.ttdealersdk.entity.request.InitRequest;
import com.tunetalk.ttdealersdk.entity.request.PortInEntity;
import com.tunetalk.ttdealersdk.entity.request.SIMRegistrationEntity;
import com.tunetalk.ttdealersdk.entity.request.SIMReplacementEntity;
import com.tunetalk.ttdealersdk.entity.response.DesiredMSISDNListEntity;
import com.tunetalk.ttdealersdk.entity.response.InitEntity;
import com.tunetalk.ttdealersdk.entity.response.RegisterSimNumberEntity;
import com.tunetalk.ttdealersdk.entity.response.UploadEntity;
import com.tunetalk.ttdealersdk.util.ActivityUtils;
import com.tunetalk.ttdealersdk.util.Common;
import com.tunetalk.ttdealersdk.util.Constant;
import com.tunetalk.ttdealersdk.util.Make;

import org.json.JSONObject;

public class ApiProvider
{
    /**
     * Init the SDK for retrieve header and microblink license
     *
     * @param activity
     * @param apiKey
     * @param listener
     */
    public static void initSDK (final Activity activity, String apiKey, final OnApiCallBack<InitEntity> listener)
    {
        InitRequest req = new InitRequest()
//            .setAppId(activity.getPackageName())
            .setAppId("appid321")
            .setApiKey(apiKey)
            .setPlatform(Constant.Settings.PLATFORM);

        ApiClient.get().getService()
            .POST(new Container(Webservice.getDealerHost(Webservice.URL.INIT))
                .setRequestBody(req)
                .setOnResult(new OnResultListener()
                {
                    @Override public void OnResult (Boolean result, Container container, Geeksone async, Exception ex)
                    {
                        Make.ProgressDialog.Dismiss();
                        if (result)
                        {
                            InitEntity resp = async.getClazz(InitEntity.class);

                            if (listener != null)
                                listener.onSuccess(resp);
                        }
                        else
                        {
                            if (! activity.isFinishing())
                            {
                                Make.dialogBuilder(activity, activity.getString(R.string.error_unknown))
                                    .setPositiveButton(activity.getString(R.string.btn_okay), new DialogInterface.OnClickListener()
                                    {
                                        @Override public void onClick (DialogInterface dialogInterface, int i)
                                        {
                                            dialogInterface.dismiss();
                                        }
                                    })
                                    .show();
                            }

                            if (listener != null)
                                listener.onFailure();
                        }
                    }
                }));
    }

    public static void retrieveSimNumber (final Activity activity, String simNo, final OnApiCallBack<RegisterSimNumberEntity> listener)
    {
        String url = Webservice.getDealerHost(Webservice.URL.RETRIEVE_SIM_NUMBER, simNo);

        Make.ProgressDialog.Show(activity);

        ApiClient.get().getService()
            .GET(new Container(url)
                .setHeaders(Webservice.HEADER)
                .setOnResult(new OnResultListener()
                {
                    @Override public void OnResult (Boolean result, Container container, Geeksone async, Exception ex)
                    {
                        Make.ProgressDialog.Dismiss();

                        if (result)
                        {
                            RegisterSimNumberEntity resp = async.getClazz(RegisterSimNumberEntity.class);

                            if (listener != null)
                                listener.onSuccess(resp);
                        }
                        else
                        {
                            if (! activity.isFinishing())
                            {
                                Make.dialogBuilder(activity, activity.getString(R.string.error_unknown))
                                    .setPositiveButton(activity.getString(R.string.btn_okay), new DialogInterface.OnClickListener()
                                    {
                                        @Override public void onClick (DialogInterface dialogInterface, int i)
                                        {
                                            activity.finish();
                                            dialogInterface.dismiss();
                                        }
                                    })
                                    .show();
                            }

                            if (listener != null)
                                listener.onFailure();
                        }
                    }
                }));
    }

    public static void getPreferNumber (final Activity activity, String last4Digits, final OnApiCallBack<DesiredMSISDNListEntity> listener)
    {
        String url = Webservice.getDealerHost(Webservice.URL.PATH_MSISDN_LAST_4_DIGIT, last4Digits);

        Make.ProgressDialog.Show(activity);

        ApiClient.get().getService()
            .GET(new Container(url)
                .setHeaders(Webservice.HEADER)
                .setOnResult(new OnResultListener()
                {
                    @Override public void OnResult (Boolean result, Container container, Geeksone async, Exception ex)
                    {
                        Make.ProgressDialog.Dismiss();

                        if (result)
                        {
                            DesiredMSISDNListEntity resp = async.getClazz(DesiredMSISDNListEntity.class);

                            if (listener != null)
                                listener.onSuccess(resp);
                        }
                        else
                        {
                            if (! activity.isFinishing())
                            {
                                Make.dialogBuilder(activity, activity.getString(R.string.error_unknown))
                                    .setPositiveButton(activity.getString(R.string.btn_okay), new DialogInterface.OnClickListener()
                                    {
                                        @Override public void onClick (DialogInterface dialogInterface, int i)
                                        {
                                            activity.finish();
                                            dialogInterface.dismiss();
                                        }
                                    })
                                    .show();
                            }

                            if (listener != null)
                                listener.onFailure();
                        }
                    }
                }));
    }

    public static void simRegistration (final Activity activity, JSONObject jsonObject, final OnApiCallBack<UploadEntity> listener)
    {
        String url = Webservice.getDealerHost(Webservice.URL.SIM_REGISTRATION);

        Make.ProgressDialog.Show(activity);

        ApiClient.get().getService()
            .POST(new Container(url)
                .setHeaders(Webservice.HEADER)
                .setRequestBody(jsonObject)
                .setOnResult(new OnResultListener()
                {
                    @Override public void OnResult (Boolean result, Container container, Geeksone async, Exception ex)
                    {
                        Make.ProgressDialog.Dismiss();
                        UploadEntity resp = async.getClazz(UploadEntity.class);

                        if (result)
                        {
                            if (listener != null)
                                listener.onSuccess(resp);
                        }
                        else
                        {
                            if (! activity.isFinishing())
                            {
                                Make.dialogBuilder(activity, activity.getString(R.string.error_unknown))
                                    .setPositiveButton(activity.getString(R.string.btn_okay), new DialogInterface.OnClickListener()
                                    {
                                        @Override public void onClick (DialogInterface dialogInterface, int i)
                                        {
                                            dialogInterface.dismiss();
                                        }
                                    })
                                    .show();
                            }

                            if (listener != null)
                                listener.onFailure();
                        }
                    }
                }));
    }

    public static void simReplacement (final Activity activity, JSONObject jsonObject, final OnApiCallBack<UploadEntity> listener)
    {
        String url = Webservice.getDealerHost(Webservice.URL.SIM_REPLACEMENT);

        Make.ProgressDialog.Show(activity);

        ApiClient.get().getService()
            .POST(new Container(url)
                .setHeaders(Webservice.HEADER)
                .setRequestBody(jsonObject)
                .setOnResult(new OnResultListener()
                {
                    @Override public void OnResult (Boolean result, Container container, Geeksone async, Exception ex)
                    {
                        Make.ProgressDialog.Dismiss();
                        UploadEntity resp = async.getClazz(UploadEntity.class);

                        if (result)
                        {
                            if (listener != null)
                                listener.onSuccess(resp);
                        }
                        else
                        {
                            if (! activity.isFinishing())
                            {
                                Make.dialogBuilder(activity, activity.getString(R.string.error_unknown))
                                    .setPositiveButton(activity.getString(R.string.btn_okay), new DialogInterface.OnClickListener()
                                    {
                                        @Override public void onClick (DialogInterface dialogInterface, int i)
                                        {
                                            dialogInterface.dismiss();
                                        }
                                    })
                                    .show();
                            }

                            if (listener != null)
                                listener.onFailure();
                        }
                    }
                }));
    }

    public static void portIn (final Activity activity, JSONObject jsonObject, final OnApiCallBack<UploadEntity> listener)
    {
        String url = Webservice.getDealerHost(Webservice.URL.PORT_IN);

        Make.ProgressDialog.Show(activity);

        ApiClient.get().getService()
            .POST(new Container(url)
                .setHeaders(Webservice.HEADER)
                .setRequestBody(jsonObject)
                .setOnResult(new OnResultListener()
                {
                    @Override public void OnResult (Boolean result, Container container, Geeksone async, Exception ex)
                    {
                        Make.ProgressDialog.Dismiss();
                        UploadEntity resp = async.getClazz(UploadEntity.class);

                        if (result)
                        {
                            if (listener != null)
                                listener.onSuccess(resp);
                        }
                        else
                        {
                            if (! activity.isFinishing())
                            {
                                Make.dialogBuilder(activity, activity.getString(R.string.error_unknown))
                                    .setPositiveButton(activity.getString(R.string.btn_okay), new DialogInterface.OnClickListener()
                                    {
                                        @Override public void onClick (DialogInterface dialogInterface, int i)
                                        {
                                            dialogInterface.dismiss();
                                        }
                                    })
                                    .show();
                            }

                            if (listener != null)
                                listener.onFailure();
                        }
                    }
                }));
    }

}
