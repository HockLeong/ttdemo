package com.tunetalk.ttdealersdk.api;

import android.content.Context;

import com.tunetalk.ttdealersdk.R;
import com.tunetalk.ttdealersdk.entity.request.PortInEntity;
import com.tunetalk.ttdealersdk.entity.request.SIMRegistrationEntity;
import com.tunetalk.ttdealersdk.entity.request.SIMReplacementEntity;
import com.tunetalk.ttdealersdk.entity.response.sql.TaskEntity;
import com.tunetalk.ttdealersdk.sql.RegistrationType;
import com.tunetalk.ttdealersdk.util.Base64;
import com.tunetalk.ttdealersdk.util.Cipher;
import com.tunetalk.ttdealersdk.util.Predator;

import org.json.JSONObject;

import java.util.UUID;

public class ApiRequestBody
{
    public static JSONObject getSimRegRequestBody (Context context, SIMRegistrationEntity sim)
    {
        try
        {
            final String first8Digit = context.getResources().getString(R.string.simreg_text_first_8_digit);
            String json = sim.getJSON(first8Digit, sim.getStaffLoginId());
            String password = UUID.randomUUID().toString().replaceAll("-", "");
            Cipher cipher = new Cipher(password);

            byte[] encryptedPassword = new Predator()
                .setRSAPublicKey(context.getAssets().open("public.der"), Predator.Key.RSA)
                .encryptRSA(password, Predator.Algorithm.RSA_NONE_PKCS1PADDING);

            password = null;

            byte[] encryptedData = cipher.encrypt(json.getBytes());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("key", Base64.encodeBytes(encryptedPassword));
            jsonObject.put("data", Base64.encodeBytes(encryptedData));

            return jsonObject;

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return new JSONObject();
        }
    }

    public static JSONObject getSimReplacementRequestBody (Context context, SIMReplacementEntity entity)
    {
        try
        {
            final String first8Digit = context.getResources().getString(R.string.simreg_text_first_8_digit);
            String json = entity.getJSON(first8Digit, entity.getStaffLoginId());
            String password = UUID.randomUUID().toString().replaceAll("-", "");
            Cipher cipher = new Cipher(password);

            byte[] encryptedPassword = new Predator()
                .setRSAPublicKey(context.getAssets().open("public.der"),
                    Predator.Key.RSA)
                .encryptRSA(password, Predator.Algorithm.RSA_NONE_PKCS1PADDING);

            byte[] encryptedData = cipher.encrypt(json.getBytes());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("key", Base64.encodeBytes(encryptedPassword));
            jsonObject.put("data", Base64.encodeBytes(encryptedData));

            return jsonObject;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return new JSONObject();
        }
    }

    public static JSONObject getPortInRequestBody (Context context, PortInEntity entity)
    {
        try
        {
            final String first8Digit = context.getResources().getString(R.string.simreg_text_first_8_digit);
            String json = entity.getJSON(first8Digit, entity.getStaffLoginId());
            String password = UUID.randomUUID().toString().replaceAll("-", "");
            Cipher cipher = new Cipher(password);

            byte[] encryptedPassword = new Predator()
                .setRSAPublicKey(context.getAssets().open("public.der"),
                    Predator.Key.RSA)
                .encryptRSA(password, Predator.Algorithm.RSA_NONE_PKCS1PADDING);

            byte[] encryptedData = cipher.encrypt(json.getBytes());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("key", Base64.encodeBytes(encryptedPassword));
            jsonObject.put("data", Base64.encodeBytes(encryptedData));

            return jsonObject;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return new JSONObject();
        }
    }
}
