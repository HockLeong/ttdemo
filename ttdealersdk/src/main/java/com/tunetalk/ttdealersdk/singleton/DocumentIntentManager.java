package com.tunetalk.ttdealersdk.singleton;

import com.tunetalk.ttdealersdk.entity.response.intent.DocumentIntentEntity;

public class DocumentIntentManager
{
    private DocumentIntentEntity mIntentEntity;
    private static DocumentIntentManager mInstance;

    public synchronized static DocumentIntentManager init ()
    {
        if (mInstance == null)
            reset();

        return mInstance;
    }

    public static void reset ()
    {
        mInstance = new DocumentIntentManager();
    }

    public DocumentIntentEntity getIntentEntity ()
    {
        if(mIntentEntity == null)
            mIntentEntity = new DocumentIntentEntity();

        return mIntentEntity;
    }

    public DocumentIntentManager setIntentEntity (DocumentIntentEntity mIntentEntity)
    {
        this.mIntentEntity = mIntentEntity;
        return this;
    }
}
