package com.tunetalk.ttdealersdk.singleton;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import com.tunetalk.ttdealersdk.R;
import com.tunetalk.ttdealersdk.api.ApiRequestBody;
import com.tunetalk.ttdealersdk.entity.request.PortInEntity;
import com.tunetalk.ttdealersdk.entity.request.SIMRegistrationEntity;
import com.tunetalk.ttdealersdk.entity.request.SIMReplacementEntity;
import com.tunetalk.ttdealersdk.entity.response.sql.TaskEntity;
import com.tunetalk.ttdealersdk.sql.OnSqlResultListener;
import com.tunetalk.ttdealersdk.sql.RegistrationType;
import com.tunetalk.ttdealersdk.sql.SQLiteHandler;
import com.tunetalk.ttdealersdk.util.Base64;
import com.tunetalk.ttdealersdk.util.Cipher;
import com.tunetalk.ttdealersdk.util.Make;
import com.tunetalk.ttdealersdk.util.Predator;

import org.json.JSONObject;

import java.util.UUID;

public class DatabaseManager
{
    private static DatabaseManager mInstance;

    public synchronized static DatabaseManager get ()
    {
        if (mInstance == null)
            reset();

        return mInstance;
    }

    public static void reset ()
    {
        mInstance = new DatabaseManager();
    }

    public void persistSimRegRecord (Context context, SIMRegistrationEntity sim, OnSqlResultListener listener)
    {
        try
        {
            final String first8Digit = context.getResources().getString(R.string.simreg_text_first_8_digit);

            JSONObject jsonObject = ApiRequestBody.getSimRegRequestBody(context, sim);

            TaskEntity task = new TaskEntity()
//                .setId(Common.getUUID())
                .setSimNo(first8Digit + sim.getLast8Digit())
                .setUsername(sim.getStaffLoginId())
                .setType(RegistrationType.SIM_REGISTRATION.getType())
                .setJSON(jsonObject.toString())
                .setInsertDate(sim.getDate());

            persist(context, task, listener);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void persistSimReplacementRecord (Context context, SIMReplacementEntity entity, OnSqlResultListener listener)
    {
        try
        {
            final String first8Digit = context.getResources().getString(R.string.simreg_text_first_8_digit);
            String json = entity.getJSON(first8Digit, entity.getStaffLoginId());
            String password = UUID.randomUUID().toString().replaceAll("-", "");
            Cipher cipher = new Cipher(password);

            byte[] encryptedPassword = new Predator()
                .setRSAPublicKey(context.getAssets().open("public.der"),
                    Predator.Key.RSA)
                .encryptRSA(password, Predator.Algorithm.RSA_NONE_PKCS1PADDING);

            byte[] encryptedData = cipher.encrypt(json.getBytes());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("key", Base64.encodeBytes(encryptedPassword));
            jsonObject.put("data", Base64.encodeBytes(encryptedData));

            TaskEntity task = new TaskEntity()
//                .setId(Common.getUUID())
                .setSimNo(first8Digit + entity.getLast8Digit())
                .setUsername(entity.getStaffLoginId())
                .setType(RegistrationType.SIM_REPLACEMENT.getType())
                .setJSON(jsonObject.toString())
                .setInsertDate(entity.getDate());

            persist(context, task, listener);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void persistPortInRecord (Context context, PortInEntity mPortInEntity, OnSqlResultListener listener)
    {
        try
        {
            final String first8Digit = context.getResources().getString(R.string.simreg_text_first_8_digit);
            String json = mPortInEntity.getJSON(first8Digit, mPortInEntity.getStaffLoginId());
            String password = UUID.randomUUID().toString().replaceAll("-", "");
            Cipher cipher = new Cipher(password);

            byte[] encryptedPassword = new Predator()
                .setRSAPublicKey(context.getAssets().open("public.der"),
                    Predator.Key.RSA)
                .encryptRSA(password, Predator.Algorithm.RSA_NONE_PKCS1PADDING);

            byte[] encryptedData = cipher.encrypt(json.getBytes());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("key", Base64.encodeBytes(encryptedPassword));
            jsonObject.put("data", Base64.encodeBytes(encryptedData));

            TaskEntity task = new TaskEntity()
//                .setId(Common.getUUID())
                .setSimNo(first8Digit + mPortInEntity.getLast8Digit())
                .setUsername(mPortInEntity.getStaffLoginId())
                .setType(RegistrationType.PORT_IN.getType())
                .setJSON(jsonObject.toString())
                .setInsertDate(mPortInEntity.getDate());

            persist(context, task, listener);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @SuppressLint ("StaticFieldLeak")
    private void persist (final Context context, final TaskEntity taskEntity, final OnSqlResultListener listener)
    {
        final SQLiteHandler mSQLite = new SQLiteHandler(context);
        new AsyncTask<Void, Void, Boolean>()
        {
            @Override
            protected void onPreExecute ()
            {
                super.onPreExecute();
                Make.ProgressDialog.Show(context);
            }

            @Override
            protected void onPostExecute (Boolean result)
            {
                super.onPostExecute(result);
                Make.ProgressDialog.Dismiss();

                if (result)
                {
                    if (listener != null)
                        listener.onSuccess(taskEntity);
                    //  setResult(RESULT_OK, getIntent());
                }
                else
                {
                    if (listener != null)
                        listener.onFailure();
                }
            }

            @Override
            protected Boolean doInBackground (Void... voids)
            {
                try
                {
                    return mSQLite.addTask(taskEntity);
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                    return false;
                }
            }
        }.execute();
    }
}
