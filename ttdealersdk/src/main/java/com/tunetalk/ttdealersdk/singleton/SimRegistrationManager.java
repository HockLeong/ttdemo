package com.tunetalk.ttdealersdk.singleton;

import com.tunetalk.ttdealersdk.entity.request.SIMRegistrationEntity;

public class SimRegistrationManager
{
    private static SimRegistrationManager mInstance;
    private SIMRegistrationEntity mSimRegistrationEntity;

    public synchronized static SimRegistrationManager get ()
    {
        if (mInstance == null)
            reset();

        return mInstance;
    }

    public static void reset ()
    {
        mInstance = new SimRegistrationManager();
    }

    public SIMRegistrationEntity getSimRegistrationEntity ()
    {
        return mSimRegistrationEntity;
    }

    public SimRegistrationManager setSimRegistrationEntity (SIMRegistrationEntity simRegistrationEntity)
    {
        this.mSimRegistrationEntity = simRegistrationEntity;
        return this;
    }
}
