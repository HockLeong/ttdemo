package com.tunetalk.ttdealersdk.activity;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.microblink.entities.recognizers.RecognizerBundle;
import com.microblink.uisettings.DocumentUISettings;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.orhanobut.logger.Logger;
import com.rey.material.app.DatePickerDialog;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.widget.Button;
import com.rey.material.widget.CheckBox;
import com.rey.material.widget.EditText;
import com.rey.material.widget.RadioButton;
import com.tunetalk.ttdealersdk.R;
import com.tunetalk.ttdealersdk.api.ApiProvider;
import com.tunetalk.ttdealersdk.api.ApiRequestBody;
import com.tunetalk.ttdealersdk.api.OnApiCallBack;
import com.tunetalk.ttdealersdk.base.BaseActivity;
import com.tunetalk.ttdealersdk.entity.request.SIMRegistrationEntity;
import com.tunetalk.ttdealersdk.entity.response.RegisterSimNumberEntity;
import com.tunetalk.ttdealersdk.entity.response.UploadEntity;
import com.tunetalk.ttdealersdk.entity.response.postcode.PostcodeItemEntity;
import com.tunetalk.ttdealersdk.entity.response.postcode.PostcodeRootEntity;
import com.tunetalk.ttdealersdk.entity.response.sql.TaskEntity;
import com.tunetalk.ttdealersdk.enums.ServiceCategories;
import com.tunetalk.ttdealersdk.ocr.DocumentType;
import com.tunetalk.ttdealersdk.ocr.OCRHelper;
import com.tunetalk.ttdealersdk.ocr.OnScanResult;
import com.tunetalk.ttdealersdk.ocr.entity.ScanResultEntity;
import com.tunetalk.ttdealersdk.ocr.entity.ScanResultItemEntity;
import com.tunetalk.ttdealersdk.singleton.DatabaseManager;
import com.tunetalk.ttdealersdk.singleton.DocumentIntentManager;
import com.tunetalk.ttdealersdk.singleton.SimRegistrationManager;
import com.tunetalk.ttdealersdk.sql.OnSqlResultListener;
import com.tunetalk.ttdealersdk.util.ActivityUtils;
import com.tunetalk.ttdealersdk.util.Base64;
import com.tunetalk.ttdealersdk.util.Common;
import com.tunetalk.ttdealersdk.util.Constant;
import com.tunetalk.ttdealersdk.util.FileUtils;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class SIMRegistrationAutoActivity extends BaseActivity
{
    private final int OCR_REQUEST_CODE = 95;
    private final int OCR_BARCODE_REQUEST_CODE = 96;
    private final int MOBILE_NUMBER_CODE = 97;

    private int mDuration = 1000;

    ScrollView svSimReg;
    CheckBox chkIsMalaysian;
    Button btnNext, btnDOB, btnNationality, btnState, btnCity, btnCountry;
    EditText etLast8Digit, etMobile, etFullName, etAlternatePhone, etEMAIL;
    EditText etNIRC, etPassport, etAddress, etPostcode, etState, etCity;
    RadioButton rbMale, rbFemale, rbOrderId, rbMSISDN;
    LinearLayout[] llRow;
    LinearLayout llConnection;
    TextView tvConnection;
    ImageView ivPhoto1;
    Switch swOku;
    byte[] mBuffer1;
    boolean isMalaysian = true;
    boolean isActionFromMenu = false;
    Boolean isMale = null;
    Boolean isMSISDN = null;
    boolean isConnected = true;
    String mDOB = null, mNationality = null, mState = null, mCity = null, mCountry = null;
    String mobileNumber;
    PostcodeRootEntity mPostcodeEntity;
    StyleableToast mStyleableToast;
    Timer mTimer;

    boolean isActivityExist = true;
    boolean isNumbered;
    String mReferenceSimNumber;
    RegisterSimNumberEntity mRegisterSimEntity;
    RecognizerBundle mRecognizerBundle;

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sim_auto_registration);
        setToolbarTitle(R.string.activity_sim_reg);

        mConnectionCount = 0;

        findView(); //Included declare some listener here
        btnNextListener(); //EditText condition checking
        ButtonListener();

        new Runnable()
        {
            @Override
            public void run ()
            {
                String json = readJsonFromAssets("Postcode.json");
                if (json != null && ! json.isEmpty())
                {
                    mPostcodeEntity = new Gson().fromJson(json, PostcodeRootEntity.class);
                    mPostcodeEntity.setup();
                }
            }
        }.run();

        getCaptureMode(DocumentIntentManager.init().getIntentEntity().getDocumentType());

        chkIsMalaysian.setEnabled(false);

        isMSISDN = true;
        rbMSISDN.setChecked(true);
        rbOrderId.setChecked(false);
    }

    void CheckNetworkState ()
    {
        new Handler().postDelayed(new Runnable()
        {
            @Override public void run ()
            {
                try
                {
                    if (Common.hasConnectivity(getApplicationContext()))
                    {
                        llConnection.setVisibility(View.GONE);

                        if (! isConnected && etMobile.getText().toString().equals("") && mRegisterSimEntity == null)
                            retrieveSimNumber(getString(R.string.simreg_text_first_8_digit) + mReferenceSimNumber);

                        mDuration = 1000;
                        mConnectionCount = 0;
                        isConnected = true;
                    }
                    else
                    {

                        llConnection.setVisibility(View.VISIBLE);
                        tvConnection.setText(getConnectionStatus());

                        isMSISDN = false;
                        rbMSISDN.setChecked(false);
                        rbOrderId.setChecked(true);
                        mDuration = 5000;
                        isConnected = false;
                    }
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }

                CheckNetworkState();

            }
        }, mDuration);
    }

    private void getCaptureMode (DocumentType mode)
    {
        switch (mode)
        {
            case MYKAD:
                capture(0);
                break;
            case MRTD:
                chkIsMalaysian.setChecked(true);
                capture(1);
                break;
            case IKAD:
                chkIsMalaysian.setChecked(true);
                capture(2);
                break;
            case WORK_PERMIT:
                chkIsMalaysian.setChecked(true);
                capture(3);
                break;
            case VISA:
                chkIsMalaysian.setChecked(true);
                capture(4);
                break;
            case BARCODE:
                capture(5);
            default:
                break;
        }
    }

    private String readJsonFromAssets (String filename)
    {
        try
        {
            InputStream is = this.getAssets().open(filename);
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            is.close();
            return new String(buffer, "UTF-8");
        }
        catch (IOException io)
        {
            io.printStackTrace();
            return null;
        }
    }

    private void findView ()
    {
        //CAUTION: FIXED ARRAY SIZE
        //TODO: Update LinearLayout value/size/whatever according to Layout
        llRow = new LinearLayout[14];
        llRow[0] = (LinearLayout) findViewById(R.id.simreg_ll_row1);
        llRow[1] = (LinearLayout) findViewById(R.id.simreg_ll_row2);
        llRow[2] = (LinearLayout) findViewById(R.id.simreg_ll_row3);
//        llRow[2] = (LinearLayout) findViewById(R.id.simreg_ll_row3);
        llRow[3] = (LinearLayout) findViewById(R.id.simreg_ll_row4);
        llRow[4] = (LinearLayout) findViewById(R.id.simreg_ll_row5);
        llRow[5] = (LinearLayout) findViewById(R.id.simreg_ll_row6);
        llRow[6] = (LinearLayout) findViewById(R.id.simreg_ll_row7);
        llRow[7] = (LinearLayout) findViewById(R.id.simreg_ll_row8);
        llRow[8] = (LinearLayout) findViewById(R.id.simreg_ll_row9);
        llRow[9] = (LinearLayout) findViewById(R.id.simreg_ll_row10);
        llRow[10] = (LinearLayout) findViewById(R.id.simreg_ll_row11);
        llRow[11] = (LinearLayout) findViewById(R.id.simreg_ll_row12);
        llRow[12] = (LinearLayout) findViewById(R.id.simreg_ll_row13);
        llRow[13] = (LinearLayout) findViewById(R.id.simreg_ll_row14);

        svSimReg = (ScrollView) findViewById(R.id.simreg_scrollview);
        btnNext = (Button) findViewById(R.id.simreg_btnNext);
        btnDOB = (Button) findViewById(R.id.simreg_btnDOB);
        btnNationality = (Button) findViewById(R.id.simreg_btnNationality);
        btnState = (Button) findViewById(R.id.simreg_btnState);
        btnCity = (Button) findViewById(R.id.simreg_btnCity);
        chkIsMalaysian = (CheckBox) findViewById(R.id.simreg_chkIsMalaysian);
        rbMale = (RadioButton) findViewById(R.id.simreg_rdMale);
        rbFemale = (RadioButton) findViewById(R.id.simreg_rdFemale);
        rbOrderId = (RadioButton) findViewById(R.id.simreg_rdOrderId);
        rbMSISDN = (RadioButton) findViewById(R.id.simreg_rdMsisdn);

        etLast8Digit = (EditText) findViewById(R.id.simreg_etLast8Digit);
        etMobile = (EditText) findViewById(R.id.simreg_etMobile);
        etFullName = (EditText) findViewById(R.id.simreg_etFullName);
        etAlternatePhone = (EditText) findViewById(R.id.simreg_etAlternateNumber);
        etEMAIL = (EditText) findViewById(R.id.simreg_etEmail);
        etNIRC = (EditText) findViewById(R.id.simreg_etNIRC);
        etPassport = (EditText) findViewById(R.id.simreg_etPassport);
        etAddress = (EditText) findViewById(R.id.simreg_etAddress);
        etPostcode = (EditText) findViewById(R.id.simreg_etPostcode);
        etState = (EditText) findViewById(R.id.simreg_etState);
        etCity = (EditText) findViewById(R.id.simreg_etCity);
        btnCountry = (Button) findViewById(R.id.simreg_btnCountry);
        ivPhoto1 = (ImageView) findViewById(R.id.ivPhoto1);

        llConnection = (LinearLayout) findViewById(R.id.llConnection);
        tvConnection = (TextView) findViewById(R.id.tvConnection);
        swOku = findViewById(R.id.swOku);

        etPostcode.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        etPassport.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        etPassport.setEnabled(false);

        etLast8Digit.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange (View view, boolean focused)
            {
                if (focused)
                {
                    etLast8Digit.setHint(getResources().getString(R.string.simreg_text_last_8digit));
                    etLast8Digit.setTextColor(getResources().getColor(R.color.TextPrimaryDark));
                }
                else if (! focused && etLast8Digit.length() != 8 && etLast8Digit.length() == 0)
                {
                    etLast8Digit.setHint(getResources().getString(R.string.error_simreg_last8digit));
                    etLast8Digit.setTextColor(getResources().getColor(R.color.TextError));
                    animateErrorBounceWithCustomMessage(etLast8Digit, getResources().getString(R.string.snackbar_wrong_sim_num));
                }
                else
                {
                    etLast8Digit.setHint(getResources().getString(R.string.simreg_text_last_8digit));
                    etLast8Digit.setTextColor(getResources().getColor(R.color.TextPrimaryDark));

                    if (mReferenceSimNumber != null && ! mReferenceSimNumber.equals(etLast8Digit.getText().toString()))
                        retrieveSimNumber(getString(R.string.simreg_text_first_8_digit) + etLast8Digit.getText().toString());
                }
            }
        });

        etLast8Digit.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etLast8Digit.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override public boolean onEditorAction (TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_DONE)
                {
                    retrieveSimNumber(getString(R.string.simreg_text_first_8_digit) +
                        etLast8Digit.getText().toString());
                }
                return false;
            }
        });

        etNIRC.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange (View view, boolean focused)
            {
                if (focused)
                {
                    etNIRC.setHint(getResources().getString(R.string.common_text_NIRC));
                    etNIRC.setTextColor(getResources().getColor(R.color.TextPrimaryDark));
                }
                else if (! focused && etNIRC.length() != 12 && etNIRC.length() != 0)
                {
                    etNIRC.setHint(getResources().getString(R.string.error_simreg_nirc));
                    etNIRC.setTextColor(getResources().getColor(R.color.TextError));
                    animateErrorBounceWithCustomMessage(etNIRC, getResources().getString(R.string.snackbar_wrong_nirc));
                }
                else
                {
                    etNIRC.setHint(getResources().getString(R.string.common_text_NIRC));
                    etNIRC.setTextColor(getResources().getColor(R.color.TextPrimaryDark));
                }
            }
        });

        etNIRC.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged (CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged (CharSequence charSequence, int i, int i1, int i2)
            {
                if (charSequence.length() == 12)
                {
                    String nirc = charSequence.toString();
                    String gender = nirc.charAt(nirc.length() - 1) + "";
                    String dob = nirc.substring(0, 6);

                    int genderCode = Integer.parseInt(gender);

                    if (genderCode % 2 == 1)
                    {
                        isMale = true;
                        rbMale.setChecked(true);
                    }
                    else
                    {
                        isMale = false;
                        rbFemale.setChecked(true);
                    }

                    int day = Integer.parseInt(dob.substring(4, 6));
                    int month = Integer.parseInt(dob.substring(2, 4));
                    int year = 0;
                    try
                    {
                        Date deliverDate = Common.getAdjustedDate(dob.substring(0, 2));
                        String dateString2 = new SimpleDateFormat("yyyy", Locale.getDefault()).format(deliverDate);
                        year = Integer.parseInt(dateString2);
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }

                    if (day > 0 && day <= 31 && month > 0 && month <= 12 && year > mCurrentYear - 100 && year <= mCurrentYear - 12)
                    {
                        try
                        {
                            String date = year + "-" + month + "-" + day;
                            Date bod = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(date);
                            mDOB = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(bod);
                            btnDOB.setText(mDOB);
                        }
                        catch (Exception ex)
                        {
                            ex.printStackTrace();
                        }
                    }
                    else
                    {
                        if (year > mCurrentYear - 12)
                        {
                            etNIRC.setHint(getResources().getString(R.string.error_simreg_nirc));
                            etNIRC.setTextColor(getResources().getColor(R.color.TextError));
                            animateErrorBounceWithCustomMessage(btnDOB, "Age cannot younger than 12 years old.");
                        }
                        else
                        {
                            etNIRC.setHint(getResources().getString(R.string.error_simreg_nirc));
                            etNIRC.setTextColor(getResources().getColor(R.color.TextError));
                            animateErrorBounceWithCustomMessage(etNIRC, getResources().getString(R.string.snackbar_wrong_nirc));
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged (Editable editable)
            {

            }
        });

        etPostcode.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged (CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged (final CharSequence charSequence, int i, int i1, int i2)
            {
                if (charSequence.length() == 5)
                {
                    new Runnable()
                    {
                        @Override
                        public void run ()
                        {
                            if (mPostcodeEntity != null)
                            {
                                String code = charSequence.toString();
                                PostcodeItemEntity[] mPostcodes = mPostcodeEntity.getPostcodes();

                                for (int i = 0; i < mPostcodes.length - 1; ++ i)
                                {
                                    if (code.equals(mPostcodes[i].getPostcode()))
                                    {
                                        mCountry = "MALAYSIA";
                                        btnCountry.setText(mCountry);

                                        mCity = mPostcodes[i].getCityName();
                                        btnCity.setText(mCity);

                                        for (int j = 0; j < mPostcodeEntity.getStateID().size(); ++ j)
                                        {
                                            if (mPostcodeEntity.getStateID().get(j).equals(mPostcodes[i].getStateId()))
                                            {
                                                mState = mPostcodeEntity.getStateName().get(j);
                                                btnState.setText(mState);
                                            }
                                        }

                                        if (mCountry.equals("MALAYSIA"))
                                        {
                                            btnState.setVisibility(View.VISIBLE);
                                            etState.setVisibility(View.GONE);
                                            btnCity.setVisibility(View.VISIBLE);
                                            etCity.setVisibility(View.GONE);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }.run();
                }
            }

            @Override
            public void afterTextChanged (Editable editable)
            {

            }
        });

        etMobile.addTextChangedListener(new TextWatcher()
        {
            @Override public void beforeTextChanged (CharSequence s, int start, int count, int after)
            {

            }

            @Override public void onTextChanged (CharSequence s, int start, int before, int count)
            {
                if (isMSISDN)
                {
                    mobileNumber = s.toString();
                }
            }

            @Override public void afterTextChanged (Editable s)
            {

            }
        });

        chkIsMalaysian.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged (CompoundButton compoundButton, boolean isChecked)
            {
                btnNationality.setEnabled(isChecked);

                isMalaysian = ! isChecked;
                animateNonMalaysian(! isMalaysian);

                findViewById(R.id.llSpecialNeeds).setVisibility(isMalaysian ? View.VISIBLE : View.GONE);
            }
        });

        CompoundButton.OnCheckedChangeListener genderListener = new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged (CompoundButton buttonView, boolean isChecked)
            {
                if (isChecked)
                {
                    rbMale.setChecked(rbMale == buttonView);
                    rbFemale.setChecked(rbFemale == buttonView);

                    isMale = rbMale.isChecked();
                }
            }
        };

        rbMale.setOnCheckedChangeListener(genderListener);
        rbFemale.setOnCheckedChangeListener(genderListener);

        CompoundButton.OnCheckedChangeListener typeListener = new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged (CompoundButton buttonView, boolean isChecked)
            {
                if (isChecked)
                {
                    if (isConnected == false)
                    {
                        rbOrderId.setChecked(true);
                        rbMSISDN.setChecked(false);
                        new AlertDialog.Builder(mActivity)
                            .setMessage(R.string.offline_order_id)
                            .setPositiveButton("OK", null)
                            .show();
                    }
                    else
                    {
                        rbMSISDN.setChecked(rbMSISDN == buttonView);
                        rbOrderId.setChecked(rbOrderId == buttonView);
                    }

                    isMSISDN = rbMSISDN.isChecked();

                    etMobile.clearFocus();

                    if (isMSISDN)
                    {
                        etMobile.setText(mobileNumber);
                        etMobile.setInputType(InputType.TYPE_CLASS_PHONE);
                        etMobile.setHint(getString(R.string.common_text_mobile));
                    }
                    else
                    {
                        etMobile.setText(null);
                        etMobile.setInputType(InputType.TYPE_CLASS_TEXT);
                        etMobile.setHint(getString(R.string.order_id));
                    }
                }
            }
        };

        rbOrderId.setOnCheckedChangeListener(typeListener);
        rbMSISDN.setOnCheckedChangeListener(typeListener);

        swOku.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override public void onCheckedChanged (CompoundButton buttonView, boolean isChecked)
            {
                btnNext.setText(isChecked
                    ? getString(R.string.common_text_next)
                    : getString(R.string.btn_register));
            }
        });

    }

    private void btnNextListener ()
    {
        final int offset = 20;
        btnNext.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                try
                {
                    Date nirc = null, dob = null;
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

                    if (isMalaysian)
                    {
                        if (etNIRC.getText().toString().length() > 0)
                        {
                            String year = sdf.format(Common.getAdjustedDate(etNIRC.getText().toString().substring(0, 2)));

                            nirc = sdf.parse(year.substring(0, 4) + "-" + etNIRC.getText().toString().substring(2, 4)
                                + "-" + etNIRC.getText().toString().substring(4, 6));
                        }
                        else
                        {
                            animateErrorBounceWithCustomMessage(etNIRC, "Looks like something went wrong scanning your document");
                        }
                    }

                    dob = sdf.parse(mDOB);
                    sdf = new SimpleDateFormat("yyyy", Locale.getDefault());
                    int year = Integer.parseInt(sdf.format(dob));


                    if (etLast8Digit.length() != 8)
                    {
                        svSimReg.smoothScrollTo(0, llRow[0].getTop() - offset);
                        animateErrorBounceWithCustomMessage(etLast8Digit, getString(R.string.snackbar_something_not_correct));
                    }
                    else if (etMobile.getVisibility() == View.VISIBLE && etMobile.getText().toString().length() == 0 && isConnected)
                    {
                        svSimReg.smoothScrollTo(0, llRow[1].getTop() - offset);
                        animateErrorBounce(etMobile);
                    }
                    else if (! Common.IsMobileNumber(etMobile.getText().toString()) && etMobile.getVisibility() == View.VISIBLE && isMSISDN)
                    {
                        svSimReg.smoothScrollTo(0, llRow[1].getTop() - offset);
                        animateErrorBounce(etMobile);
                    }
                    else if (etMobile.getVisibility() == View.VISIBLE && isMSISDN == false && isConnected && etMobile.getText().toString().length() == 0)
                    {
                        svSimReg.smoothScrollTo(0, llRow[1].getTop() - offset);
                        animateErrorBounce(etMobile);
                    }
                    else if (etFullName.length() == 0)
                    {
                        svSimReg.smoothScrollTo(0, llRow[2].getTop() - offset);
                        animateErrorBounce(etFullName);
                    }
                    else if (! Common.IsName(chkIsMalaysian.isChecked(), etFullName.getText().toString()))
                    {
                        svSimReg.smoothScrollTo(0, llRow[2].getTop() - offset);
                        animateErrorBounceWithCustomMessage(etFullName, "Please make sure full name is correct.");
                    }
                    else if (! isMalaysian && mNationality == null)
                    {
                        svSimReg.smoothScrollTo(0, llRow[3].getTop() - offset);
                        animateErrorBounceWithoutFocus(btnNationality);
                    }
                    else if (! isMalaysian && etPassport.length() == 0)
                    {
                        svSimReg.smoothScrollTo(0, llRow[4].getTop() - offset);
                        animateErrorBounce(etPassport);
                    }
                    else if (isMalaysian && etNIRC.length() != 12)
                    {
                        svSimReg.smoothScrollTo(0, llRow[4].getTop() - offset);
                        animateErrorBounceWithCustomMessage(etNIRC, getString(R.string.snackbar_something_not_correct));
                    }
                    else if (isMalaysian && ! Common.IsNIRC(etNIRC.getText().toString(), mCurrentYear))
                    {
                        svSimReg.smoothScrollTo(0, llRow[4].getTop() - offset);
                        animateErrorBounceWithCustomMessage(etNIRC, getString(R.string.snackbar_something_not_correct));
                    }
                    else if (isMale == null)
                    {
                        svSimReg.smoothScrollTo(0, llRow[5].getTop() - offset);
                        animateErrorBounce(rbMale);
                        animateErrorBounce(rbFemale);
                    }
                    else if (mDOB == null)
                    {
                        svSimReg.smoothScrollTo(0, llRow[6].getTop() - offset);
                        animateErrorBounceWithCustomMessage(btnDOB, "Looks like something went wrong scanning your document");
                    }
                    else if (isMalaysian && nirc != null && dob.compareTo(nirc) != 0)
                    {
                        svSimReg.smoothScrollTo(0, llRow[4].getTop() - offset);
                        animateErrorBounceWithCustomMessage(btnDOB, "Date of birth missmatch with NIRC");
                    }
                    else if (year > mCurrentYear - 12)
                    {
                        svSimReg.smoothScrollTo(0, llRow[4].getTop() - offset);
                        animateErrorBounceWithCustomMessage(btnDOB, "Age cannot younger than 12 years old.");
                    }
                    else if (etAlternatePhone.getText().toString().trim().length() > 0 && ! Common.IsMobileNumber(etAlternatePhone.getText().toString()))
                    {
                        svSimReg.smoothScrollTo(0, llRow[12].getTop() - offset);
                        animateErrorBounceWithCustomMessage(etAlternatePhone, getString(R.string.error_incorrect_mobile_number));
                    }
                    else if (etEMAIL.getText().toString().trim().length() > 0 && ! android.util.Patterns.EMAIL_ADDRESS.matcher(etEMAIL.getText()).matches())
                    {
                        svSimReg.smoothScrollTo(0, llRow[13].getTop() - offset);
                        animateErrorBounceWithCustomMessage(etEMAIL, getString(R.string.snackbar_something_not_correct));
                    }
                    else if (etAddress.length() == 0)
                    {
                        svSimReg.smoothScrollTo(0, llRow[7].getTop() - offset);
                        animateErrorBounce(etAddress);
                    }
                    else if (etAddress.getText().toString().contains("&"))
                    {
                        svSimReg.smoothScrollTo(0, llRow[8].getTop() - offset);
                        animateErrorBounceWithCustomMessage(etAddress, "Address contain invalid character: '&'");
                    }
                    else if (etPostcode.length() == 0)
                    {
                        svSimReg.smoothScrollTo(0, llRow[8].getTop() - offset);
                        animateErrorBounce(etPostcode);
                    }
                    else if (mCountry == null)
                    {
                        svSimReg.smoothScrollTo(0, llRow[9].getTop() - offset);
                        animateErrorBounce(btnCountry);
                    }
                    else if (! mCountry.equals("MALAYSIA") && etState.getText().toString().trim().isEmpty())
                    {
                        svSimReg.smoothScrollTo(0, llRow[10].getTop() - offset);
                        animateErrorBounce(etState);
                    }
                    else if (! mCountry.equals("MALAYSIA") && etCity.getText().toString().trim().isEmpty())
                    {
                        svSimReg.smoothScrollTo(0, llRow[11].getTop() - offset);
                        animateErrorBounce(etCity);
                    }
                    else if (mCountry.equals("MALAYSIA") && mState == null)
                    {
                        svSimReg.smoothScrollTo(0, llRow[10].getTop() - offset);
                        animateErrorBounceWithoutFocus(btnState);
                    }
                    else if (mCountry.equals("MALAYSIA") && mCity == null)
                    {
                        svSimReg.smoothScrollTo(0, llRow[11].getTop() - offset);
                        animateErrorBounceWithoutFocus(btnCity);
                    }
                    else if (etEMAIL.getText().length() > 0 && etEMAIL.getText().toString().contains("tunetalk.com") && ! etEMAIL.getText().toString().equalsIgnoreCase("apps@tunetalk.com"))
                    {
                        svSimReg.smoothScrollTo(0, llRow[13].getTop() - offset);
                        animateErrorBounceWithCustomMessage(etEMAIL, "Email ending with @tunetalk.com. Please use another email address.");
                    }
                    else
                    {
                        if (Common.hasConnectivity(getApplicationContext()))
                            registerSim();
                        else
                            addTaskToQueue();
                    }
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }
        });
    }

    @SuppressLint ("StaticFieldLeak")
    private void retrieveSimNumber (final String simNum)
    {
        if (Common.hasConnectivity(getApplicationContext()))
        {
            ApiProvider.retrieveSimNumber(this, simNum, new OnApiCallBack<RegisterSimNumberEntity>()
            {
                @Override public void onSuccess (RegisterSimNumberEntity response)
                {
                    mRegisterSimEntity = response;

                    if (mRegisterSimEntity.getResultCode())
                    {
                        etMobile.setText("");

                        if (Common.isValidString(mRegisterSimEntity.getSimNumber()))
                        {
                            isNumbered = true;
                            rbMSISDN.setChecked(true);
                            etMobile.setText("0" + mRegisterSimEntity.getSimNumber());
                            etMobile.setEnabled(false);
                            etMobile.setOnFocusChangeListener(null);
                            rbOrderId.setEnabled(false);

                        }
                        else
                        {
                            isNumbered = false;
                            etMobile.setEnabled(true);
                            rbOrderId.setEnabled(true);
                            etMobile.setOnFocusChangeListener(new View.OnFocusChangeListener()
                            {
                                @Override public void onFocusChange (View v, boolean hasFocus)
                                {
                                    if (isMSISDN)
                                    {
                                        if (hasFocus)
                                        {
                                            startActivityForResult(new Intent(mActivity, SimRegistrationMobileActivity.class), MOBILE_NUMBER_CODE);

                                            etFullName.requestFocus();
                                        }
                                    }
                                }
                            });
                        }
                    }
                    else
                    {
                        if (! mActivity.isFinishing())
                        {
                            new AlertDialog.Builder(mActivity)
                                .setMessage(mRegisterSimEntity.getMessage())
                                .setPositiveButton("OK", null)
                                .show();
                        }
                    }
                }

                @Override public void onFailure ()
                {
                }
            });
        }
    }

    @SuppressLint ("StaticFieldLeak") private void registerSim ()
    {
        final SIMRegistrationEntity sim = new SIMRegistrationEntity()
            .setLast8Digit(etLast8Digit.getText().toString().trim())
            .setBlockedNumber(isMSISDN ? (isNumbered ? null : etMobile.getText().toString().substring(1)) : null)
            .setFullName(etFullName.getText().toString().trim())
            .setDOB(mDOB)
            .setIsMale(isMale)
            .setIsMalaysian(isMalaysian)
            .setAddress(etAddress.getText().toString().trim())
            .setPostcode(etPostcode.getText().toString().trim())
            .setAlternatePhoneNumber(etAlternatePhone.getText().toString().trim())
            .setEMAIL(etEMAIL.getText().toString().trim())
            .setManual(false)
            .setWebOrderId(! isMSISDN ? etMobile.getText().toString().trim() : null)
            .setOKU(swOku.isChecked())
            .setStaffLoginId(DocumentIntentManager.init().getIntentEntity().getPartnerDealerCode());

        if (isMalaysian)
        {
            sim.setNIRC(etNIRC.getText().toString().trim())
                .setNationality(getString(R.string.common_text_malaysia));
        }
        else
        {
            sim.setNationality(mNationality)
                .setPassport(etPassport.getText().toString().trim());
        }

        if (mCountry.equals("MALAYSIA"))
        {
            sim.setCity(mCity)
                .setState(mState)
                .setCountry(mCountry);
        }
        else
        {
            sim.setState(etState.getText().toString().trim())
                .setCity(etCity.getText().toString().trim())
                .setCountry(mCountry);
        }

        sim.setImageOne(Base64.encodeBytes(mBuffer1));

        SimRegistrationManager.get().setSimRegistrationEntity(sim);
        Logger.json(new GsonBuilder().create().toJson(SimRegistrationManager.get().getSimRegistrationEntity()));

        if (swOku.isChecked())
        {
            okuRegistration(sim);
            return;
        }

        JSONObject jsonObject = ApiRequestBody.getSimRegRequestBody(this, sim);
        ApiProvider.simRegistration(this, jsonObject, new OnApiCallBack<UploadEntity>()
        {
            @Override public void onSuccess (UploadEntity response)
            {
                if (response.getResultCode())
                {
                    ActivityUtils.result(mActivity, Constant.ResultCode.SUCCESSFUL, null);
                }
                else
                {
                    if (Common.isValidString(response.getMessage()) && response.getCode().equals("-500"))
                    {
                        ActivityUtils.result(mActivity, Constant.ResultCode.FAILED, response.getMessage());
                    }
                }
            }

            @Override public void onFailure ()
            {

            }
        });

    }

    @SuppressLint ("StaticFieldLeak") private void addTaskToQueue ()
    {
        final SIMRegistrationEntity sim = new SIMRegistrationEntity()
            .setLast8Digit(etLast8Digit.getText().toString().trim())
            .setFullName(etFullName.getText().toString().trim())
            .setDOB(mDOB)
            .setIsMale(isMale)
            .setIsMalaysian(isMalaysian)
            .setAddress(etAddress.getText().toString().trim())
            .setPostcode(etPostcode.getText().toString().trim())
            .setAlternatePhoneNumber(etAlternatePhone.getText().toString().trim())
            .setEMAIL(etEMAIL.getText().toString().trim())
            .setManual(false)
            .setWebOrderId(! isMSISDN ? etMobile.getText().toString().trim() : null)
            .setOKU(swOku.isChecked())
            .setStaffLoginId(DocumentIntentManager.init().getIntentEntity().getPartnerDealerCode());

        if (Common.hasConnectivity(getApplicationContext()))
            sim.setBlockedNumber(isNumbered ? null : etMobile.getText().toString().substring(1));
        else
            sim.setBlockedNumber(null);

        if (isMalaysian)
        {
            sim.setNIRC(etNIRC.getText().toString().trim())
                .setNationality(getString(R.string.common_text_malaysia));
        }
        else
        {
            sim.setNationality(mNationality)
                .setPassport(etPassport.getText().toString().trim());
        }

        if (mCountry.equals("MALAYSIA"))
        {
            sim.setCity(mCity)
                .setState(mState)
                .setCountry(mCountry);
        }
        else
        {
            sim.setState(etState.getText().toString().trim())
                .setCity(etCity.getText().toString().trim())
                .setCountry(mCountry);
        }

        sim.setImageOne(Base64.encodeBytes(mBuffer1));

        if (swOku.isChecked())
        {
            okuRegistration(sim);
            return;
        }

        DatabaseManager.get().persistSimRegRecord(this, sim, new OnSqlResultListener()
        {
            @Override public void onSuccess (TaskEntity entity)
            {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_submit_reg_complete), Toast.LENGTH_LONG).show();
                finish();
            }

            @Override public void onFailure ()
            {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_unknown_error), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void okuRegistration (SIMRegistrationEntity sim)
    {
        Intent intent = new Intent(this, OkuRegistrationActivity.class);
        intent.putExtra(Constant.Key.Intent.SERVICE_CATEGORIES, ServiceCategories.SIM_REG_AUTO);
        intent.putExtra(Constant.Key.Intent.SIM_REGISTRATION_REQUEST_BODY, sim);
        startActivityForResult(intent, Constant.RequestCode.OKU_REQUEST_CODE);
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_rescan, menu);

        MenuItem item = menu.findItem(R.id.menu_rescan);
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener()
        {
            @Override
            public boolean onMenuItemClick (MenuItem item)
            {
                String[] options = new String[2];
                options[0] = getResources().getStringArray(R.array.orc_rescan)[getIntent().getIntExtra("Mode", 0)];
                options[1] = getResources().getStringArray(R.array.orc_rescan)[getResources().getStringArray(R.array.orc_rescan).length - 1];

                new AlertDialog.Builder(mActivity, R.style.DialogStyle)
                    .setTitle(R.string.ocr_select_document)
                    .setSingleChoiceItems(options, 100, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick (DialogInterface dialog, int which)
                        {
                            dialog.dismiss();
                            isActionFromMenu = true;
                            if (which == 0)
                            {
                                capture(getIntent().getIntExtra("Mode", 0));
                            }

                            if (which == 1)
                                capture(5);
                        }
                    })
                    .setNegativeButton("BACK", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick (DialogInterface dialog, int which)
                        {
                            dialog.dismiss();
                        }
                    }).show();
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void animateErrorBounceWithoutFocus (View v)
    {
        v.setTranslationX(75);
        v.animate()
            .translationX(0)
            .setInterpolator(new BounceInterpolator())
            .setDuration(500)
            .setStartDelay(300)
            .start();

        Snackbar snack = Snackbar.make(v, R.string.snackbar_empty_field, Snackbar.LENGTH_LONG);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(getResources().getColor(R.color.SnackErrorBackground));
        snack.show();
    }

    private void animateErrorBounceWithCustomMessage (View v, String msg)
    {
        v.setTranslationX(75);
        v.animate()
            .translationX(0)
            .setInterpolator(new BounceInterpolator())
            .setDuration(500)
            .setStartDelay(200)
            .start();

        Snackbar snack = Snackbar.make(v, msg, Snackbar.LENGTH_LONG);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(getResources().getColor(R.color.SnackErrorBackground));
        snack.show();
    }

    private void animateErrorBounce (View v)
    {
        v.setTranslationX(75);
        v.animate()
            .translationX(0)
            .setInterpolator(new BounceInterpolator())
            .setDuration(500)
            .setStartDelay(200)
            .start();

        Snackbar snack = Snackbar.make(v, R.string.snackbar_empty_field, Snackbar.LENGTH_LONG);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(getResources().getColor(R.color.SnackErrorBackground));
        snack.show();
    }

    private void ButtonListener ()
    {
        btnNationality.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                Intent i = new Intent(mActivity, ListViewActivity.class);
                i.putExtra("Type", 1);
                startActivityForResult(i, 1);
            }
        });

        btnCountry.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                Intent i = new Intent(mActivity, ListViewActivity.class);
                i.putExtra("Type", 4);
                startActivityForResult(i, 1);
            }
        });

        btnState.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                Intent i = new Intent(mActivity, ListViewActivity.class);
                i.putExtra("Type", 2);
                startActivityForResult(i, 2);
            }
        });

        btnCity.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                if (mState == null)
                {
                    animateErrorBounce(btnState);
                    new Handler().postDelayed(new Runnable()
                    {
                        @Override
                        public void run ()
                        {
                            btnState.performClick();
                        }
                    }, 1000);
                }
                else
                {
                    Intent i = new Intent(mActivity, ListViewActivity.class);
                    i.putExtra("Type", 3);
                    i.putExtra("State", mState);
                    startActivityForResult(i, 3);
                }
            }
        });

        btnDOB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                Dialog.Builder mDialogBuilder = new DatePickerDialog.Builder()
                {
                    @Override
                    public void onPositiveActionClicked (DialogFragment fragment)
                    {
                        DatePickerDialog dialog = (DatePickerDialog) fragment.getDialog();
                        dialog.dateRange(mCurrentDay, mCurrentMonth - 1, mCurrentYear - 99, mCurrentDay, mCurrentMonth - 1, mCurrentYear - 12);
                        mDOB = dialog.getFormattedDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH));
                        btnDOB.setText(mDOB);
                        super.onPositiveActionClicked(fragment);
                    }

                    @Override
                    public void onNegativeActionClicked (DialogFragment fragment)
                    {
                        super.onNegativeActionClicked(fragment);
                    }
                }.dateRange(mCurrentDay, mCurrentMonth - 1, mCurrentYear - 99, mCurrentDay, mCurrentMonth - 1, mCurrentYear - 12)
                    .date(mCurrentDay, mCurrentMonth - 1, mCurrentYear - 12);

                mDialogBuilder.positiveAction("CONFIRM").negativeAction("BACK");

                DialogFragment fragment = DialogFragment.newInstance(mDialogBuilder);
                fragment.show(getSupportFragmentManager(), null);
            }
        });
    }

    private void animateNonMalaysian (boolean isChecked)
    {
        final ValueAnimator vaNIRC, vaPassport;

        if (isChecked)
        {
            btnNationality.setVisibility(View.VISIBLE);
            vaNIRC = ValueAnimator.ofFloat(10f, 0f);
            vaNIRC.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
            {
                @Override
                public void onAnimationUpdate (ValueAnimator valueAnimator)
                {
                    etNIRC.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, (float) valueAnimator.getAnimatedValue()));
                }
            });

            vaPassport = ValueAnimator.ofFloat(0f, 10f);
            vaPassport.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
            {
                @Override
                public void onAnimationUpdate (ValueAnimator valueAnimator)
                {
                    etPassport.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, (float) valueAnimator.getAnimatedValue()));
                }
            });
        }
        else
        {
            vaNIRC = ValueAnimator.ofFloat(0f, 10f);
            vaNIRC.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
            {
                @Override
                public void onAnimationUpdate (ValueAnimator valueAnimator)
                {
                    etNIRC.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, (float) valueAnimator.getAnimatedValue()));
                }
            });

            vaPassport = ValueAnimator.ofFloat(10f, 0f);
            vaPassport.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
            {
                @Override
                public void onAnimationUpdate (ValueAnimator valueAnimator)
                {
                    etPassport.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, (float) valueAnimator.getAnimatedValue()));
                }
            });
        }

        vaNIRC.setDuration(500);
        vaPassport.setDuration(500);
        vaNIRC.setInterpolator(new DecelerateInterpolator());
        vaPassport.setInterpolator(new DecelerateInterpolator());

        if (isChecked)
        {
            btnNationality.setScaleY(1.3f);
            btnNationality.setScaleX(1.3f);
            btnNationality.setAlpha(0);
            btnNationality.animate()
                .setInterpolator(new BounceInterpolator())
                .scaleX(1f)
                .scaleY(1f)
                .alpha(1)
                .setDuration(600)
                .setStartDelay(200)
                .setListener(new Animator.AnimatorListener()
                {
                    @Override
                    public void onAnimationStart (Animator animator)
                    {
                        etPassport.setText(null);
                    }

                    @Override
                    public void onAnimationEnd (Animator animator)
                    {
                        vaNIRC.start();
                        vaPassport.start();
                        etNIRC.setText(null);
                        btnNationality.animate().setListener(null);
                    }

                    @Override
                    public void onAnimationCancel (Animator animator)
                    {

                    }

                    @Override
                    public void onAnimationRepeat (Animator animator)
                    {

                    }
                })
                .start();
        }
        else
        {
            btnNationality.animate()
                .setInterpolator(new DecelerateInterpolator())
                .scaleX(1.3f)
                .scaleY(1.3f)
                .alpha(0)
                .setDuration(400)
                .setListener(new Animator.AnimatorListener()
                {
                    @Override
                    public void onAnimationStart (Animator animator)
                    {
                        etNIRC.setText(null);
                    }

                    @Override
                    public void onAnimationEnd (Animator animator)
                    {
                        vaNIRC.start();
                        vaPassport.start();
                        btnNationality.setText(getResources().getString(R.string.simreg_text_pick_nationality));
                        mNationality = null;
                        etPassport.setText(null);
                        btnNationality.animate().setListener(null);
                        btnNationality.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onAnimationCancel (Animator animator)
                    {

                    }

                    @Override
                    public void onAnimationRepeat (Animator animator)
                    {

                    }
                })
                .start();
        }
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (mTimer != null)
        {
            mTimer.purge();
            mTimer.cancel();
        }

        if (requestCode == 100 && resultCode == RESULT_OK)
            finish();

        if(requestCode == Constant.RequestCode.OKU_REQUEST_CODE)
        {
            switch (resultCode)
            {
                case Constant.ResultCode.SUCCESSFUL:
                case Constant.ResultCode.FAILED:
                case Constant.ResultCode.OFFLINE_SERVICE:
                    setResult(resultCode, data);
                    finish();
                    break;
                default:
                    break;
            }
            return;
        }

        if (resultCode == RESULT_OK)
        {
            switch (data.getExtras().getInt("Type"))
            {
                case 1:
                    mNationality = data.getStringExtra("SelectedItem");
                    btnNationality.setText(mNationality);
                    break;
                case 2:
                    mState = data.getStringExtra("SelectedItem");
                    btnState.setText(mState);
                    btnCity.setText(getResources().getString(R.string.simreg_text_pick_city));
                    mCity = null;
                    break;
                case 3:
                    mCity = data.getStringExtra("SelectedItem");
                    btnCity.setText(mCity);
                    break;
                case 4:
                    mCountry = data.getStringExtra("SelectedItem");
                    btnCountry.setText(mCountry);
                    if (mCountry.equals("MALAYSIA"))
                    {
                        etPostcode.setInputType(InputType.TYPE_CLASS_NUMBER);
                        btnState.setVisibility(View.VISIBLE);
                        etState.setVisibility(View.GONE);
                        btnCity.setVisibility(View.VISIBLE);
                        etCity.setVisibility(View.GONE);
                    }
                    else
                    {
                        etPostcode.setInputType(InputType.TYPE_CLASS_TEXT);
                        btnState.setVisibility(View.GONE);
                        etState.setVisibility(View.VISIBLE);
                        btnCity.setVisibility(View.GONE);
                        etCity.setVisibility(View.VISIBLE);
                    }
                    btnState.setText(getString(R.string.simreg_text_pick_state));
                    btnCity.setText(getString(R.string.simreg_text_pick_city));
                    etState.setText(null);
                    etCity.setText(null);
                    mState = null;
                    mCity = null;
                    break;
            }

            if (data != null)
            {
                Vibrator v = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(200);

                try
                {
                    mRecognizerBundle.loadFromIntent(data);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                if (requestCode == OCR_REQUEST_CODE)
                {
                    OCRHelper.get().getResult(mRecognizerBundle, new OnScanResult()
                    {
                        @Override public void onScanResult (ScanResultEntity entity)
                        {
                            try
                            {
                                if (entity.getMyKad() != null)
                                {
                                    ScanResultItemEntity myKad = entity.getMyKad();
                                    etFullName.setText(myKad.getFullName());
                                    etNIRC.setText(myKad.getNric());

                                    etPostcode.setText(myKad.getPoscode());
                                    etAddress.setText(myKad.getAddress());
                                }
                                else if (entity.getMrtd() != null)
                                {
                                    ScanResultItemEntity mrtd = entity.getMrtd();
                                    etFullName.setText(mrtd.getFullName());
                                    etPassport.setText(mrtd.getPassportNumber());

                                    mNationality = mrtd.getNationality();

                                    if (Common.isValidString(mNationality))
                                        btnNationality.setText(mNationality);
                                    else
                                        btnNationality.setText(getResources().getString(R.string.simreg_text_pick_nationality));

                                    if (mrtd.getGender().equals("M"))
                                        rbMale.setChecked(true);
                                    else
                                        rbFemale.setChecked(true);

                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                                    mDOB = sdf.format(new SimpleDateFormat("yyMMdd", Locale.getDefault()).parse(mrtd.getDob()));
                                    btnDOB.setText(mDOB);
                                }
                                else if (entity.getiKad() != null)
                                {
                                    ScanResultItemEntity ikad = entity.getiKad();
                                    etFullName.setText(ikad.getFullName());
                                    etPassport.setText(ikad.getPassportNumber());

                                    etPostcode.setText(ikad.getPoscode());
                                    etAddress.setText(ikad.getAddress());

                                    mNationality = ikad.getNationality();
                                    if (Common.isValidString(mNationality))
                                        btnNationality.setText(mNationality);
                                    else
                                        btnNationality.setText(getResources().getString(R.string.simreg_text_pick_nationality));

                                    if (ikad.getGender().equals("M"))
                                        rbMale.setChecked(true);
                                    else
                                        rbFemale.setChecked(true);

                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                                    Date dob = sdf.parse(ikad.getDob());
                                    mDOB = sdf.format(dob);
                                    btnDOB.setText(mDOB);
                                }
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }

                            if (etNIRC.getText() != null && etNIRC.getText().toString().length() > 0)
                                etNIRC.setEnabled(false);
                            else
                                etNIRC.setEnabled(true);

                            if (etFullName.getText() != null && etFullName.getText().toString().length() > 0)
                                etFullName.setEnabled(false);
                            else
                                etFullName.setEnabled(true);

                            if (etPassport.getText() != null && etPassport.getText().toString().length() > 0)
                                etPassport.setEnabled(false);
                            else
                                etPassport.setEnabled(true);
                        }

                        @Override public void onImageResult (byte[] imageBytes)
                        {
                            mBuffer1 = imageBytes;

                            if (mBuffer1 == null || mBuffer1.length == 0)
                            {
                                new AlertDialog.Builder(mActivity)
                                    .setTitle("Opps...")
                                    .setMessage("Unable to read NRIC/iKad/Passport photo, please try again.")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener()
                                    {
                                        @Override public void onClick (DialogInterface dialog, int which)
                                        {
                                            finish();
                                        }
                                    }).show();
                            }
                            else
                            {
                                FileUtils.ClearOCRFolder();
                                mBuffer1 = FileUtils.AddWatermark(mActivity, mBuffer1);

                                Glide.with(mActivity)
                                    .load(mBuffer1)
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .signature(new StringSignature(System.currentTimeMillis() + ""))
                                    .into(ivPhoto1);

                                if (isActionFromMenu)
                                    isActionFromMenu = false;
                                else
                                    capture(5);
                            }
                        }
                    });

                }
                else if (requestCode == OCR_BARCODE_REQUEST_CODE)
                {
                    isActionFromMenu = false;

                    OCRHelper.get().getResult(mRecognizerBundle, new OnScanResult()
                    {
                        @Override public void onScanResult (ScanResultEntity entity)
                        {
                            try
                            {
                                if (entity.getBarcode() != null)
                                {
                                    CheckNetworkState();
                                    mReferenceSimNumber = entity.getBarcode().getScannedBarCode().replaceAll("60199900", "");
                                    etLast8Digit.setText(mReferenceSimNumber);
                                    retrieveSimNumber(getString(R.string.simreg_text_first_8_digit) + mReferenceSimNumber);
                                }
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }

                        @Override public void onImageResult (byte[] imageBytes)
                        {

                        }
                    });
                }
                else if (requestCode == MOBILE_NUMBER_CODE)
                {
                    if (data.getStringExtra("MOBILE") != null)
                        etMobile.setText(data.getStringExtra("MOBILE"));
                }
            }
        }
        else
        {
            if (requestCode != OCR_BARCODE_REQUEST_CODE)
            {
                if (! isActionFromMenu)
                {
                    if (requestCode == OCR_BARCODE_REQUEST_CODE || requestCode == OCR_REQUEST_CODE)
                        finish();
                }
            }
        }
    }

    public void capture (int type)
    {

        String message = type == 0 ? "Scan a NRIC"
            : type == 1 ? "Scan a Passport"
            : type == 2 ? "Scan a iKad"
            : type == 3 ? "Scan a Work Permit"
            : type == 4 ? "Scan a Visa"
            : "Scan a SIM Card barcode";
//        FrenchToast(message);

        mStyleableToast = StyleableToast.makeText(getApplicationContext(), message, R.style.StyleToast);

        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask()
        {
            @Override public void run ()
            {
                SIMRegistrationAutoActivity.this.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run ()
                    {
                        mStyleableToast.show();
                    }
                });
            }
        }, 0, 3500);

        isActivityExist = true;

        FileUtils.MakeOCRFolder();

        mRecognizerBundle = OCRHelper.get().buildMyKadElement();

        if (type == 1 || type == 3 || type == 4)
            mRecognizerBundle = OCRHelper.get().buildMrtdElement();
        else if (type == 2)
            mRecognizerBundle = OCRHelper.get().buildIKadElement();
        else if (type == 5)
            mRecognizerBundle = OCRHelper.get().buildBarCodeElement();

        DocumentUISettings settings = new DocumentUISettings(mRecognizerBundle);
        OCRHelper.get().scan(this, settings, type == 5 ? OCR_BARCODE_REQUEST_CODE : OCR_REQUEST_CODE);
    }

    @Override public void onBackPressed ()
    {
        setResult(Constant.ResultCode.USER_CANCELLED);
        super.onBackPressed();
    }

    @Override protected void onDestroy ()
    {
        SimRegistrationManager.reset();

        if (mTimer != null)
        {
            mTimer.purge();
            mTimer.cancel();
        }

        super.onDestroy();
    }
}
