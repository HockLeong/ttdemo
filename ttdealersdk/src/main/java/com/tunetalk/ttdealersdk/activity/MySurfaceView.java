package com.tunetalk.ttdealersdk.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tunetalk.ttdealersdk.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

public class MySurfaceView extends Activity implements SurfaceHolder.Callback
{
    public static final int JPEG_QUALITY = 75;
    public static final int PICTURE_HEIGHT = 400, PICTURE_WIDTH = 600;
    final float SIMCARD = 0.62f, IC = 0.62f, PASSPORT = 0.67f;
    float CROP_RATIO;
    int mCaptureType, mPosition;
    Boolean isCaptured = false, isPreviewRatioEqualPictureRatio = false;
    Activity mActivity;
    Button btnCapture, btnDiscard, btnDone;
    TextView tvCaptureType, tvInstruction;
    ImageView ivCaptured;
    RelativeLayout mRelativeLayout;
    DrawRectangle mRectangle;
    Bitmap mCroppedImage = null, mWaterMarkImage = null;

    SurfaceView mSurfaceView;
    SurfaceHolder mSurfaceHolder;
    Camera mCamera;
    PictureCallback mJPEGPictureCallback;
    Camera.ShutterCallback mShutterCallback;
    Camera.AutoFocusCallback mAutoFocusCallback;

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_surface);
        mActivity = this;
        tvCaptureType = (TextView) findViewById(R.id.tvCaptureType);
        tvInstruction = (TextView) findViewById(R.id.tvInstruction);
        ivCaptured = (ImageView) findViewById(R.id.ivCaptured);
        mSurfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        mRelativeLayout = (RelativeLayout) findViewById(R.id.container);
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(this);

        mCaptureType = getIntent().getExtras().getInt("Type");
        mPosition = getIntent().getExtras().getInt("Position");
        switch (mCaptureType)
        {
            case 0:
                CROP_RATIO = SIMCARD;
                tvCaptureType.setText("Capture SIM Card");
                tvInstruction.setText("Place SIM card inside red rectangle");
                break;
            case 1:
                CROP_RATIO = IC;
                tvCaptureType.setText("Capture NRIC");
                tvInstruction.setText("Place NRIC inside red rectangle");
                break;
            case 2:
                CROP_RATIO = PASSPORT;
                tvCaptureType.setText("Capture Passport");
                tvInstruction.setText("Place passport inside red rectangle");
                break;
            case 3:
                CROP_RATIO = IC;
                tvCaptureType.setText("Capture OKU Card");
                tvInstruction.setText("Place OKU card inside red rectangle");
                break;
            case 4:
                CROP_RATIO = IC;
                tvCaptureType.setText("Capture OKU National ID");
                tvInstruction.setText("Place OKU National ID inside red rectangle");
                break;
            default:
                CROP_RATIO = SIMCARD;
                tvCaptureType.setText("Capture SIM Card");
                tvInstruction.setText("Place SIM card inside red rectangle");
                break;
        }

        mShutterCallback = new Camera.ShutterCallback()
        {
            @Override
            public void onShutter ()
            {

            }
        };

        btnCapture = (Button) findViewById(R.id.btnCapture);
        btnCapture.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                btnCapture.setEnabled(false);
                mCamera.takePicture(mShutterCallback, null, mJPEGPictureCallback);
            }
        });

        btnDiscard = (Button) findViewById(R.id.btnDiscard);
        btnDiscard.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                isCaptured = false;
                swapButton();
                refreshCamera();
            }
        });

        actionButtonDone(); //Long async code goes here
        actionJPEGCallBack(); //Long async code goes here
    }

    @SuppressLint ("StaticFieldLeak")
    private void actionButtonDone ()
    {
        btnDone = (Button) findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                new AsyncTask<Void, Void, Void>()
                {
                    ProgressDialog pd;
                    Intent i;

                    @Override
                    protected void onPreExecute ()
                    {
                        super.onPreExecute();
                        pd = ProgressDialog.show(mActivity, "Nearly done", "Seasoning image...", false);
                        i = getIntent();
                    }

                    @Override
                    protected void onPostExecute (Void aVoid)
                    {
                        super.onPostExecute(aVoid);
                        new Handler().postDelayed(new Runnable()
                        {
                            @Override
                            public void run ()
                            {
                                pd.dismiss();
                                setResult(RESULT_OK, i);
                                finish();
                                mCroppedImage.recycle();
                                mWaterMarkImage.recycle();
                            }
                        }, 1000);
                    }

                    @Override
                    protected Void doInBackground (Void... voids)
                    {
                        try
                        {
                            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                            Bitmap watermark = BitmapFactory.decodeStream(getAssets().open("watermark.png"));
                            watermark = Bitmap.createScaledBitmap(watermark, Math.round(mCroppedImage.getWidth()), Math.round(mCroppedImage.getHeight()), true);

                            mWaterMarkImage = Bitmap.createBitmap(mCroppedImage.getWidth(), mCroppedImage.getHeight(),
                                mCroppedImage.getConfig() != null ? mCroppedImage.getConfig() : Bitmap.Config.ARGB_8888);

                            Canvas canvas = new Canvas(mWaterMarkImage);
                            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                            canvas.drawBitmap(mCroppedImage, 0, 0, paint);
                            canvas.drawBitmap(watermark,
                                (float) (mCroppedImage.getWidth() / 2) - (watermark.getWidth() / 2),
                                (float) (mCroppedImage.getHeight() / 2) - (watermark.getHeight() / 2), paint);

                            mWaterMarkImage.compress(Bitmap.CompressFormat.JPEG, JPEG_QUALITY, outputStream);
                            i.putExtra("Type", mCaptureType);
                            i.putExtra("Position", mPosition);
                            i.putExtra("ImageBuffer", outputStream.toByteArray());

                            watermark.recycle();
                        }
                        catch (IOException io)
                        {
                            io.printStackTrace();
                        }
                        return null;
                    }
                }.execute();
            }
        });
    }

    private void actionJPEGCallBack ()
    {
        mJPEGPictureCallback = new PictureCallback()
        {
            @Override
            public void onPictureTaken (byte[] bytes, Camera camera)
            {
                isCaptured = true;
                mCamera.stopPreview();
                final byte[] imgBytes = bytes;

                new AsyncTask<Void, Void, Void>()
                {
                    ProgressDialog pd;

                    @Override
                    protected void onPreExecute ()
                    {
                        super.onPreExecute();
                        pd = ProgressDialog.show(mActivity, "One second please", "Processing image...", false);
                    }

                    @Override
                    protected void onPostExecute (Void aVoid)
                    {
                        super.onPostExecute(aVoid);
                        new Handler().postDelayed(new Runnable()
                        {
                            @Override
                            public void run ()
                            {
                                pd.dismiss();
                                swapButton();
                            }
                        }, 1000);
                    }

                    @Override
                    protected Void doInBackground (Void... voids)
                    {
                        Bitmap originalImg = BitmapFactory.decodeByteArray(imgBytes, 0, imgBytes.length);

                        if (originalImg.getWidth() > originalImg.getHeight()) //Convert Lanscape to portail
                        {
                            Matrix matrix = new Matrix();
                            matrix.setRotate(90f);
                            originalImg = Bitmap.createBitmap(originalImg, 0, 0, originalImg.getWidth(), originalImg.getHeight(), matrix, true);
                        }

                        if (isPreviewRatioEqualPictureRatio)
                        {
                            int width = originalImg.getWidth();
                            int height = originalImg.getHeight();

                            int x = Math.round(mRectangle.GAP);
                            int cropWidth = width - (x * 2);

                            int cropHeight = Math.round(cropWidth * CROP_RATIO + mRectangle.STROKE_WIDTH * 2 + mRectangle.EXTRA_HEIGHT / mRectangle.SCALE / 4);
                            int y = Math.round((height / 2) - (cropHeight / 2) + mRectangle.STROKE_WIDTH + mRectangle.EXTRA_HEIGHT / mRectangle.SCALE);

                            mCroppedImage = Bitmap.createBitmap(originalImg, x, y, cropWidth, cropHeight);
                            mCroppedImage = Bitmap.createScaledBitmap(mCroppedImage, PICTURE_WIDTH, PICTURE_HEIGHT, true);

                            originalImg.recycle();
                        }
                        else
                        {
                            int clusterWidth = originalImg.getWidth() / 10;
                            int clusterHeight = originalImg.getHeight() / 10;
                            mCroppedImage = Bitmap.createBitmap(originalImg, clusterWidth * 2, clusterHeight * 2, clusterWidth * 6, clusterHeight * 6);
                        }

                        return null;
                    }
                }.execute();
            }
        };
    }

    private void swapButton ()
    {
        if (isCaptured)
        {
            btnCapture.setVisibility(View.INVISIBLE);
            btnDone.setVisibility(View.VISIBLE);
            btnDiscard.setVisibility(View.VISIBLE);
            ivCaptured.setImageBitmap(mCroppedImage);
            ivCaptured.setVisibility(View.VISIBLE);
            mSurfaceView.setVisibility(View.INVISIBLE);
            mRectangle.setVisibility(View.INVISIBLE);
        }
        else
        {
            isCaptured = false;
            btnCapture.setEnabled(true);
            ivCaptured.setImageBitmap(null);
            btnCapture.setVisibility(View.VISIBLE);
            btnDone.setVisibility(View.INVISIBLE);
            btnDiscard.setVisibility(View.INVISIBLE);
            ivCaptured.setVisibility(View.INVISIBLE);
            mSurfaceView.setVisibility(View.VISIBLE);
            mRectangle.setVisibility(View.VISIBLE);
        }
    }

    private void focusOnTouch (MotionEvent event)
    {
        if (mCamera != null)
        {
            Camera.Parameters parameters = mCamera.getParameters();
            if (parameters.getMaxNumMeteringAreas() > 0)
            {
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                mCamera.setParameters(parameters);
                mCamera.autoFocus(mAutoFocusCallback);
            }
            else
            {
                mCamera.autoFocus(mAutoFocusCallback);
            }
        }
    }

    @Override
    public void surfaceCreated (SurfaceHolder surfaceHolder)
    {
        mCamera = Camera.open();
        Camera.Parameters param = mCamera.getParameters();
        Camera.Size previewSize = getOptimalPreviewSize(param.getSupportedPreviewSizes());
        Camera.Size pictureSize = getOptimalPictureSize(param.getSupportedPictureSizes(), previewSize);

        param.setPictureSize(pictureSize.width, pictureSize.height);
        param.setPreviewSize(previewSize.width, previewSize.height);
        param.setJpegQuality(JPEG_QUALITY);

        mSurfaceHolder.setKeepScreenOn(true);
        mRectangle = new DrawRectangle(this, previewSize.height, previewSize.width);  //Flip height, width

        double screenRatio = Double.parseDouble(String.format("%.2f", ((double) mRectangle.SCREEN_HEIGHT / mRectangle.SCREEN_WIDTH)));
        double previewRatio = Double.parseDouble(String.format("%.2f", ((double) mRectangle.PREVIEW_HEIGHT / mRectangle.PREVIEW_WIDTH)));

        if (Math.abs(screenRatio - previewRatio) > 0.1)
            mSurfaceHolder.setFixedSize(mRectangle.SCREEN_WIDTH, (int) Math.round(mRectangle.SCREEN_WIDTH * previewRatio));

        mRelativeLayout.addView(mRectangle);

        if (param.getSupportedFocusModes() != null)
        {
            if (param.getFocusMode().contains(Camera.Parameters.FOCUS_MODE_AUTO))
                param.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);

            if (param.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
                param.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }

        if (param.getSupportedFlashModes() != null)
            if (param.getSupportedFlashModes().contains(Camera.Parameters.FLASH_MODE_AUTO))
                param.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);

        try
        {
            mCamera.setParameters(param);
            mCamera.setPreviewDisplay(mSurfaceHolder);
            mCamera.setDisplayOrientation(90);
            mCamera.startPreview();
            mSurfaceView.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch (View v, MotionEvent event)
                {
                    if (event.getAction() == MotionEvent.ACTION_DOWN)
                    {
                        focusOnTouch(event);
                    }
                    return true;
                }
            });
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }

    public void refreshCamera ()
    {
        if (mSurfaceHolder.getSurface() == null)
            return;

        try
        {
            mCamera.stopPreview();
            mCamera.setPreviewDisplay(mSurfaceHolder);
            mCamera.startPreview();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged (SurfaceHolder surfaceHolder, int i, int i1, int i2)
    {
        //refreshCamera();
    }

    @Override
    public void surfaceDestroyed (SurfaceHolder surfaceHolder)
    {
        mCamera.stopPreview();
        mCamera.release();
        mCamera = null;
    }

    private Camera.Size getOptimalPreviewSize (List<Camera.Size> sizeList)
    {
        Camera.Size optimalSize = null;

        if (sizeList.get(0).width > sizeList.get(sizeList.size() - 1).width)
        {
            for (int i = 0; i < sizeList.size(); ++ i)
                if (optimalSize == null && sizeList.get(i).width <= 2048 && sizeList.get(i).width >= 1024)
                    optimalSize = sizeList.get(i);
        }
        else //ASC order
        {
            for (int i = sizeList.size() - 1; i >= 0; -- i)
                if (optimalSize == null && sizeList.get(i).width <= 2048 && sizeList.get(i).width >= 1024)
                    optimalSize = sizeList.get(i);
        }

        if (optimalSize == null) //Get biggest, but unsupported ratio
        {
            if (sizeList.get(0).width > sizeList.get(sizeList.size() - 1).width)
                optimalSize = sizeList.get(0);
            else
                optimalSize = sizeList.get(sizeList.size() - 1);
        }
        return optimalSize;
    }

    private Camera.Size getOptimalPictureSize (List<Camera.Size> sizeList, Camera.Size prevSize)
    {
        Camera.Size optimalSize = null;
        String prevRatio = String.format("%.2f", ((double) prevSize.width / prevSize.height));

        if (sizeList.get(0).width > sizeList.get(sizeList.size() - 1).width)
        {
            for (int i = 0; i < sizeList.size(); ++ i)
            {
                String picRatio = String.format("%.2f", ((double) sizeList.get(i).width / sizeList.get(i).height));
                if (picRatio.equals(prevRatio) && optimalSize == null && sizeList.get(i).width <= 2048 && sizeList.get(i).width >= 1024)
                {
                    optimalSize = sizeList.get(i);
                    isPreviewRatioEqualPictureRatio = true;
                }
            }
        }
        else //DESC order
        {
            for (int i = sizeList.size() - 1; i >= 0; -- i)
            {
                String picRatio = String.format("%.2f", ((double) sizeList.get(i).width / sizeList.get(i).height));

                if (picRatio.equals(prevRatio) && optimalSize == null && sizeList.get(i).width <= 2048 && sizeList.get(i).width >= 1024)
                {
                    optimalSize = sizeList.get(i);
                    isPreviewRatioEqualPictureRatio = true;
                }
            }
        }

        if (optimalSize == null) //Get biggest, but unsupported ratio
        {
            if (sizeList.get(0).width > sizeList.get(sizeList.size() - 1).width)
                optimalSize = sizeList.get(0);
            else
                optimalSize = sizeList.get(sizeList.size() - 1);
        }
        return optimalSize;
    }

    //Note: FIX tablet may have extra height
    public class DrawRectangle extends View
    {
        private float RATIO;
        public float SCALE, GAP = 25, EXTRA_HEIGHT = 5;

        public float REC_TOP, REC_BOTTOM, REC_LEFT, REC_RIGHT;
        public int STROKE_WIDTH = 4, STROKE_COLOR, PREVIEW_HEIGHT, PREVIEW_WIDTH, SCREEN_HEIGHT, SCREEN_WIDTH;

        Paint mPaint = new Paint();
        Context mContext;

        public DrawRectangle (Context context, int previewWidth, int previewHeight)
        {
            super(context);
            this.PREVIEW_WIDTH = previewWidth;
            this.PREVIEW_HEIGHT = previewHeight;
            mContext = context;

            RATIO = CROP_RATIO;
            SCREEN_HEIGHT = getResources().getDisplayMetrics().heightPixels;
            SCREEN_WIDTH = getResources().getDisplayMetrics().widthPixels;
            SCALE = getResources().getDisplayMetrics().density;

            EXTRA_HEIGHT = EXTRA_HEIGHT * SCALE * SCALE;
            STROKE_WIDTH = Math.round(STROKE_WIDTH * SCALE);
            GAP = Math.round(GAP * SCALE);
            STROKE_COLOR = Color.RED;
        }

        @Override
        public void onDraw (Canvas canvas)
        {
            mPaint.setColor(STROKE_COLOR);
            mPaint.setAntiAlias(true);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeWidth(STROKE_WIDTH);

            REC_LEFT = STROKE_WIDTH / 2 + GAP;
            REC_RIGHT = SCREEN_WIDTH - (STROKE_WIDTH / 2) - GAP;

            REC_TOP = (SCREEN_HEIGHT / 2) - (REC_RIGHT * RATIO / 2) + (EXTRA_HEIGHT / 2);
            REC_BOTTOM = (SCREEN_HEIGHT / 2) + (REC_RIGHT * RATIO / 2) - (EXTRA_HEIGHT / 2);

            canvas.drawRect(REC_LEFT, REC_TOP, REC_RIGHT, REC_BOTTOM, mPaint);
        }
    }
}