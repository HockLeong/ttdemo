package com.tunetalk.ttdealersdk.activity;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.microblink.entities.recognizers.RecognizerBundle;
import com.microblink.uisettings.DocumentUISettings;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.rey.material.widget.Button;
import com.rey.material.widget.CheckBox;
import com.rey.material.widget.EditText;
import com.tunetalk.ttdealersdk.R;
import com.tunetalk.ttdealersdk.api.ApiProvider;
import com.tunetalk.ttdealersdk.api.ApiRequestBody;
import com.tunetalk.ttdealersdk.api.OnApiCallBack;
import com.tunetalk.ttdealersdk.base.BaseActivity;
import com.tunetalk.ttdealersdk.entity.request.SIMReplacementEntity;
import com.tunetalk.ttdealersdk.entity.response.UploadEntity;
import com.tunetalk.ttdealersdk.entity.response.sql.TaskEntity;
import com.tunetalk.ttdealersdk.ocr.DocumentType;
import com.tunetalk.ttdealersdk.ocr.OCRHelper;
import com.tunetalk.ttdealersdk.ocr.OnScanResult;
import com.tunetalk.ttdealersdk.ocr.entity.ScanResultEntity;
import com.tunetalk.ttdealersdk.singleton.DocumentIntentManager;
import com.tunetalk.ttdealersdk.singleton.DatabaseManager;
import com.tunetalk.ttdealersdk.sql.OnSqlResultListener;
import com.tunetalk.ttdealersdk.util.ActivityUtils;
import com.tunetalk.ttdealersdk.util.Base64;
import com.tunetalk.ttdealersdk.util.Common;
import com.tunetalk.ttdealersdk.util.Constant;
import com.tunetalk.ttdealersdk.util.FileUtils;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class SIMReplacementAutoActivity extends BaseActivity
{
    private final int OCR_REQUEST_CODE = 95;
    private final int OCR_BARCODE_REQUEST_CODE = 96;

    ScrollView svSimReg;
    CheckBox chkIsMalaysian;
    Button btnNext;
    EditText etLast8Digit, etFullName;
    EditText etNIRC, etPassport, etMobileNumber, etEMAIL;
    LinearLayout[] llRow;
    ImageView ivPhoto1;
    byte[] mBuffer1;

    boolean isMalaysian = true;
    boolean isActionFromMenu = false;
    StyleableToast mStyleableToast;
    Timer mTimer;

    boolean isActivityExist = true;
    RecognizerBundle mRecognizerBundle;

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sim_auto_replacement);
        setToolbarTitle(R.string.activity_sim_replace);

        findView(); //Included declare some listener here
        btnNextListener(); //EditText condition checking

        getCaptureMode(DocumentIntentManager.init().getIntentEntity().getDocumentType());

        chkIsMalaysian.setEnabled(false);
    }

    private void getCaptureMode (DocumentType mode)
    {
        switch (mode)
        {
            case MYKAD:
                capture(0);
                break;
            case MRTD:
                chkIsMalaysian.setChecked(true);
                capture(1);
                break;
            case IKAD:
                chkIsMalaysian.setChecked(true);
                capture(2);
                break;
            case WORK_PERMIT:
                chkIsMalaysian.setChecked(true);
                capture(3);
                break;
            case VISA:
                chkIsMalaysian.setChecked(true);
                capture(4);
                break;
            case BARCODE:
                capture(5);
            default:
                break;
        }
    }

    private void findView ()
    {
        //CAUTION: FIXED ARRAY SIZE
        //TODO: Update LinearLayout value/size/whatever according to Layout
        llRow = new LinearLayout[8];
        llRow[0] = (LinearLayout) findViewById(R.id.replace_ll_row1);
        llRow[1] = (LinearLayout) findViewById(R.id.replace_ll_row2);
//        llRow[2] = (LinearLayout) findViewById(R.id.replace_ll_row3);
        llRow[3] = (LinearLayout) findViewById(R.id.replace_ll_row4);
        llRow[4] = (LinearLayout) findViewById(R.id.replace_ll_row5);
        llRow[5] = (LinearLayout) findViewById(R.id.replace_ll_row6);
        llRow[6] = (LinearLayout) findViewById(R.id.replace_ll_row8);

        svSimReg = (ScrollView) findViewById(R.id.replace_scrollview);
        btnNext = (Button) findViewById(R.id.replace_btnNext);
        chkIsMalaysian = (CheckBox) findViewById(R.id.replace_chkIsMalaysian);

        etLast8Digit = (EditText) findViewById(R.id.replace_etLast8Digit);
        etFullName = (EditText) findViewById(R.id.replace_etFullName);
        etEMAIL = (EditText) findViewById(R.id.replace_etEmail);
        etNIRC = (EditText) findViewById(R.id.replace_etNIRC);
        etPassport = (EditText) findViewById(R.id.replace_etPassport);
        etMobileNumber = (EditText) findViewById(R.id.replace_etPhoneNumber);
        ivPhoto1 = (ImageView) findViewById(R.id.ivPhoto1);


        etPassport.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        etPassport.setEnabled(false);

        etLast8Digit.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange (View view, boolean focused)
            {
                if (focused)
                {
                    etLast8Digit.setHint(getResources().getString(R.string.simreg_text_last_8digit));
                    etLast8Digit.setTextColor(getResources().getColor(R.color.TextPrimaryDark));
                }
                else if (! focused && etLast8Digit.length() != 8 && etLast8Digit.length() != 0)
                {
                    etLast8Digit.setHint(getResources().getString(R.string.error_simreg_last8digit));
                    etLast8Digit.setTextColor(getResources().getColor(R.color.TextError));
                    animateErrorBounceWithCustomMessage(etNIRC, getResources().getString(R.string.snackbar_wrong_sim_num));
                }
                else
                {
                    etLast8Digit.setHint(getResources().getString(R.string.simreg_text_last_8digit));
                    etLast8Digit.setTextColor(getResources().getColor(R.color.TextPrimaryDark));
                }
            }
        });

        etNIRC.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange (View view, boolean focused)
            {
                if (focused)
                {
                    etNIRC.setHint(getResources().getString(R.string.common_text_NIRC));
                    etNIRC.setTextColor(getResources().getColor(R.color.TextPrimaryDark));
                }
                else if (! focused && etNIRC.length() != 12 && etNIRC.length() != 0)
                {
                    etNIRC.setHint(getResources().getString(R.string.error_simreg_nirc));
                    etNIRC.setTextColor(getResources().getColor(R.color.TextError));
                    animateErrorBounceWithCustomMessage(etNIRC, getResources().getString(R.string.snackbar_wrong_nirc));
                }
                else
                {
                    etNIRC.setHint(getResources().getString(R.string.common_text_NIRC));
                    etNIRC.setTextColor(getResources().getColor(R.color.TextPrimaryDark));
                }
            }
        });

        chkIsMalaysian.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged (CompoundButton compoundButton, boolean isChecked)
            {
                isMalaysian = ! isChecked;
                animateNonMalaysian(! isMalaysian);
            }
        });
    }

    private void btnNextListener ()
    {
        final int offset = 20;
        btnNext.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                try
                {
                    int year = mCurrentYear;
                    if (isMalaysian)
                    {
                        Date nirc = null;
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

                        if (isMalaysian)
                        {
                            if (etNIRC.getText().toString().length() > 0)
                            {
                                String date = sdf.format(Common.getAdjustedDate(etNIRC.getText().toString().substring(0, 2)));

                                nirc = sdf.parse(date.substring(0, 4) + "-" + etNIRC.getText().toString().substring(2, 4)
                                    + "-" + etNIRC.getText().toString().substring(4, 6));
                            }
                        }

                        sdf = new SimpleDateFormat("yyyy", Locale.getDefault());
                        year = Integer.parseInt(sdf.format(nirc));
                    }

                    if (etLast8Digit.length() != 8)
                    {
                        svSimReg.smoothScrollTo(0, llRow[0].getTop() - offset);
                        animateErrorBounceWithCustomMessage(etLast8Digit, getString(R.string.snackbar_something_not_correct));
                    }
                    else if (etFullName.length() == 0)
                    {
                        svSimReg.smoothScrollTo(0, llRow[1].getTop() - offset);
                        animateErrorBounce(etFullName);
                    }
                    else if (! Common.IsName(chkIsMalaysian.isChecked(), etFullName.getText().toString().trim()))
                    {
                        svSimReg.smoothScrollTo(0, llRow[1].getTop() - offset);
                        animateErrorBounceWithCustomMessage(etFullName, "Please make sure full name is correct.");
                    }
                    else if (! isMalaysian && etPassport.length() == 0)
                    {
                        svSimReg.smoothScrollTo(0, llRow[4].getTop() - offset);
                        animateErrorBounce(etPassport);
                    }
                    else if (isMalaysian && etNIRC.length() != 12)
                    {
                        svSimReg.smoothScrollTo(0, llRow[4].getTop() - offset);
                        animateErrorBounceWithCustomMessage(etNIRC, getString(R.string.snackbar_something_not_correct));
                    }
                    else if (isMalaysian && ! Common.IsNIRC(etNIRC.getText().toString(), mCurrentYear))
                    {
                        svSimReg.smoothScrollTo(0, llRow[4].getTop() - offset);
                        animateErrorBounceWithCustomMessage(etNIRC, getString(R.string.snackbar_something_not_correct));
                    }
                    else if (isMalaysian && (year > mCurrentYear - 12))
                    {
                        svSimReg.smoothScrollTo(0, llRow[4].getTop() - offset);
                        animateErrorBounceWithCustomMessage(etNIRC, "Age cannot younger than 12 years old.");
                    }
                    else if (etEMAIL.getText().toString().trim().length() > 0 && ! android.util.Patterns.EMAIL_ADDRESS.matcher(etEMAIL.getText()).matches())
                    {
                        svSimReg.smoothScrollTo(0, llRow[5].getTop() - offset);
                        animateErrorBounceWithCustomMessage(etEMAIL, getString(R.string.snackbar_something_not_correct));
                    }
                    else if (! Common.IsMobileNumber(etMobileNumber.getText().toString()))
                    {
                        svSimReg.smoothScrollTo(0, llRow[6].getTop() - offset);
                        animateErrorBounceWithCustomMessage(etMobileNumber, getString(R.string.error_incorrect_mobile_number));
                    }
                    else if (mBuffer1 == null || mBuffer1.length == 0)
                    {
                        svSimReg.smoothScrollTo(0, ivPhoto1.getTop() - offset);
                        animateErrorBounceWithCustomMessage(ivPhoto1, "Form cannot submit without identity photo, please retake.");

                    }
                    else if (etEMAIL.getText().length() > 0 && etEMAIL.getText().toString().contains("tunetalk.com") && ! etEMAIL.getText().toString().equalsIgnoreCase("apps@tunetalk.com"))
                    {
                        svSimReg.smoothScrollTo(0, llRow[13].getTop() - offset);
                        animateErrorBounceWithCustomMessage(etEMAIL, "Email ending with @tunetalk.com. Please use another email address.");
                    }
                    else
                    {
                        if (Common.hasConnectivity(getApplicationContext()))
                            replaceSim();
                        else
                            addTaskToQueue();
                    }
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }
        });
    }

    @SuppressLint ("StaticFieldLeak")
    void replaceSim ()
    {
        final SIMReplacementEntity entity = new SIMReplacementEntity()
            .setLast8Digit(etLast8Digit.getText().toString().trim())
            .setFullName(etFullName.getText().toString().trim())
            .setIsMalaysian(isMalaysian)
            .setMobileNumber(etMobileNumber.getText().toString().trim())
            .setEMAIL(etEMAIL.getText().toString().trim())
            .setStaffLoginId(DocumentIntentManager.init().getIntentEntity().getPartnerDealerCode())
            .setManual(false);

        if (isMalaysian)
            entity.setNIRC(etNIRC.getText().toString().trim());
        else
            entity.setPassport(etPassport.getText().toString().trim());

        entity.setImageOne(Base64.encodeBytes(mBuffer1));

        JSONObject jsonObject = ApiRequestBody.getSimReplacementRequestBody(this, entity);
        ApiProvider.simReplacement(this, jsonObject, new OnApiCallBack<UploadEntity>()
        {
            @Override public void onSuccess (UploadEntity response)
            {
                if (response.getResultCode())
                {
                    ActivityUtils.result(mActivity, Constant.ResultCode.SUCCESSFUL, null);
                }
                else
                {
                    if (Common.isValidString(response.getMessage()) && response.getCode().equals("-500"))
                    {
                        ActivityUtils.result(mActivity, Constant.ResultCode.FAILED, response.getMessage());
                    }
                }
            }

            @Override public void onFailure ()
            {

            }
        });

    }

    @SuppressLint ("StaticFieldLeak")
    void addTaskToQueue ()
    {
        final SIMReplacementEntity entity = new SIMReplacementEntity()
            .setLast8Digit(etLast8Digit.getText().toString().trim())
            .setFullName(etFullName.getText().toString().trim())
            .setIsMalaysian(isMalaysian)
            .setMobileNumber(etMobileNumber.getText().toString().trim())
            .setEMAIL(etEMAIL.getText().toString().trim())
            .setStaffLoginId(DocumentIntentManager.init().getIntentEntity().getPartnerDealerCode())
            .setManual(false);

        if (isMalaysian)
            entity.setNIRC(etNIRC.getText().toString().trim());
        else
            entity.setPassport(etPassport.getText().toString().trim());

        entity.setImageOne(Base64.encodeBytes(mBuffer1));

        DatabaseManager.get().persistSimReplacementRecord(this, entity, new OnSqlResultListener()
        {
            @Override public void onSuccess (TaskEntity entity)
            {
                ActivityUtils.result(mActivity, Constant.ResultCode.OFFLINE_SERVICE, null);
            }

            @Override public void onFailure ()
            {
                Toast.makeText(mActivity, getResources().getString(R.string.toast_unknown_error), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_rescan, menu);

        MenuItem item = menu.findItem(R.id.menu_rescan);
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener()
        {
            @Override
            public boolean onMenuItemClick (MenuItem item)
            {
                String[] options = new String[2];
                options[0] = getResources().getStringArray(R.array.orc_rescan)[getIntent().getIntExtra("Mode", 0)];
                options[1] = getResources().getStringArray(R.array.orc_rescan)[getResources().getStringArray(R.array.orc_rescan).length - 1];

                new AlertDialog.Builder(mActivity, R.style.DialogStyle)
                    .setTitle(R.string.ocr_select_document)
                    .setSingleChoiceItems(options, 100, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick (DialogInterface dialog, int which)
                        {
                            dialog.dismiss();
                            isActionFromMenu = true;
                            if (which == 0)
                            {
                                capture(getIntent().getIntExtra("Mode", 0));
                            }

                            if (which == 1)
                                capture(5);
                        }
                    })
                    .setNegativeButton("BACK", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick (DialogInterface dialog, int which)
                        {
                            dialog.dismiss();
                        }
                    }).show();
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void animateErrorBounceWithCustomMessage (View v, String msg)
    {
        v.setTranslationX(75);
        v.animate()
            .translationX(0)
            .setInterpolator(new BounceInterpolator())
            .setDuration(500)
            .setStartDelay(200)
            .start();

        Snackbar snack = Snackbar.make(v, msg, Snackbar.LENGTH_LONG);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(getResources().getColor(R.color.SnackErrorBackground));
        snack.show();
    }

    private void animateErrorBounce (View v)
    {
        v.setTranslationX(75);
        v.animate()
            .translationX(0)
            .setInterpolator(new BounceInterpolator())
            .setDuration(500)
            .setStartDelay(200)
            .start();

        Snackbar snack = Snackbar.make(v, R.string.snackbar_empty_field, Snackbar.LENGTH_LONG);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(getResources().getColor(R.color.SnackErrorBackground));
        snack.show();
    }

    private void animateNonMalaysian (boolean isChecked)
    {
        final ValueAnimator vaNIRC, vaPassport;

        if (isChecked)
        {
            vaNIRC = ValueAnimator.ofFloat(10f, 0f);
            vaNIRC.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
            {
                @Override
                public void onAnimationUpdate (ValueAnimator valueAnimator)
                {
                    etNIRC.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, (float) valueAnimator.getAnimatedValue()));
                }
            });

            vaPassport = ValueAnimator.ofFloat(0f, 10f);
            vaPassport.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
            {
                @Override
                public void onAnimationUpdate (ValueAnimator valueAnimator)
                {
                    etPassport.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, (float) valueAnimator.getAnimatedValue()));
                }
            });
        }
        else
        {
            vaNIRC = ValueAnimator.ofFloat(0f, 10f);
            vaNIRC.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
            {
                @Override
                public void onAnimationUpdate (ValueAnimator valueAnimator)
                {
                    etNIRC.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, (float) valueAnimator.getAnimatedValue()));
                }
            });

            vaPassport = ValueAnimator.ofFloat(10f, 0f);
            vaPassport.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
            {
                @Override
                public void onAnimationUpdate (ValueAnimator valueAnimator)
                {
                    etPassport.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, (float) valueAnimator.getAnimatedValue()));
                }
            });
        }

        vaNIRC.setDuration(500);
        vaPassport.setDuration(500);
        vaNIRC.setInterpolator(new DecelerateInterpolator());
        vaPassport.setInterpolator(new DecelerateInterpolator());
        etPassport.setText(null);
        etNIRC.setText(null);
        vaNIRC.start();
        vaPassport.start();
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        isActivityExist = false;

        if (mTimer != null)
        {
            mTimer.purge();
            mTimer.cancel();
        }

        if (requestCode == 200 && resultCode == RESULT_OK)
            finish();

        if (resultCode == RESULT_OK)
        {
            if (data != null)
            {
                final Vibrator v = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(200);

                final String filePath = FileUtils.ORC_DIR + "/identity.jpg";

                try
                {
                    mRecognizerBundle.loadFromIntent(data);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                if (requestCode == OCR_REQUEST_CODE)
                {
                    OCRHelper.get().getResult(mRecognizerBundle, new OnScanResult()
                    {
                        @Override public void onScanResult (ScanResultEntity entity)
                        {
                            try
                            {
                                if(entity.getMyKad() != null)
                                {
                                    etFullName.setText(entity.getMyKad().getFullName());
                                    etNIRC.setText(entity.getMyKad().getNric());
                                }
                                else if (entity.getMrtd() != null)
                                {
                                    etFullName.setText(entity.getMrtd().getFullName());
                                    etPassport.setText(entity.getMrtd().getPassportNumber());
                                }
                                else if (entity.getiKad() != null)
                                {
                                    etFullName.setText(entity.getiKad().getFullName());
                                    etPassport.setText(entity.getiKad().getPassportNumber());
                                }
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }

                            if (etNIRC.getText() != null && etNIRC.getText().toString().length() > 0)
                                etNIRC.setEnabled(false);
                            else
                                etNIRC.setEnabled(true);

                            if (etFullName.getText() != null && etFullName.getText().toString().length() > 0)
                                etFullName.setEnabled(false);
                            else
                                etFullName.setEnabled(true);

                            if (etPassport.getText() != null && etPassport.getText().toString().length() > 0)
                                etPassport.setEnabled(false);
                            else
                                etPassport.setEnabled(true);
                        }

                        @Override public void onImageResult (byte[] imageBytes)
                        {
                            mBuffer1 = imageBytes;

                            if (mBuffer1 == null || mBuffer1.length == 0)
                            {
                                new AlertDialog.Builder(mActivity)
                                    .setTitle("Opps...")
                                    .setMessage("Unable to read NRIC/iKad/Passport photo, please try again.")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener()
                                    {
                                        @Override public void onClick (DialogInterface dialog, int which)
                                        {
                                            finish();
                                        }
                                    }).show();
                            }
                            else
                            {
                                FileUtils.ClearOCRFolder();
                                mBuffer1 = FileUtils.AddWatermark(mActivity, mBuffer1);

                                if(! mActivity.isFinishing())
                                {
                                    Glide.with(mActivity)
                                        .load(mBuffer1)
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .signature(new StringSignature(System.currentTimeMillis() + ""))
                                        .into(ivPhoto1);
                                }

                                if (isActionFromMenu)
                                    isActionFromMenu = false;
                                else
                                    capture(5);
                            }
                        }
                    });
                }
                else if (requestCode == OCR_BARCODE_REQUEST_CODE)
                {
                    isActionFromMenu = false;

                    OCRHelper.get().getResult(mRecognizerBundle, new OnScanResult()
                    {
                        @Override public void onScanResult (ScanResultEntity entity)
                        {
                            try
                            {
                                if(entity.getBarcode() != null)
                                {
                                    etLast8Digit.setText(entity.getBarcode().getScannedBarCode().replaceAll("60199900", ""));
                                }
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }

                        @Override public void onImageResult (byte[] imageBytes)
                        {

                        }
                    });
                }
            }
        }
        else
        {
            if (requestCode != OCR_BARCODE_REQUEST_CODE)
            {
                if (! isActionFromMenu)
                {
                    if (requestCode == OCR_REQUEST_CODE)
                        finish();
                }
            }
        }
    }

    public void capture (int type)
    {
        String message = type == 0 ? "Scan a NRIC"
            : type == 1 ? "Scan a Passport"
            : type == 2 ? "Scan a iKad"
            : type == 3 ? "Scan a Work Permit"
            : type == 4 ? "Scan a Visa"
            : "Scan a SIM Card barcode";

        mStyleableToast = StyleableToast.makeText(getApplicationContext(), message, R.style.StyleToast);

        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask()
        {
            @Override public void run ()
            {
                SIMReplacementAutoActivity.this.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run ()
                    {
                        mStyleableToast.show();
                    }
                });
            }
        }, 0, 3500);

        isActivityExist = true;

        FileUtils.MakeOCRFolder();

        mRecognizerBundle = OCRHelper.get().buildMyKadElement();

        if (type == 1 || type == 3 || type == 4)
            mRecognizerBundle = OCRHelper.get().buildMrtdElement();
        else if (type == 2)
            mRecognizerBundle = OCRHelper.get().buildIKadElement();
        else if (type == 5)
            mRecognizerBundle = OCRHelper.get().buildBarCodeElement();

        DocumentUISettings settings = new DocumentUISettings(mRecognizerBundle);
        OCRHelper.get().scan(this, settings,
            type == 5 ? OCR_BARCODE_REQUEST_CODE : OCR_REQUEST_CODE);
    }

    @Override public void onBackPressed ()
    {
        setResult(Constant.ResultCode.USER_CANCELLED);
        super.onBackPressed();
    }

    @Override protected void onDestroy ()
    {
        if (mTimer != null)
        {
            mTimer.purge();
            mTimer.cancel();
        }

        super.onDestroy();
    }
}
