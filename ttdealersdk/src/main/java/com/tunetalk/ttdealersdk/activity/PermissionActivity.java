package com.tunetalk.ttdealersdk.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.tunetalk.ttdealersdk.R;
import com.tunetalk.ttdealersdk.enums.ServiceCategories;
import com.tunetalk.ttdealersdk.ocr.DocumentType;
import com.tunetalk.ttdealersdk.singleton.DocumentIntentManager;
import com.tunetalk.ttdealersdk.util.ActivityUtils;
import com.tunetalk.ttdealersdk.util.Constant;
import com.tunetalk.ttdealersdk.util.Permissions;

public class PermissionActivity extends AppCompatActivity
{
    String[] mScanOptions;
    Intent mServiceIntent;
    String apiKey, iccid;
    ServiceCategories mServiceCategory;

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissions);

        apiKey = getIntent().getStringExtra(Constant.Key.Intent.API_KEY);
        iccid = getIntent().getStringExtra(Constant.Key.Intent.ICCID);
        mServiceCategory = DocumentIntentManager.init().getIntentEntity().getServiceCategories();

        checkPermissions();
    }

    private void checkPermissions ()
    {
        if (Build.VERSION.SDK_INT >= 23)
        {
            if (Permissions.hasPermission(PermissionActivity.this, Permissions.CAMERA,
                Permissions.STORAGE_READ, Permissions.STORAGE_WRITE))
            {
                proceedService();
            }
            else
                requestPermissions(new String[] {Permissions.CAMERA, Permissions.STORAGE_READ,
                    Permissions.STORAGE_WRITE}, Permissions.CAMERA_PERMISSION_CODE);
        }
        else
        {
            proceedService();
        }
    }

    private void proceedService ()
    {
        if(mServiceCategory != null)
        {
            if (mServiceCategory.equals(ServiceCategories.SIM_REG_AUTO))
            {
                mServiceIntent = new Intent(this, SIMRegistrationAutoActivity.class);
                mScanOptions = getResources().getStringArray(R.array.ocr_registration_type);
            }
            else if (mServiceCategory.equals(ServiceCategories.SIM_REPLACEMENT_AUTO))
            {
                mServiceIntent = new Intent(this, SIMReplacementAutoActivity.class);
                mScanOptions = getResources().getStringArray(R.array.ocr_document_type);
            }
            else if (mServiceCategory.equals(ServiceCategories.PORT_IN_AUTO))
            {
                mServiceIntent = new Intent(this, PortInAutoActivity.class);
                mScanOptions = getResources().getStringArray(R.array.ocr_document_type);
            }

            new AlertDialog.Builder(this, R.style.DialogStyle)
                .setTitle(R.string.ocr_select_document)
                .setSingleChoiceItems(mScanOptions, 100, new DialogInterface.OnClickListener()
                {
                    @Override public void onClick (DialogInterface dialog, int which)
                    {
                        setDocumentType(which);

                        startActivityForResult(mServiceIntent, Constant.RequestCode.SCAN_DOCUMENT_REQUEST_CODE);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(getString(R.string.common_text_back), new DialogInterface.OnClickListener()
                {
                    @Override public void onClick (DialogInterface dialog, int which)
                    {
                        dialog.dismiss();

                        ActivityUtils.result(PermissionActivity.this, Constant.ResultCode.USER_CANCELLED, getString(R.string.action_user_cancel));
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener()
                {
                    @Override public void onCancel (DialogInterface dialogInterface)
                    {
                        ActivityUtils.result(PermissionActivity.this, Constant.ResultCode.USER_CANCELLED, getString(R.string.action_user_cancel));
                    }
                })
                .show();
        }
    }

    private void setDocumentType (int position)
    {
        DocumentIntentManager.init().getIntentEntity()
            .setDocumentType(position == 0 ? DocumentType.MYKAD
                : position == 1 ? DocumentType.MRTD
                : DocumentType.IKAD);
    }

    @Override public void onRequestPermissionsResult (int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (Permissions.hasPermission(this, Permissions.CAMERA, Permissions.STORAGE_READ, Permissions.STORAGE_WRITE))
        {
            proceedService();
        }
        else
        {
            new AlertDialog.Builder(this, R.style.DialogStyle)
                .setMessage("Tunetalk Dealer required number of permission in order to work properly.")
                .setTitle("Request Permission")
                .setPositiveButton(getString(R.string.btn_ok), new DialogInterface.OnClickListener()
                {
                    @Override public void onClick (DialogInterface dialog, int which)
                    {
                        finish();
                        dialog.dismiss();
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener()
                {
                    @Override public void onCancel (DialogInterface dialogInterface)
                    {
                        finish();
                    }
                })
                .show();

        }
    }

    @Override protected void onActivityResult (int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant.RequestCode.SCAN_DOCUMENT_REQUEST_CODE)
        {
            switch (resultCode)
            {
                case Constant.ResultCode.USER_CANCELLED:
                    ActivityUtils.result(this, Constant.ResultCode.USER_CANCELLED, getString(R.string.action_user_cancel));
                    break;
                case Constant.ResultCode.SUCCESSFUL:
                case Constant.ResultCode.FAILED:
                case Constant.ResultCode.OFFLINE_SERVICE:
                    setResult(resultCode, data);
                    finish();
                    break;
                default:
                    break;
            }
        }
    }

    @Override public void onBackPressed ()
    {
        super.onBackPressed();
        finish();
    }

    @Override protected void onDestroy ()
    {
        super.onDestroy();
        DocumentIntentManager.reset();
    }
}
