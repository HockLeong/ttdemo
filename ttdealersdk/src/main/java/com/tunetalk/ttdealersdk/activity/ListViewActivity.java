package com.tunetalk.ttdealersdk.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.rey.material.widget.EditText;
import com.tunetalk.ttdealersdk.R;
import com.tunetalk.ttdealersdk.entity.response.country.CountryEntity;
import com.tunetalk.ttdealersdk.entity.response.country.CountryRootEntity;
import com.tunetalk.ttdealersdk.entity.response.geolocation.CityEntity;
import com.tunetalk.ttdealersdk.entity.response.geolocation.CityRootEntity;
import com.tunetalk.ttdealersdk.entity.response.geolocation.StateEntity;
import com.tunetalk.ttdealersdk.entity.response.geolocation.StateRootEntity;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ListViewActivity extends AppCompatActivity
{
    Activity mActiviy;
    Intent mIntent;
    List<String> mReferenceCollection;
    List<String> mCollection;
    ListView mListView;
    EditText etSearch;
    SimpleAdapter lvAdapter;

    int mType;
    String mSelectedItem;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_selector);
        mActiviy = this;
        mIntent = getIntent();
        mReferenceCollection = new ArrayList<>();
        mType = mIntent.getExtras().getInt("Type");
        mListView = (ListView) findViewById(R.id.list_selector_ListView);
        etSearch = (EditText) findViewById(R.id.list_selector_etSearch);

        etSearch.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int x, int x1, int x2)
            {
            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                if(editable.toString().trim().length() != 0)
                {
                    mCollection.clear();
                    for(int i = 0; i < mReferenceCollection.size(); ++i)
                        if(mReferenceCollection.get(i).contains(editable.toString().toUpperCase()))
                            mCollection.add(mReferenceCollection.get(i));

                    lvAdapter.notifyDataSetChanged();
                }
                else
                {
                    mCollection = new ArrayList<>(mReferenceCollection);
                    lvAdapter.notifyDataSetChanged();
                }
            }
        });

        new Runnable()
        {
            @Override
            public void run()
            {
                if(mType == 1 || mType == 4) //Nationality || Country
                {
                    String jsonCountry = readJsonFromAssets("Country.json");
                    if(jsonCountry != null && !jsonCountry.isEmpty())
                    {
                        CountryRootEntity countryRoot = new Gson().fromJson(jsonCountry, CountryRootEntity.class);
                        CountryEntity[] countryList = countryRoot.getCountryEntity();
                        for(CountryEntity country : countryList)
                            mReferenceCollection.add(country.getName());

                        if(mType == 1)
                            mReferenceCollection.remove("MALAYSIA");
                    }
                }
                else if(mType == 2)  //State
                {
                    String jsonState = readJsonFromAssets("State.json");
                    if(jsonState != null && !jsonState.isEmpty())
                    {
                        StateRootEntity stateRoot = new Gson().fromJson(jsonState, StateRootEntity.class);
                        StateEntity[] stateList = stateRoot.getStateEntity();
                        for(StateEntity state : stateList)
                            mReferenceCollection.add(state.getStateName());
                    }
                }
                else if(mType == 3)  //City
                {
                    String selectedState = getIntent().getStringExtra("State");
                    String jsonState = readJsonFromAssets("State.json");
                    String jsonCity = readJsonFromAssets("City.json");
                    int StateID = -1;

                    if(jsonState != null && !jsonState.isEmpty())
                    {
                        StateRootEntity stateRoot = new Gson().fromJson(jsonState, StateRootEntity.class);
                        StateEntity[] stateList = stateRoot.getStateEntity();

                        for(int i = 0; i < stateList.length; ++i)
                            if(stateList[i].getStateName().equals(selectedState))
                                StateID = Integer.parseInt(stateList[i].getStateID());

                        if(StateID > 0 && jsonCity != null && !jsonCity.isEmpty())
                        {
                            CityRootEntity cityRoot = new Gson().fromJson(jsonCity, CityRootEntity.class);
                            CityEntity[] cityList = cityRoot.getCityEntity();
                            for(CityEntity city : cityList)
                                if(city.getStateID() == StateID)
                                    mReferenceCollection.add(city.getCityName().toUpperCase());
                        }
                    }
                }
            }
        }.run();

        mCollection = new ArrayList<>(mReferenceCollection);
        lvAdapter = new SimpleAdapter();
        mListView.setAdapter(lvAdapter);
    }

    private String readJsonFromAssets(String filename)
    {
        try
        {
            InputStream is = this.getAssets().open(filename);
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            is.close();
            return new String(buffer, "UTF-8");
        }
        catch (IOException io)
        {
            io.printStackTrace();
            return null;
        }
    }

    class SimpleAdapter extends BaseAdapter
    {
        public SimpleAdapter()
        {
        }

        @Override
        public int getCount()
        {
            return mCollection.size();
        }

        @Override
        public Object getItem(int i)
        {
            return i;
        }

        @Override
        public long getItemId(int i)
        {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup)
        {
            final int position = i;
            LayoutInflater inflater = (LayoutInflater) mActiviy.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if(view == null)
                view = inflater.inflate(R.layout.item_list_selector, viewGroup, false);
            TextView tvName = (TextView) view.findViewById(R.id.list_selector_TextView);
            tvName.setText(mCollection.get(position));

            view.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    mSelectedItem = mCollection.get(position);
                    notifyDataSetChanged();
                    mIntent.putExtra("SelectedItem", mSelectedItem);
                    mIntent.putExtra("Type", mType);
                    mActiviy.setResult(RESULT_OK, mIntent);
                    new Handler().postDelayed(
                            new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    keyboard.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                                    finish();
                                }
                            },500);
                }
            });
            return view;
        }
    }
}
