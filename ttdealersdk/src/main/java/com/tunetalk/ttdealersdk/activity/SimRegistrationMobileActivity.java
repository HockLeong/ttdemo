package com.tunetalk.ttdealersdk.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.rey.material.widget.EditText;
import com.tunetalk.ttdealersdk.R;
import com.tunetalk.ttdealersdk.api.ApiProvider;
import com.tunetalk.ttdealersdk.api.ApiRequestBody;
import com.tunetalk.ttdealersdk.api.OnApiCallBack;
import com.tunetalk.ttdealersdk.base.BaseActivity;
import com.tunetalk.ttdealersdk.entity.response.DesiredMSISDNListEntity;
import com.tunetalk.ttdealersdk.entity.response.UploadEntity;
import com.tunetalk.ttdealersdk.util.ActivityUtils;
import com.tunetalk.ttdealersdk.util.Common;
import com.tunetalk.ttdealersdk.util.Constant;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class SimRegistrationMobileActivity extends BaseActivity
{
    EditText etSearch;
    ListView lvMobile;
    MobileAdapter lvAdapter;
    LinearLayout llNoResult;

    List<String> mCollection;

    @Override
    public void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sim_registration_mobile);

        setToolbarTitle(getString(R.string.mobile_number));

        etSearch = findViewById(R.id.etSearch);
        lvMobile = findViewById(R.id.lvMobile);
        llNoResult = findViewById(R.id.llNoResult);

        mCollection = new ArrayList<>();

        getPreferredNumber(generateRandomDigits(10000));

        etSearch.addTextChangedListener(new TextWatcher()
        {
            @Override public void beforeTextChanged (CharSequence s, int start, int count, int after)
            {

            }

            @Override public void onTextChanged (CharSequence s, int start, int before, int count)
            {

            }

            @Override public void afterTextChanged (Editable s)
            {
                if (s.toString().trim().length() != 0)
                {
                    if (s.toString().trim().length() == 4)
                    {
                        mCollection.clear();
                        getPreferredNumber(s.toString().trim());
                    }
                }
            }
        });
    }

    class MobileAdapter extends BaseAdapter
    {
        public MobileAdapter ()
        {

        }

        @Override
        public int getCount ()
        {
            return mCollection.size();
        }

        @Override
        public Object getItem (int i)
        {
            return i;
        }

        @Override
        public long getItemId (int i)
        {
            return i;
        }

        @Override
        public View getView (final int i, View view, ViewGroup viewGroup)
        {
            if (view == null)
                view = getLayoutInflater().inflate(R.layout.item_list_selector, viewGroup, false);

            ((TextView) view.findViewById(R.id.list_selector_TextView)).setText(mCollection.get(i));

            view.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick (View view)
                {
                    getIntent().putExtra("MOBILE", mCollection.get(i));
                    setResult(RESULT_OK, getIntent());

                    new Handler().postDelayed(new Runnable()
                    {
                        @Override
                        public void run ()
                        {
                            InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            keyboard.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                            finish();
                        }
                    }, 300);

                }
            });
            return view;
        }
    }

    private void getPreferredNumber (final String digits)
    {

        ApiProvider.getPreferNumber(this, digits, new OnApiCallBack<DesiredMSISDNListEntity>()
        {
            @Override public void onSuccess (DesiredMSISDNListEntity response)
            {
                if (response.getResultCode())
                {
                    if (Common.isValidList(response.getDesiredMSISDN()))
                    {
                        for (int i = 0; i < response.getDesiredMSISDN().size(); i++)
                            mCollection.add("0" + response.getDesiredMSISDN().get(i));

                        lvMobile.setVisibility(View.VISIBLE);
                        llNoResult.setVisibility(View.GONE);

                        if (lvAdapter == null)
                        {
                            lvAdapter = new MobileAdapter();
                            lvMobile.setAdapter(lvAdapter);
                        }
                        else
                        {
                            lvAdapter.notifyDataSetChanged();
                        }
                    }
                    else
                    {
                        lvMobile.setVisibility(View.GONE);
                        llNoResult.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    new AlertDialog.Builder(mActivity)
                        .setMessage(response.getMessage())
                        .setPositiveButton("OK", null)
                        .show();

                    lvMobile.setVisibility(View.GONE);
                    llNoResult.setVisibility(View.VISIBLE);
                }
            }

            @Override public void onFailure ()
            {

            }
        });
    }

    private String generateRandomDigits (int digit)
    {
        Random random = new Random();
        return String.format(Locale.getDefault(), "%04d", random.nextInt(digit));
    }
}
