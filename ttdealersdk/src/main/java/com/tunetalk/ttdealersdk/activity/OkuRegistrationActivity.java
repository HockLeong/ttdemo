package com.tunetalk.ttdealersdk.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.tunetalk.ttdealersdk.R;
import com.tunetalk.ttdealersdk.api.ApiProvider;
import com.tunetalk.ttdealersdk.api.ApiRequestBody;
import com.tunetalk.ttdealersdk.api.OnApiCallBack;
import com.tunetalk.ttdealersdk.base.BaseActivity;
import com.tunetalk.ttdealersdk.entity.request.PortInEntity;
import com.tunetalk.ttdealersdk.entity.request.SIMRegistrationEntity;
import com.tunetalk.ttdealersdk.entity.response.UploadEntity;
import com.tunetalk.ttdealersdk.entity.response.sql.TaskEntity;
import com.tunetalk.ttdealersdk.enums.ServiceCategories;
import com.tunetalk.ttdealersdk.singleton.DatabaseManager;
import com.tunetalk.ttdealersdk.sql.OnSqlResultListener;
import com.tunetalk.ttdealersdk.util.ActivityUtils;
import com.tunetalk.ttdealersdk.util.Base64;
import com.tunetalk.ttdealersdk.util.Common;
import com.tunetalk.ttdealersdk.util.Constant;

import org.json.JSONObject;

public class OkuRegistrationActivity extends BaseActivity
{
    LinearLayout llDocOne, llDocTwo;
    ImageView ivOkuDocOne, ivOkuDocTwo;
    Switch swParent;
    Button btnNext;

    Intent mCameraIntent;
    ServiceCategories mCategories;

    private String base64Doc1, base64Doc2;

    private String first8Digit;

    public static String TAG = OkuRegistrationActivity.class.getSimpleName();

    SIMRegistrationEntity mSimRegistrationEntity;
    PortInEntity mPortInEntity;

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oku_registration);

        setToolbarTitle(getString(R.string.activity_special_needs));

        llDocOne = findViewById(R.id.llDocOne);
        llDocTwo = findViewById(R.id.llDocTwo);
        ivOkuDocOne = findViewById(R.id.ivOkuDocOne);
        ivOkuDocTwo = findViewById(R.id.ivOkuDocTwo);
        swParent = findViewById(R.id.swParent);
        btnNext = findViewById(R.id.btnNext);

        first8Digit = getResources().getString(R.string.simreg_text_first_8_digit);

        mCategories = (ServiceCategories) getIntent().getSerializableExtra(Constant.Key.Intent.SERVICE_CATEGORIES);

        try
        {
            mSimRegistrationEntity = (SIMRegistrationEntity) getIntent().getSerializableExtra(Constant.Key.Intent.SIM_REGISTRATION_REQUEST_BODY);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            mPortInEntity = (PortInEntity) getIntent().getSerializableExtra(Constant.Key.Intent.PORT_IN_REQUEST_BODY);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        llDocTwo.setVisibility(View.GONE);

        ivOkuDocOne.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick (View v)
            {
                mCameraIntent = new Intent(mActivity, MySurfaceView.class);
                mCameraIntent.putExtra("Type", 3);
                mCameraIntent.putExtra("Position", 0);
                startCameraActivityWithDelay();
            }
        });

        ivOkuDocTwo.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick (View v)
            {
                mCameraIntent = new Intent(mActivity, MySurfaceView.class);
                mCameraIntent.putExtra("Type", 4);
                mCameraIntent.putExtra("Position", 1);
                startCameraActivityWithDelay();
            }
        });

        swParent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override public void onCheckedChanged (CompoundButton buttonView, final boolean isChecked)
            {
                final float alphaValue = isChecked ? 1 : 0;
                llDocTwo.setVisibility(isChecked ? View.VISIBLE : View.GONE);

            }
        });

        if (mCategories.equals(ServiceCategories.SIM_REG_AUTO))
            btnNext.setText(getString(R.string.btn_register));
        else if (mCategories.equals(ServiceCategories.PORT_IN_AUTO))
            btnNext.setText(getString(R.string.btn_submit_port_in));
        else
            btnNext.setText(getString(R.string.common_text_next));

        btnNext.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick (View v)
            {
                if (base64Doc1 == null)
                {
                    Toast.makeText(getApplicationContext(), "OKU Card image cannot be empty", Toast.LENGTH_SHORT).show();
                }
                else if (swParent.isChecked() && base64Doc2 == null)
                {
                    Toast.makeText(getApplicationContext(), "OKU\'s NRIC image cannot be empty", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    validateService();
                }
            }
        });
    }

    private void validateService ()
    {
        if (mCategories.equals(ServiceCategories.SIM_REG_AUTO))
        {
            mSimRegistrationEntity.setOkuImageOne(base64Doc1);

            if (swParent.isChecked() && base64Doc2 != null)
                mSimRegistrationEntity.setOkuImageTwo(base64Doc2);

            if (Common.hasConnectivity(getApplicationContext()))
                simRegistration();
            else
                addSimRegTask();
        }

        if (mCategories.equals(ServiceCategories.PORT_IN_AUTO))
        {
            mPortInEntity.setOkuImageOne(base64Doc1);

            if (swParent.isChecked() && base64Doc2 != null)
                mPortInEntity.setOkuImageTwo(base64Doc2);

            if (Common.hasConnectivity(getApplicationContext()))
                portIn();
            else
                addPortInTask();
        }
    }

    @SuppressLint ("StaticFieldLeak")
    private void simRegistration ()
    {
        JSONObject jsonObject = ApiRequestBody.getSimRegRequestBody(this, mSimRegistrationEntity);
        ApiProvider.simRegistration(this, jsonObject, new OnApiCallBack<UploadEntity>()
        {
            @Override public void onSuccess (UploadEntity response)
            {
                if (response.getResultCode())
                {
                    ActivityUtils.result(mActivity, Constant.ResultCode.SUCCESSFUL, null);
                }
                else
                {
                    if (Common.isValidString(response.getMessage()) && response.getCode().equals("-500"))
                    {
                        ActivityUtils.result(mActivity, Constant.ResultCode.FAILED, response.getMessage());
                    }
                }
            }

            @Override public void onFailure ()
            {

            }
        });
    }

    @SuppressLint ("StaticFieldLeak") private void addSimRegTask ()
    {
        DatabaseManager.get().persistSimRegRecord(this, mSimRegistrationEntity, new OnSqlResultListener()
        {
            @Override public void onSuccess (TaskEntity entity)
            {
                ActivityUtils.result(mActivity, Constant.ResultCode.OFFLINE_SERVICE, null);
//                Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_submit_reg_complete), Toast.LENGTH_LONG).show();
            }

            @Override public void onFailure ()
            {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_unknown_error), Toast.LENGTH_LONG).show();
            }
        });
    }

    @SuppressLint ("StaticFieldLeak")
    private void portIn ()
    {
        JSONObject jsonObject = ApiRequestBody.getPortInRequestBody(this, mPortInEntity);
        ApiProvider.portIn(this, jsonObject, new OnApiCallBack<UploadEntity>()
        {
            @Override public void onSuccess (UploadEntity response)
            {
                if (response.getResultCode())
                {
                    ActivityUtils.result(mActivity, Constant.ResultCode.SUCCESSFUL, null);
                }
                else
                {
                    if (Common.isValidString(response.getMessage()) && response.getCode().equals("-500"))
                    {
                        ActivityUtils.result(mActivity, Constant.ResultCode.FAILED, response.getMessage());
                    }
                }
            }

            @Override public void onFailure ()
            {

            }
        });
    }

    @SuppressLint ("StaticFieldLeak")
    private void addPortInTask ()
    {
        DatabaseManager.get().persistPortInRecord(this, mPortInEntity, new OnSqlResultListener()
        {
            @Override public void onSuccess (TaskEntity entity)
            {
                ActivityUtils.result(mActivity, Constant.ResultCode.OFFLINE_SERVICE, null);
//                Toast.makeText(mActivity, getResources().getString(R.string.toast_submit_port_complete), Toast.LENGTH_LONG).show();
            }

            @Override public void onFailure ()
            {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_unknown_error), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void startCameraActivityWithDelay ()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA))
            {
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, 99);
            }
            else
            {
                Toast.makeText(mActivity, R.string.snackbar_camera_permission_denied, Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            startActivityForResult(mCameraIntent, 0);
        }
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
        {
            if (data.hasExtra("ImageBuffer"))
            {
                byte[] imgBuffer = data.getExtras().getByteArray("ImageBuffer");
                Bitmap bmp = BitmapFactory.decodeByteArray(imgBuffer, 0, imgBuffer.length);

                switch (data.getExtras().getInt("Type"))
                {
                    case 3:
                        ivOkuDocOne.setImageBitmap(bmp);
                        ivOkuDocOne.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        base64Doc1 = Base64.encodeBytes(imgBuffer);
                        break;
                    case 4:

                        ivOkuDocTwo.setImageBitmap(bmp);
                        ivOkuDocTwo.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        base64Doc2 = Base64.encodeBytes(imgBuffer);
                        break;
                }
            }
        }
    }
}
