package com.tunetalk.ttdealersdk.ui;

import android.app.Activity;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tunetalk.ttdealersdk.R;

public class CustomInfoDialog
{
    Activity _mActivity;
    AlertDialog mDialog;
    View mView;

    public CustomInfoDialog (Activity act)
    {
        _mActivity = act;
    }

    public View showDialog (String title, String message, final DialogType dialogType)
    {
        mDialog = new AlertDialog.Builder(_mActivity)
            .setCancelable(false)
            .create();

        if (dialogType == DialogType.CONFIRMATION)
        {
            mView = _mActivity.getLayoutInflater().inflate(R.layout.dialog_confirmation, null, false);
            mDialog.setView(mView);

            ((TextView) mView.findViewById(R.id.tvTitle)).setText(title);
            ((TextView) mView.findViewById(R.id.tvMessage)).setText(message);

            mView.findViewById(R.id.btnConfirm).setOnClickListener(new View.OnClickListener()
            {
                @Override public void onClick (View v)
                {
                    mDialog.dismiss();
                }
            });

            mView.findViewById(R.id.tvCancel).setOnClickListener(new View.OnClickListener()
            {
                @Override public void onClick (View v)
                {
                    mDialog.dismiss();
                }
            });
        }
        else if (dialogType == DialogType.SUCCESSFUL)
        {
            mView = _mActivity.getLayoutInflater().inflate(R.layout.dialog_successful_message, null, false);
            mDialog.setView(mView);

            ((TextView) mView.findViewById(R.id.tvTitle)).setText(title);
            ((TextView) mView.findViewById(R.id.tvMessage)).setText(message);

            mView.findViewById(R.id.btnOK).setOnClickListener(new View.OnClickListener()
            {
                @Override public void onClick (View v)
                {
                    mDialog.dismiss();
                }
            });
        }
        else if (dialogType == DialogType.FAILED)
        {
            mView = _mActivity.getLayoutInflater().inflate(R.layout.dialog_successful_message, null, false);
            mDialog.setView(mView);

            ((ImageView) mView.findViewById(R.id.ivImageView)).setImageResource(R.mipmap.failed);
            ((TextView) mView.findViewById(R.id.tvTitle)).setText(title);
            ((TextView) mView.findViewById(R.id.tvMessage)).setText(message);

            mView.findViewById(R.id.btnOK).setOnClickListener(new View.OnClickListener()
            {
                @Override public void onClick (View v)
                {
                    mDialog.dismiss();
                }
            });
        }

        if (! _mActivity.isFinishing())
            mDialog.show();

        return mView;
    }

    public void dismiss ()
    {
        if (mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
    }

    public AlertDialog getDialog ()
    {
        return mDialog;
    }

    public enum DialogType
    {
        CONFIRMATION, SUCCESSFUL, FAILED
    }
}
