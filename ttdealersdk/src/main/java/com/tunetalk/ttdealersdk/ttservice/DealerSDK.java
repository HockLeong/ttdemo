package com.tunetalk.ttdealersdk.ttservice;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.microblink.MicroblinkSDK;
import com.microblink.intent.IntentDataTransferMode;
import com.tunetalk.ttdealersdk.R;
import com.tunetalk.ttdealersdk.activity.PermissionActivity;
import com.tunetalk.ttdealersdk.api.ApiProvider;
import com.tunetalk.ttdealersdk.api.OnApiCallBack;
import com.tunetalk.ttdealersdk.api.Webservice;
import com.tunetalk.ttdealersdk.entity.response.InitEntity;
import com.tunetalk.ttdealersdk.entity.response.OfflineTaskSubmissionEntity;
import com.tunetalk.ttdealersdk.entity.response.PendingTaskEntity;
import com.tunetalk.ttdealersdk.entity.response.UploadEntity;
import com.tunetalk.ttdealersdk.entity.response.sql.TaskEntity;
import com.tunetalk.ttdealersdk.enums.ServiceCategories;
import com.tunetalk.ttdealersdk.singleton.DocumentIntentManager;
import com.tunetalk.ttdealersdk.sql.SQLiteHandler;
import com.tunetalk.ttdealersdk.util.APIConstant;
import com.tunetalk.ttdealersdk.util.ActivityUtils;
import com.tunetalk.ttdealersdk.util.Common;
import com.tunetalk.ttdealersdk.util.Constant;
import com.tunetalk.ttdealersdk.util.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DealerSDK
{
    private static final String TAG = DealerSDK.class.getName();
    private static boolean isInit;
    private static SQLiteHandler mSqlHandler;

    /**
     * API called to init the SDK by obtaining MicroBlink license key
     *
     * @param activity
     * @param apiKey
     * @param isProduction
     * @param listener
     * @return
     */
    public static synchronized DealerSDK init (final Activity activity, final String apiKey, boolean isProduction, final OnInitResultListener listener)
    {
        APIConstant.isProductionServer = isProduction;

        String licenseKey = SharedPreferencesUtils.getString(activity, Constant.Preferences.OFFLINE_DATA);

        if (Common.hasConnectivity(activity))
        {
            ApiProvider.initSDK(activity, apiKey, new OnApiCallBack<InitEntity>()
            {
                @Override public void onSuccess (InitEntity response)
                {
                    if (response.getResultCode())
                    {
                        isInit = Common.isValidString(response.getMircroBlinkLicenseKey()) && Common.isValidString(response.getApiKey());

                        setMicroblinkLicenseKey(activity, response.getMircroBlinkLicenseKey());

                        Webservice.HEADER = new HashMap<>();
                        Webservice.HEADER.put(Constant.Key.Common.APIKEY, response.getApiKey());

                        if (listener != null)
                            listener.onInitResult(true, response.getMessage());
                    }
                    else
                    {
//                    if (response.getCode().equals("-500") && ! Common.isNullOrEmpty(response.getMessage()))
//                    {
//                        ActivityUtils.result(activity, Constant.ResultCode.Common.INIT_REQUIRED, response.getMessage());
//                    }

                        isInit = false;

                        if (listener != null)
                            listener.onInitResult(false, response.getMessage());
                    }
                }

                @Override public void onFailure ()
                {
                    isInit = false;
                }
            });
        }
        else
        {
            if (Common.isValidString(licenseKey))
            {
                isInit = true;
                setMicroblinkLicenseKey(activity, licenseKey);
            }
            else
            {
                isInit = false;
                Log.e(TAG, activity.getString(R.string.init_api_message));
            }
        }

        return new DealerSDK();
    }

    /**
     * Used when perform sim registration process
     *
     * @param activity
     * @param partnerDealerCode
     */
    public static void simRegister (Activity activity, String partnerDealerCode)
    {
        if (isInit)
        {
            if(! Common.isValidString(partnerDealerCode))
            {
                Log.e(TAG, activity.getString(R.string.error_invalid_partner_dealer_code));
                return;
            }

            DocumentIntentManager.init().getIntentEntity()
                .setPartnerDealerCode(partnerDealerCode)
                .setServiceCategories(ServiceCategories.SIM_REG_AUTO);

            Intent intent = new Intent(activity, PermissionActivity.class);
            activity.startActivityForResult(intent, Constant.RequestCode.SCAN_DOCUMENT_REQUEST_CODE);
        }
        else
        {
            Log.e(TAG, activity.getString(R.string.init_api_message));
        }
    }

    /**
     * Used when perform sim replacement process
     *
     * @param activity
     * @param partnerDealerCode
     */
    public static void simReplacement (Activity activity, String partnerDealerCode)
    {
        if (isInit)
        {
            if(! Common.isValidString(partnerDealerCode))
            {
                Log.e(TAG, activity.getString(R.string.error_invalid_partner_dealer_code));
                return;
            }

            DocumentIntentManager.init().getIntentEntity()
                .setPartnerDealerCode(partnerDealerCode)
                .setServiceCategories(ServiceCategories.SIM_REPLACEMENT_AUTO);

            Intent intent = new Intent(activity, PermissionActivity.class);
            activity.startActivityForResult(intent, Constant.RequestCode.SCAN_DOCUMENT_REQUEST_CODE);
        }
        else
        {
            Log.e(TAG, activity.getString(R.string.init_api_message));
        }
    }

    /**
     * Used when perform port in process
     *
     * @param activity
     * @param partnerDealerCode
     */
    public static void portIn (Activity activity, String partnerDealerCode)
    {
        if (isInit)
        {
            if(! Common.isValidString(partnerDealerCode))
            {
                Log.e(TAG, activity.getString(R.string.error_invalid_partner_dealer_code));
                return;
            }

            DocumentIntentManager.init().getIntentEntity()
                .setPartnerDealerCode(partnerDealerCode)
                .setServiceCategories(ServiceCategories.PORT_IN_AUTO);

            Intent intent = new Intent(activity, PermissionActivity.class);
            activity.startActivityForResult(intent, Constant.RequestCode.SCAN_DOCUMENT_REQUEST_CODE);
        }
        else
        {
            Log.e(TAG, activity.getString(R.string.init_api_message));
        }
    }

    public static List<PendingTaskEntity> getPendingTasks (Context context)
    {
        List<PendingTaskEntity> pendingTaskList = new ArrayList<>();

        SQLiteHandler sql = new SQLiteHandler(context);
        List<TaskEntity> taskList = sql.readAllPendingTasks();

        if (isInit)
        {
            if (Common.isValidList(taskList))
            {
                for (TaskEntity task : taskList)
                {
                    PendingTaskEntity entity = new PendingTaskEntity()
                        .setId(task.getId())
                        .setInsertDate(task.getInsertDate())
                        .setSimNo(task.getSimNo())
                        .setServiceType(getServiceType(task.getType()));

                    pendingTaskList.add(entity);
                }
            }

            return pendingTaskList;
        }
        else
        {
            Log.e(TAG, context.getString(R.string.init_api_message));
            return null;
        }
    }

    public static void submitPendingTasks (Activity activity, String id, final OnTaskSubmitListener submitListener)
    {
        mSqlHandler = new SQLiteHandler(activity);
        TaskEntity entity = mSqlHandler.readPendingTaskById(id);

        switch (entity.getType())
        {
            case 0:
                submitRegistration(activity, entity, submitListener);
                break;
            case 1:
                submitReplacement(activity, entity, submitListener);
                break;
            case 2:
                submitPortIn(activity, entity, submitListener);
                break;
            default:
                break;
        }
    }

    private static void submitRegistration (Activity activity, final TaskEntity task,
                                            final OnTaskSubmitListener submitListener)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(task.getJSON());

            ApiProvider.simRegistration(activity, jsonObject, new OnApiCallBack<UploadEntity>()
            {
                @Override public void onSuccess (UploadEntity response)
                {
                    if (submitListener != null)
                    {
                        OfflineTaskSubmissionEntity entity = new OfflineTaskSubmissionEntity()
                            .setSimNo(task.getSimNo())
                            .setCreatedDate(task.getInsertDate());

                        if (response.getResultCode())
                        {
                            entity.setResultCode(Constant.ResultCode.SUCCESSFUL);
                        }
                        else
                        {
                            entity
                                .setResultCode(Constant.ResultCode.FAILED)
                                .setErrorDescription(response.getMessage());
                        }

                        submitListener.onTaskSuccess(entity);

                        mSqlHandler.removeTask(task.getId());
                    }
                }

                @Override public void onFailure ()
                {
                    if (submitListener != null)
                    {
                        submitListener.onTasksFailed(Constant.ResultCode.FAILED, "Server error");
                    }
                }
            });
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private static void submitReplacement (Activity activity, final TaskEntity task,
                                           final OnTaskSubmitListener submitListener)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(task.getJSON());

            ApiProvider.simReplacement(activity, jsonObject, new OnApiCallBack<UploadEntity>()
            {
                @Override public void onSuccess (UploadEntity response)
                {
                    if (submitListener != null)
                    {
                        OfflineTaskSubmissionEntity entity = new OfflineTaskSubmissionEntity()
                            .setSimNo(task.getSimNo())
                            .setCreatedDate(task.getInsertDate());

                        if (response.getResultCode())
                        {
                            entity.setResultCode(Constant.ResultCode.SUCCESSFUL);
                        }
                        else
                        {
                            entity
                                .setResultCode(Constant.ResultCode.FAILED)
                                .setErrorDescription(response.getMessage());
                        }

                        submitListener.onTaskSuccess(entity);

                        mSqlHandler.removeTask(task.getId());
                    }
                }

                @Override public void onFailure ()
                {
                    if (submitListener != null)
                    {
                        submitListener.onTasksFailed(Constant.ResultCode.FAILED, "Server error");
                    }
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static void submitPortIn (Activity activity, final TaskEntity task,
                                      final OnTaskSubmitListener submitListener)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(task.getJSON());

            ApiProvider.portIn(activity, jsonObject, new OnApiCallBack<UploadEntity>()
            {
                @Override public void onSuccess (UploadEntity response)
                {
                    if (submitListener != null)
                    {
                        OfflineTaskSubmissionEntity entity = new OfflineTaskSubmissionEntity()
                            .setSimNo(task.getSimNo())
                            .setCreatedDate(task.getInsertDate());

                        if (response.getResultCode())
                        {
                            entity.setResultCode(Constant.ResultCode.SUCCESSFUL);
                        }
                        else
                        {
                            entity
                                .setResultCode(Constant.ResultCode.FAILED)
                                .setErrorDescription(response.getMessage());
                        }

                        submitListener.onTaskSuccess(entity);

                        mSqlHandler.removeTask(task.getId());
                    }
                }

                @Override public void onFailure ()
                {
                    if (submitListener != null)
                    {
                        submitListener.onTasksFailed(Constant.ResultCode.FAILED, "Server error");
                    }
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static String getServiceType (int type)
    {
        return type == 0 ? "Sim Registration"
            : type == 1 ? "Sim Replacement"
            : "Port In";
    }

    private static void setMicroblinkLicenseKey (Activity activity, String licenseKey)
    {
        try
        {
//            licenseKey = "sRwAAAAXY29tLnR1bmV0YWxrLnR1bmVkZWFsZXI3LAb6dWkNZ0gTnzq8l8Ol94NsG6lYaNgv9yn7Y/WbZx5VCACKi89D5rtgLYClePGETzzRWbyP/86Yd72EGh5/ycrm0xrNk0thssJ2/I/yEmBVzf5KkSBFCus+N4WqJVy0+4ZocA==";

            SharedPreferencesUtils.insert(activity, Constant.Preferences.OFFLINE_DATA, licenseKey);
            MicroblinkSDK.setLicenseKey(licenseKey, activity.getApplicationContext());
            MicroblinkSDK.setIntentDataTransferMode(IntentDataTransferMode.PERSISTED_OPTIMISED);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

}
