package com.tunetalk.ttdealersdk.ttservice;

public interface OnInitResultListener
{
    void onInitResult (boolean isInit, String errorDescription);
}
