package com.tunetalk.ttdealersdk.ttservice;

import com.tunetalk.ttdealersdk.entity.response.OfflineTaskSubmissionEntity;

public interface OnTaskSubmitListener
{
    void onTaskSuccess(OfflineTaskSubmissionEntity entity);

    void onTasksFailed(int resultCode, String errorDescription);
}
