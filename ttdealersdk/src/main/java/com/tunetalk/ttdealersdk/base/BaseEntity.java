package com.tunetalk.ttdealersdk.base;

import com.tunetalk.ttdealersdk.util.APIConstant;

public class BaseEntity
{
    private String message;
    private String code;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getCode ()
    {
        return code;
    }

    public BaseEntity setCode (String code)
    {
        this.code = code;
        return this;
    }

    public boolean getResultCode ()
    {
        return code.equals("200");
    }

}
