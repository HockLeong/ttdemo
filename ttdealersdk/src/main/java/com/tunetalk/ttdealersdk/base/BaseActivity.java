package com.tunetalk.ttdealersdk.base;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.job.JobScheduler;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.tunetalk.ttdealersdk.R;
import com.tunetalk.ttdealersdk.ui.CustomInfoDialog;
import com.tunetalk.ttdealersdk.util.APIConstant;
import com.tunetalk.ttdealersdk.util.CustomDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class BaseActivity extends AppCompatActivity
{
    FragmentManager SUPPORT_FRAGMENT_MANAGER;
    APIConstant APICONSTANT;
    SharedPreferences mSharedPreferences;
    Toolbar mToolbar;
    public Activity mActivity;
    public CustomDialog mCustomDialog;
    NotificationManager mNotificationManager;
    public int mCurrentDay, mCurrentMonth, mCurrentYear;
    static BaseActivity mThis;
    String mSession;
    public String mRole;
    public int mConnectionCount;
    public static CustomInfoDialog mCustomInfoDialog;
    public JobScheduler mJobScheduler;

    private int NOTIFICATION_ID = 1;

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mActivity = this;
        mThis = this;
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        SUPPORT_FRAGMENT_MANAGER = getSupportFragmentManager();
        APICONSTANT = new APIConstant(getApplicationContext());

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        mCurrentDay = Integer.parseInt(new SimpleDateFormat("dd", Locale.getDefault()).format(calendar.getTime()));
        mCurrentMonth = Integer.parseInt(new SimpleDateFormat("M", Locale.getDefault()).format(calendar.getTime()));
        mCurrentYear = Integer.parseInt(new SimpleDateFormat("yyyy", Locale.getDefault()).format(calendar.getTime()));

        mSession = mSharedPreferences.getString("Session", null);

        mCustomDialog = new CustomDialog()
            .with(getSupportFragmentManager())
            .setContentView(R.layout.dialog_loading)
            .setMessage(getResources().getString(R.string.common_text_loading))
            .withLoadingAnimation(true)
            .setCancelable(false)
            .setCancledOnTouchOutside(false)
            .build();

//        mCustomInfoDialog = new CustomInfoDialog(this);

        if (Build.VERSION.SDK_INT >= 21)
            mJobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);

        Logger.addLogAdapter(new AndroidLogAdapter());
    }

    public void setToolbarTitle (int resId)
    {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(resId);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick (View view)
                {
                    mActivity.onBackPressed();
//                    finish();
                }
            });
        }
    }

    public void setToolbarTitle (String title)
    {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(title);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick (View view)
                {
                    BaseActivity.super.onBackPressed();
                }
            });
        }
    }

    public static BaseActivity getInstance ()
    {
        if (mThis == null)
            return new BaseActivity();
        else
            return mThis;
    }

    public String getConnectionStatus ()
    {
        String mConnectionStatus = "";

        if (mConnectionCount == 0)
        {
            mConnectionStatus = "No internet connection";
            mConnectionCount++;
        }
        else if (mConnectionCount == 1)
        {
            mConnectionStatus = "Working in offline mode";
            mConnectionCount++;
        }
        else if (mConnectionCount == 2)
        {
            mConnectionStatus = "You can still do SIM registration, SIM replacement and submit port-in.";
            mConnectionCount = 0;
        }

        return mConnectionStatus;
    }
}
