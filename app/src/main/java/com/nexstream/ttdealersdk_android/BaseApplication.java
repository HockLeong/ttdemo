package com.nexstream.ttdealersdk_android;

import android.app.Application;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

public class BaseApplication extends Application
{
    @Override public void onCreate ()
    {
        super.onCreate();
        MultiDex.install(this);
    }
}
