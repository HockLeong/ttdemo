package com.nexstream.ttdealersdk_android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;
import com.tunetalk.ttdealersdk.entity.response.OfflineTaskSubmissionEntity;
import com.tunetalk.ttdealersdk.entity.response.PendingTaskEntity;
import com.tunetalk.ttdealersdk.ttservice.DealerSDK;
import com.tunetalk.ttdealersdk.ttservice.OnInitResultListener;
import com.tunetalk.ttdealersdk.ttservice.OnTaskSubmitListener;
import com.tunetalk.ttdealersdk.util.Constant;
import com.tunetalk.tunedealer.R;

import java.util.List;

public class MainServiceActivity extends AppCompatActivity implements View.OnClickListener
{
    private final String TAG = getClass().getName();
    boolean isInitialise;

    Button btnSimReg, btnSimReplacement, btnPortIn, btnRetrieve, btnSubmit;
    TextView tvResponse;

    String resultCodeMsg, errorDescriptionMsg;

    List<PendingTaskEntity> entityList;

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_service);

        btnSimReg = findViewById(R.id.btnSimReg);
        btnSimReplacement = findViewById(R.id.btnSimReplacement);
        btnPortIn = findViewById(R.id.btnPortIn);
        btnRetrieve = findViewById(R.id.btnRetrieve);
        btnSubmit = findViewById(R.id.btnSubmit);
        tvResponse = findViewById(R.id.tvResponse);

        btnSimReg.setOnClickListener(this);
        btnSimReplacement.setOnClickListener(this);
        btnPortIn.setOnClickListener(this);
        btnRetrieve.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);

        DealerSDK.init(this, "apikey321", false, new OnInitResultListener()
        {
            @Override public void onInitResult (boolean isInit, String errorDescription)
            {
                isInitialise = isInit;
            }
        });
    }

    @Override public void onClick (View view)
    {
        switch (view.getId())
        {
            case R.id.btnSimReg:
                DealerSDK.simRegister(this, "abc123");
                break;
            case R.id.btnSimReplacement:
                DealerSDK.simReplacement(this, "abc123");
                break;
            case R.id.btnPortIn:
                DealerSDK.portIn(this, "abc123");
                break;
            case R.id.btnRetrieve:
                entityList = DealerSDK.getPendingTasks(this);
                Log.e(TAG, new GsonBuilder().create().toJson(entityList));
                break;
            case R.id.btnSubmit:
                if (entityList != null && entityList.size() > 0)
                {
                    resultCodeMsg = "";
                    errorDescriptionMsg = "";
                    tvResponse.setText("");

                    for (PendingTaskEntity entity : entityList)
                    {
                        DealerSDK.submitPendingTasks(this, entity.getId(), new OnTaskSubmitListener()
                        {
                            @Override public void onTaskSuccess (OfflineTaskSubmissionEntity entity)
                            {
                                String response = new GsonBuilder().setPrettyPrinting().create().toJson(entity);
                                Log.e("Debug", response);
                                tvResponse.setText(response);
                            }

                            @Override public void onTasksFailed (int resultCode, String errorDescription)
                            {
                                resultCodeMsg = "";
                                errorDescriptionMsg = "";
                                tvResponse.setText("");

                                resultCodeMsg = "Result Code : " + String.valueOf(resultCode);

                                if (errorDescription != null && ! errorDescription.isEmpty())
                                {
                                    Log.e("Debug", errorDescription);
                                    errorDescriptionMsg += "Error Msg: " + errorDescription;
                                }

                                tvResponse.setText(resultCodeMsg + "\n\n" + errorDescriptionMsg);
                            }
                        });
                    }

                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Please ensure there is pending task before submission", Toast.LENGTH_SHORT)
                        .show();
                }
                break;
            default:
                break;
        }
    }


    @Override protected void onActivityResult (int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        resultCodeMsg = "";
        errorDescriptionMsg = "";
        tvResponse.setText("");
        if (requestCode == Constant.RequestCode.SCAN_DOCUMENT_REQUEST_CODE)
        {
            try
            {
                Log.e("Debug", String.valueOf(resultCode));
                resultCodeMsg = "Result Code : " + String.valueOf(resultCode);

                if (data != null)
                {
                    String errorDesc = data.getStringExtra(Constant.Key.Intent.ERROR_DESCRIPTION);

                    if (errorDesc != null && ! errorDesc.isEmpty())
                    {
                        Log.e("Debug", errorDesc);
                        errorDescriptionMsg += "Error Msg: " + errorDesc;
                    }
                }

                tvResponse.setText(resultCodeMsg + "\n\n" + errorDescriptionMsg);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}
